#!/bin/bash

# CONFIGURE THIS VARIABLES
CODE_SIGN_ID_FILENAME="*.p12"
CODE_SIGN_ID_PW=""
PROVISIONING_FILENAME="*.mobileprovision"
PROJECT_FILE="CXPMobile.xcodeproj"
SCHEME="CXPMobile"
PRODUCT_NAME="Chiggle"
IPA_FILENAME="Chiggle"
BUILD_PATH=Build
# END VARIABLES

# FROM THIS POINT ON: DO NOT MODIFY
# precalculated variables
PROVISIONING_UUID=`grep UUID -A1 -a ${PROVISIONING_FILENAME} | grep -io "[-A-Z0-9]\{36\}"`
PROVISIONING_NAME=`egrep -a -A 2 '>Name<' ${PROVISIONING_FILENAME} | grep string | sed -e 's/<string>//' -e 's/<\/string>//' -e 's/	//'`

# install certificate [optional if the .p12 identities haven't been installed]
# security import "${CODE_SIGN_ID_FILENAME}" -k "~/Library/Keychains/login.keychain" -P "${CODE_SIGN_ID_PW}" -T /usr/bin/codesign

# install provisioning profile
cp "${PROVISIONING_FILENAME}" ~"/Library/MobileDevice/Provisioning Profiles/${PROVISIONING_UUID}.mobileprovision"

# export archive
xcodebuild -project "${PROJECT_FILE}" -scheme ${SCHEME} archive PROVISIONING_PROFILE="${PROVISIONING_UUID}" -archivePath "${BUILD_PATH}/${PRODUCT_NAME}.xcarchive"

# export ipa
xcodebuild -project "${PROJECT_FILE}" -exportArchive -exportFormat IPA -exportProvisioningProfile "${PROVISIONING_NAME}" -archivePath "${BUILD_PATH}/${PRODUCT_NAME}.xcarchive" -exportPath "${BUILD_PATH}/${IPA_FILENAME}.ipa"