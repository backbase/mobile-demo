/**
 * ------------------------------------------------------------------------
 * widgetGwtLogin entry point
 * ------------------------------------------------------------------------
 */
(function(window, factory) {
    'use strict';
    var name = 'widgetWobbly';
    if (typeof module === 'object' && module.exports) {
        // only CommonJS-like environments that support module.exports,
        module.exports = factory(name);
    } else {
        // Browser globals (root is window)
        window[name] = factory(name);
    }
}(this, function(name) {

    'use strict';

function init(widget) {

        var wBody = $(widget.body);

        // The widget needs to inform it's done loading so preloading works as expected
        gadgets.pubsub.publish('cxp.item.loaded', {
            id: widget.model.name
        });

        buildChart(widget.body.firstChild);
    }


    function buildChart(el){
        var mouse = [document.body.clientWidth/2, document.body.clientHeight/2],
            count = 0;

        var svg = d3.select(el).append("svg");


        var g = svg.selectAll("g")
            .data(d3.range(25))
            .enter().append("g")
            .attr("transform", "translate(" + mouse + ")");

        g.append("rect")
            .attr("rx", 6)
            .attr("ry", 6)
            .attr("x", -12.5)
            .attr("y", -12.5)
            .attr("width", 25)
            .attr("height", 25)
            .attr("transform", function(d, i) { return "scale(" + (1 - d / 25) * 20 + ")"; })
            .style("fill", d3.scale.category20c());

        g.datum(function(d) {
            return {center: [0, 0], angle: 0};
        });

        svg.on("touchmove", function() {
            mouse = d3.mouse(this);
        });

        d3.timer(function() {
            count++;
            g.attr("transform", function(d, i) {
                d.center[0] += (mouse[0] - d.center[0]) / (i + 5);
                d.center[1] += (mouse[1] - d.center[1]) / (i + 5);
                d.angle += Math.sin((count + i) / 10) * 7;
                return "translate(" + d.center + ")rotate(" + d.angle + ")";
            });
        });
    }



   

    /**
     * Main Widget function
     * @param  {object} widget instance
     * @return {object}        Application object
     * @public
     */
    function App(widget, deps) {

        gadgets.pubsub.publish('cxp.item.loaded', {
            id: widget.model.name
        });

 

        var obj = Object.create(App.prototype);
        var args = Array.prototype.slice.call(arguments);
        obj.widget = widget;
        return obj;
    }

    /**
     * Widget proto
     * @type {object}
     */
    App.prototype = {
        bootstrap: function() {
            init(this.widget);
            return this;
        }
    };

    return App;
}));
