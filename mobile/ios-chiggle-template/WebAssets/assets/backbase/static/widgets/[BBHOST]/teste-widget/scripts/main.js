(function(window) {
	var $city;
	var city;
    function run(widget) {

        var $widget = $(widget.body);

        $city = $widget.find('h1');

        city = widget.getPreference('city');

        $city.text(city);
        registerEvents(widget);

		$widget.find('.loading').removeClass('loading');
    }

    function registerEvents(widget) {
    	var widget = widget
        // Binding a callback when preferences are changed
        widget.model.addEventListener('PrefModified', function(event) {
            console.log(event);
            // debugger

            $city.text(event.newValue);

            gadgets.pubsub.publish('city-name', {
        		city:event.newValue
        	});
        });

        
    }


    window.testeWidget = run;

})(window);