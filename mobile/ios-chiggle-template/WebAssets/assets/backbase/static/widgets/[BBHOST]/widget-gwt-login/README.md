# widget-gwt-login
Performs authentication on MBAAS

## Information
| Name       |  widget-gwt-login |
|------------|---|
| Version    | 1  |
| Bundle     | DBP |
| Authors      | carlos  |
| Icon       | ![icon](icon.png) |
| Status     | N/A |

## Preferences

## Events

## Custom Components

## Develop Standalone

```bash
cd <widget-path>
bower install && bblp start
```

## Requirements

### User Requirements

### Business Requirements

## References
