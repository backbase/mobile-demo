// Enables offline capabilities in content widgets
// exposes makeOffline, the widget should call this global function and pass the widget object 

(function(window) {

  "use strict";

  var ls = window.localStorage;
  var state = navigator.onLine;
  var timeKey = "-last-saved";

  /**
   * Gets base64 from image
   * @param {img} image to convert
   * @param {callback} callback function
   */
  function getDataUri(img, callback) {
    var image = img;
    //use the image dimensions in the DOM to save space.
    var width = image.width;
    var height = image.height;

    var canvas = document.createElement('canvas');
    canvas.width = width;
    canvas.height = height;
    canvas.getContext('2d').drawImage(img, 0, 0, width, height);
    // get as Data URI
    callback(canvas.toDataURL('image/png'));
  }


  /**
   * Gets images from the widget dom
   * @param {widget} widget object
   */
  function getImages(widget) {
    // var images = findImages(htmlStr);
    var imgs = widget.body.getElementsByTagName('img');
    Array.prototype.forEach.call(imgs, function(img, i) {

      img.addEventListener('load', function(event) {
        getDataUri(event.target, function(dataUri) {

          ls[img.src] = dataUri;

          console.log(dataUri.length, img.width, img.height);
        });
      });
    });
  }

  /**
   * set the widget content from localStorage
   * @param {widget} widget object
   */
  function setContent(widget) {

    widget.body.innerHTML = ls[widget.model.name];
    var imgs = widget.body.getElementsByTagName('img');

    Array.prototype.forEach.call(imgs, function(el, i) {
      el.src = localStorage[el.src];
    });

  }

  /**
   * Saves widget content to localStorage
   * @param {widget} widget object
   */
  function makeOffline(widget) {

    if (state) {
      var cached = ls[widget.model.name];
      //if cached and last updated is the same as the saved one
      if (cached && cached.length > 0 && ls[widget.model.name + timeKey] == widget.getPreference("widgetContentsUpdated")) {

        setContent(widget);

        console.log("LOADED FROM CACHE!!");
      } else {

        var widgetHtml = widget.body.firstChild.innerHTML;
        ls[widget.model.name] = widgetHtml;
        ls[widget.model.name + timeKey] = widget.getPreference("widgetContentsUpdated");
        getImages(widget);

        console.log("Updating from server......");
      }

      return;
    }

    //if offline we set the cached content
    setContent(widget);
  }

  window.makeOffline = window.makeOffline || makeOffline;

}(window));