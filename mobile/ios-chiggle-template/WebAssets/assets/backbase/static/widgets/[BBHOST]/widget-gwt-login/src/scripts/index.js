/**
 * ------------------------------------------------------------------------
 * widgetGwtLogin entry point
 * ------------------------------------------------------------------------
 */
(function(window, factory) {
    'use strict';
    var name = 'widgetGwtLogin';
    if (typeof module === 'object' && module.exports) {
        // only CommonJS-like environments that support module.exports,
        module.exports = factory(name);
    } else {
        // Browser globals (root is window)
        window[name] = factory(name);
    }
}(this, function(name) {

    'use strict';

    var bus = window.gadgets.pubsub;
    var ng = window.angular;

    /**
     * Main Controller
     * @param {object} widget cxp widget instance
     * @param {object} $http angular ajax request library
     */
    function MainCtrl(widget, $http) {
        var vm = this;

        vm.user = {
            username: '',
            password: ''
        };

        var navigationMap = {
            login: 'navigation:login',
            register: 'navigation:register-from-login'
        };
        
        vm.register = function() {
            bus.publish(navigationMap.register);
        };

        vm.action = JSON.parse(widget.getPreference('action'));

        function buildRequest(formData) {

            var LOGINENDPOINT = 'j_spring_security_check';
            var LOGOUTENDPOINT = 'j_spring_security_logout';
            var formData = formData || {};
            var serverUrl = b$.portal.config.serverRoot;

            var endpoint = (formData && formData.username) ? LOGINENDPOINT : LOGOUTENDPOINT;

            var req = {
                method: 'POST',
                url: serverUrl + '/' + endpoint,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json',
                    'Req-X-Auth-Token': 'JWT'
                },

                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {
                    j_username: formData.username || '',
                    j_password: formData.password || '',
                    deviceType: "Android",
                    deviceId: "1234567890"
                }
            };

            return req;
        }

        vm.authenticate = function(data) {
            var req = buildRequest(data);

            $http(req)
                .success(function(res) {
                    if (res && res.status && res.status === 'OK') {
                        gadgets.pubsub.publish('navigation:user-auth');
                    }
                }).error(function(res) {
                    alert('Please try again');
                });
        };
    }


    function addEventListeners(widget) {
        widget.addEventListener('preferencesSaved', function(ev) {
            this.refreshHTML();
        })
    }

    /**
     * Error Controller
     * Binds the widget errors to the view
     * @param {object} widget cxp widget instance
     */
    function ErrorCtrl(widget) {}

    /**
     * Create Angular Module
     * @param  {object} widget widget instance
     * @param  {array} deps   angular modules dependencies
     * @return {object}        angular module
     */
    function createModule(widget, deps) {
        return ng.module(name, deps || [])
            .value('widget', widget)
            .controller('MainCtrl', ['widget', '$http', MainCtrl])
            .controller('ErrorCtrl', ['widget', ErrorCtrl]);
    }

    /**
     * Main Widget function
     * @param  {object} widget instance
     * @return {object}        Application object
     * @public
     */
    function App(widget, deps) {

        gadgets.pubsub.publish('cxp.item.loaded', {
            id: widget.model.name
        });

        addEventListeners(widget);

        var obj = Object.create(App.prototype);
        var args = Array.prototype.slice.call(arguments);
        obj.widget = widget;
        obj.module = createModule.apply(obj, args);
        return obj;
    }

    /**
     * Widget proto
     * @type {object}
     */
    App.prototype = {
        bootstrap: function() {
            ng.bootstrap(this.widget.body, [this.module.name]);
            return this;
        },
        destroy: function() {
            this.module = null;
        }
    };

    return App;
}));
