# Cxp Mobile - iOS drawer template

## What's in the Box
This project contain a fully functional iOS app that can be used as starting point for any project. It provides a set of exaples of interaction between a mobile application and CXP platform and best practices (we strongly encourage to follow those too).

### This repository contains:

- An iOS Application
- The latest version of CXP Mobile SDK
- Backbase widgets
- Project settings

### Screenshots

![Alt text](./screenshots/home.png?raw=true)

![Alt text](./screenshots/overview.png?raw=true)

![Alt text](./screenshots/changelog.png?raw=true)

![Alt text](./screenshots/changelog-detail.png?raw=true)

## Prerequisites
- xCode
- A git client is installed

## Usage
- Clone this repo to your local machine
- Open up the project on xCode
- Run the app

## License
[TODO]
