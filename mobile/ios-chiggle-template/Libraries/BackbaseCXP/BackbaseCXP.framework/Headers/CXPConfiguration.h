//
//  CXPConfiguration.h
//  BackbaseCXP
//
//  Created by Backbase R&D B.V. on 23/02/15.
//

#import <BackbaseCXP/BackbaseCXP.h>
#import <Foundation/Foundation.h>

/**
 * CXP-specific configuration
 */
@interface CXPConfiguration : NSObject

/// Portal name
@property (strong, nonatomic) NSString* portal;

/// Base backend URL (host + port)
@property (strong, nonatomic) NSString* serverURL;

/// Remote context root. if omitted, remoteContextRoot is initialized with serverURL
@property (strong, nonatomic) NSString* remoteContextRoot;

/// Local model path
@property (strong, nonatomic) NSString* localModelPath;

/// Template specific information.
#ifdef __cplusplus
@property (strong, nonatomic, getter=getTemplate) NSDictionary* _template;
#else
@property (strong, nonatomic, getter=getTemplate) NSDictionary* template;
#endif

/// Behaviour Map array
@property (strong, nonatomic) NSArray* behaviourMap DEPRECATED_ATTRIBUTE;

/// Synced Preferences definition
@property (strong, nonatomic) NSDictionary* syncedPreferences;

/// Domain Access array, whitelist of domains
@property (strong, nonatomic) NSArray* domainAccess;

/// Block all webview http(s) requests to enforce more secure communications via a native plugin
@property (assign, nonatomic) BOOL blockWebViewRequests;

/// Pinned SSL certificates, if not empty only https connections to specific certificates are allowed.
@property (strong, nonatomic) NSArray* pinnedCertificates;

/// SSL pinning exceptions, host patterns to be excluded from the pinning check.
@property (strong, nonatomic) NSArray* pinningExceptions;

/// Debug flag
@property (assign, nonatomic) BOOL debug;

/// Debug context root $(contextRoot) replacement, only for debug/development purposes
@property (strong, nonatomic) NSString* debugContextRoot;

/// Debug performance endpoint URL
@property (strong, nonatomic) NSString* debugPerformanceEndpointURL;

/// Debug flag to allow untrusted certificates
@property (assign, nonatomic) BOOL debugAllowUntrustedCertificates;

@end
