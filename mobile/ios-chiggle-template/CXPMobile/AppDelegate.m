//
//  AppDelegate.m
//  CXPMobile
//
//  Created by Backbase R&D B.V.
//

#import "AppDelegate.h"
#import "CXPViewController.h"
#import "Renderable.h"
#import "LoadingViewController.h"
#import "SWRevealViewController.h"
#import "SlideoutMenuViewController.h"
#import "ReachabilityManager.h"

@interface AppDelegate ()

@property (nonatomic, strong, readwrite) NSObject<Model> *model;
@property (nonatomic, strong) LoadingViewController *loadingVC;
@property (nonatomic, copy) void (^modelSuccessBlock)(NSObject<Model> *model);
@property (nonatomic, copy) void (^modelFailureBlock)(NSError *error);
@property (nonatomic, strong) UIView *loadingView;

@end

@implementation AppDelegate

/**
 * This method is executed when the application finished launching.
 */
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Programmatically create a window and attach it to the entire screen
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    // Attach the initial view controller to the window
    self.window.rootViewController = [self initialViewController];

    // Show the window BEFORE initialize CXP SDK, this allows faster preload and better performance overall.
    [self.window makeKeyAndVisible];

    // Initialize the Backbase CXP SDK
    [self setupBackbaseCXP];
    
    // Initialize Network manager
    [ReachabilityManager sharedInstance];

    return YES;
}

/**
 * This method is creating the initial view controller. Currently it's copying the splash screen to allow windows to
 * preload.
 */
- (UIViewController *)initialViewController {
    // Create temporary splash screen copy to allow widgets to preload, this screen will be removed when preloading  is finished

    self.loadingVC = [LoadingViewController new];
    __weak typeof(self) wSelf = self;
    self.loadingVC.retryBlock = ^(){
        __strong typeof(self) sSelf = wSelf;
        [sSelf clearBackbaseCXP];
        [sSelf setupBackbaseCXP];
    };
    return self.loadingVC;
}

/**
 * This method is setting up all the Backbase CXP Mobile SDK related components.
 */
- (void)setupBackbaseCXP {
    // Check if the device is jailbroken, deny app usage if this is the case
    if ([CXP isDeviceJailbroken]) {
        [[[UIAlertView alloc]
                initWithTitle:kCHGDeviceIsJailbrokenAlertTitle
                      message:kCHGDeviceIsJailbrokenAlertMessage
                     delegate:nil
            cancelButtonTitle:nil
            otherButtonTitles:nil] show];
        return;
    }

    // Initialize and configure library
    NSError *error = nil;
    [CXP initialize:kCHGConfigFilePath error:&error];
    if (error) {
        [CXP logError:self
              message:[NSString stringWithFormat:@"Unable to read configuration due error: %@",
                                                 error.localizedDescription ?: @"Unknown error"]];
    }

    // Register observer that observes preloaded items
    [CXP registerPreloadObserver:self selector:@selector(preloadCompleted:)];

    // Register observer that observes navigation flow events
    [CXP registerNavigationEventListener:self selector:@selector(didReceiveNavigationNotification:)];

    // Register observer that observes security policy violations
    [CXP securityViolationDelegate:self];

    // Get a list of pages from the main navigation
    [CXP model:self order:@[kModelSourceServer, kModelSourceFile]];
    
    // On login reload the model
    [CXP registerObserver:self selector:@selector(loadModel:) forEvent:@"navigation:user-auth"];
    
    // Show/hide loading view
    [CXP registerObserver:self selector:@selector(startLoadingAnimation:) forEvent:@"loading-animation:start"];
    [CXP registerObserver:self selector:@selector(stopLoadingAnimation:) forEvent:@"loading-animation:stop"];
}

/**
 * This method is clearing up all the Backbase CXP Mobile SDK related components.
 * It should do the opposite of -setupBackbaseCXP method.
 */
- (void)clearBackbaseCXP {
    [CXP unregisterObserver:self forEvent:@"navigation:user-auth"];
    [CXP unregisterObserver:self forEvent:@"loading-animation:start"];
    [CXP unregisterObserver:self forEvent:@"loading-animation:stop"];
    [CXP unregisterNavigationEventListener:self];
    [CXP unregisterPreloadObserver:self];
    [CXP invalidateModel];
}

/**
 * This method is executed when all items scheduled for preloaded, are preloaded. It's mainly used to initialize the
 * user interace and hide the splash screen after the user interface is initialized.
 */
- (void)preloadCompleted:(NSNotification *)notification {
    if (!self.loadingVC.isAlertShown) {
        [self proceedWithPreloadedInterface];
    }
}

- (void)proceedWithPreloadedInterface
{
    UIViewController *containerViewController = nil;
    NSArray *sitemap = [self.model siteMapItemChildrenFor:@"Main Navigation"];
    NSObject<SiteMapItemChild> *siteMapObject = [sitemap firstObject];
    
    if ([sitemap count] == 1 && [[siteMapObject children] count] > 0) { // UINavigationController
        containerViewController = [self createNavigationControllerWithSiteMapObject:siteMapObject];
    } else { // SWRevealViewController
        containerViewController = [self createSlideoutControllerWithSiteMapArray:sitemap];
    }
    
#warning - Hack that improves perceived user experience
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView transitionWithView:self.window
                          duration:0.3
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{ self.window.rootViewController = containerViewController; }
                        completion:^(BOOL finished) {
                            [self stopLoadingAnimation];
                        }];
    });
}

- (UINavigationController *)createNavigationControllerWithSiteMapObject:(NSObject<SiteMapItemChild> *)siteMapObject {
    // Create renderable item to render the content of the page
    NSObject<Renderable> *renderable = [self.model itemById:siteMapObject.itemRef];
    // Create view controller
    CXPViewController *viewController = [[CXPViewController alloc] initWithRenderable:renderable];
    // Create navigation controller
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    return navigationController;
}

- (SWRevealViewController *)createSlideoutControllerWithSiteMapArray:(NSArray *)sitemap {
    NSObject<SiteMapItemChild> *firstSiteMapObject = [sitemap firstObject];
    NSArray *renderables = [self renderablesFromSitemap:sitemap];
    UINavigationController *frontVC = [self createNavigationControllerWithSiteMapObject:firstSiteMapObject];
    SlideoutMenuViewController *slideoutVC = [[SlideoutMenuViewController alloc] initWithRenderables:renderables];
    UINavigationController *menuVC = [[UINavigationController alloc] initWithRootViewController:slideoutVC];
    SWRevealViewController *revealViewController = [[SWRevealViewController alloc] initWithRearViewController:menuVC frontViewController:frontVC];
    CGFloat rearViewRevealWidth = self.window.bounds.size.width - 50.0;
    [revealViewController setRearViewRevealWidth:rearViewRevealWidth];
    [revealViewController setDelegate:slideoutVC];

    return revealViewController;
}

- (NSArray *)renderablesFromSitemap:(NSArray *)sitemap {
    NSMutableArray *renderables = [NSMutableArray array];
    for (NSObject<SiteMapItemChild> *siteMapObject in sitemap) {
        NSObject<Renderable> *renderable = [self.model itemById:siteMapObject.itemRef];
        [renderables addObject:renderable];
    }
    
    return [renderables copy];
}

/**
 * This method is executed when the library detects a navigation request from a widget. The notification contains
 * information about the origin, target and relation of the origin and target. This information is used to determine
 * what en how to show a page.
 */
- (void)didReceiveNavigationNotification:(NSNotification *)notification {
    // Get information about the navigation flow event
    NSString *origin = notification.userInfo[@"origin"];
    NSString *target = notification.userInfo[@"target"];
    NSString *relationship = notification.userInfo[@"relationship"];

    // Check if an external link is requested
    if ([relationship isEqualToString:kCXPNavigationFlowRelationshipExternal]) {
        // Open the external link is the externan web browser
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:target]];
        return;
    }

    // Check if a root item is selected
    if ([relationship isEqualToString:kCXPNavigationFlowRelationshipRoot]) {
        SWRevealViewController *revealViewController = (SWRevealViewController *)self.window.rootViewController;
        UINavigationController *rearNavVC = (UINavigationController *)revealViewController.rearViewController;
        SlideoutMenuViewController *slideoutVC = (SlideoutMenuViewController *)rearNavVC.viewControllers.firstObject;
        
        for (NSObject<Renderable> *renderable in slideoutVC.renderables) {
            if ([renderable.itemId isEqualToString:target]) {
                CXPViewController *viewController = [[CXPViewController alloc] initWithRenderable:renderable];
                UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:viewController];
                [revealViewController pushFrontViewController:navVC animated:YES];
                return;
            }
        }
    }

    // Check if a child item is selected
    if ([relationship isEqualToString:kCXPNavigationFlowRelationshipChild]) {
        // Get the view controller submitting the navigation flow event
        UINavigationController *navigationController =
        (UINavigationController *)self.window.rootViewController;
        
        // Create renderable item to render the content of the requested page
        NSObject<Renderable> *renderable = [self.model itemById:target];
        
        // Create a new view controller
        CXPViewController *viewController = [[CXPViewController alloc] initWithRenderable:renderable];
        
        // Push the newly created view controller to the current navigation stack
        [navigationController pushViewController:viewController animated:YES];
        
#warning - Navigation hack1
        if ([origin isEqualToString:@"index"] && [renderable.itemId isEqualToString:@"page_1454341767652"]) {
            CXPViewController *topVC = [navigationController.viewControllers firstObject];
            [navigationController setViewControllers:@[topVC, viewController] animated:NO];
        }
    }
    
    // Check if a parent item is selected
    if ([relationship isEqualToString:kCXPNavigationFlowRelationshipParent]) {
        UINavigationController *navigationController =
        (UINavigationController *)self.window.rootViewController;
       
        // Go one step back in navigation hierarchy
        [navigationController popViewControllerAnimated:YES];
    }
    
    // Check if an other item is selected
    if ([relationship isEqualToString:kCXPNavigationFlowRelationshipOther]) {
        // Get the view controller submitting the navigation flow event
        UINavigationController *navigationController =
        (UINavigationController *)self.window.rootViewController;
        
        // Create renderable item to render the content of the requested page
        NSObject<Renderable> *renderable = [self.model itemById:target];
        
        // Create a new view controller
        CXPViewController *viewController = [[CXPViewController alloc] initWithRenderable:renderable];
        
        // Push the newly created view controller to the current navigation stack
        [navigationController pushViewController:viewController animated:YES];
        
#warning - Navigation hack2
        if ([origin isEqualToString:@"page_1454389035883"] && [renderable.itemId isEqualToString:@"index"]) {
            CXPViewController *topVC = [navigationController.viewControllers firstObject];
            [navigationController setViewControllers:@[topVC, viewController] animated:NO];
        }
    }
    
    // Check if a sibling item is selected
    if ([relationship isEqualToString:kCXPNavigationFlowRelationshipSibling]) {
        NSLog(@"*** Received sibling relationship.");
    }
}

#pragma mark - ModelDelegate

/**
 * This method is executed when the model is loaded. It's creating a reference to the model so we can use it at a later
 * stage.
 */
- (void)modelDidLoad:(NSObject<Model> *)model {
    self.model = model;
    if (self.modelSuccessBlock) {
        self.modelSuccessBlock(model);
        self.modelSuccessBlock = nil;
    }
}

/**
 * This method is executed when the model couldn't be loaded.
 */
- (void)modelDidFailLoadWithError:(NSError *)error {
    // Show a non-closable error indicating that something bad happened
    [[[UIAlertView alloc] initWithTitle:kCHGModelLoadingErrorAlertTitle
                                message:kCHGModelLoadingErrorAlertMessage
                               delegate:nil
                      cancelButtonTitle:nil
                      otherButtonTitles:nil] show];
    
    if (self.modelFailureBlock) {
        self.modelFailureBlock(error);
        self.modelFailureBlock = nil;
    }
}

#pragma mark - SecurityViolationDelegate

/**
 * This method is executed when a (security) policy violation has occurred. It can be used to block the usage of the
 * app.
 */
- (void)securityDidReceiveViolation:(NSError *)error {
    // Show a non-closable error indicating that something bad happened
    [[[UIAlertView alloc] initWithTitle:kCHGSecurityPolicyViolationAlertTitle
                                message:kCHGSecurityPolicyViolationAlertMessage
                               delegate:nil
                      cancelButtonTitle:nil
                      otherButtonTitles:nil] show];
}

#pragma mark - Custom methods
/**
 * Reload the model
 */
- (void)loadModel:(NSNotification *)notification {
    [self startLoadingAnimation];
    [CXP model:self order:@[kModelSourceServer, kModelSourceFile]];
}

- (void)startLoadingAnimation:(NSNotification *)notification {
    [self startLoadingAnimation];
}

- (void)stopLoadingAnimation:(NSNotification *)notification {
    [self stopLoadingAnimation];
}

- (void)reloadModelWithSuccess:(void (^)(NSObject<Model> *model))success failure:(void (^)(NSError *error))failure {
    if (success) {
        self.modelSuccessBlock = success;
    }
    if (failure) {
        self.modelFailureBlock = failure;
    }
    [self startLoadingAnimation];
    [CXP model:self order:@[kModelSourceServer, kModelSourceFile]];
}

- (void)startLoadingAnimation {
//    UIView *view = [[[[UIApplication sharedApplication] keyWindow] subviews] lastObject];
//    UIView *view = self.window.rootViewController.view;
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    self.loadingView = [[UIView alloc] initWithFrame:window.frame];
    self.loadingView.userInteractionEnabled = NO;
    self.loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.loadingView addSubview:spinner];
    [spinner setCenter:self.loadingView.center];
    [spinner startAnimating];
    
    [window addSubview:self.loadingView];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
}

- (void)stopLoadingAnimation {
    [self.loadingView removeFromSuperview];
    self.loadingView = nil;
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
}

@end
