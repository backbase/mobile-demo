//
//  SlideoutMenuViewController.m
//  CXPMobile
//
//  Created by Oleksandr Perepelitsyn on 2/8/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import "SlideoutMenuViewController.h"
#import "CXPViewController.h"
#import "NetworkManager.h"
#import "AppDelegate.h"

typedef NS_ENUM(NSInteger, UITableViewGroups) {
    UITableViewGroupPages = 0,
    UITableViewGroupsCount
};

@interface SlideoutMenuViewController ()

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, copy, readwrite) NSArray *renderables;

@end

@implementation SlideoutMenuViewController

- (instancetype)initWithRenderables:(NSArray *)renderables
{
    self = [super init];
    if (self) {
        _renderables = [renderables copy];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    NSIndexPath *firstRow = [NSIndexPath indexPathForRow:0 inSection:UITableViewGroupPages];
    [self.tableView selectRowAtIndexPath:firstRow animated:NO scrollPosition:UITableViewScrollPositionTop];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return UITableViewGroupsCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch ((UITableViewGroups)section) {
        case UITableViewGroupPages:
            return [self.renderables count];
            break;
        case UITableViewGroupsCount:
            NSAssert(NO, @"Undefined group.");
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    
    switch ((UITableViewGroups)indexPath.section) {
        case UITableViewGroupPages: {
            NSObject<Renderable> *renderable = self.renderables[indexPath.row];
            // Set title
            cell.textLabel.text = renderable.itemName;
            // Set icon, if available
            NSArray *iconPack = renderable.itemIcons;
            if (iconPack.count > 0) {
                NSObject<IconPack> *icon = iconPack[0];
                cell.imageView.image = icon.normal;
            }
        }
            break;

        case UITableViewGroupsCount:
            break;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch ((UITableViewGroups)indexPath.section) {
        case UITableViewGroupPages:
            [self didSelectPageAtRow:indexPath.row];
            break;

        case UITableViewGroupsCount:
            break;
    }
}

#pragma mark - SWRevealViewControllerDelegate

- (void)revealController:(SWRevealViewController *)revealController didAddViewController:(UIViewController *)viewController forOperation:(SWRevealControllerOperation)operation animated:(BOOL)animated {
    if (operation == SWRevealControllerOperationReplaceFrontController) {
        NSIndexPath *indexPath = [self indexPathForFrontViewController];
        [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionTop];
    }
}

#pragma mark - Helper methods

- (void)didSelectPageAtRow:(NSInteger)row {
    NSObject<Renderable> *frontVCRenderable = [self renderableForViewController:self.revealViewController.frontViewController];
    NSObject<Renderable> *currentRenderable = self.renderables[row];
    
    if ([frontVCRenderable.itemName isEqualToString:currentRenderable.itemName]) {
        [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
        return;
    } else {
        CXPViewController *viewController = [[CXPViewController alloc] initWithRenderable:currentRenderable];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
        [self.revealViewController pushFrontViewController:navigationController animated:YES];
    }
}

- (void)didSelectLogout {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate startLoadingAnimation];
    [[NetworkManager sharedInstance] logoutWithCompletion:^(BOOL success) {
        if (success) {
            [appDelegate reloadModelWithSuccess:^(NSObject<Model> *model) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
                    [appDelegate stopLoadingAnimation];
                });
            } failure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [appDelegate stopLoadingAnimation];
                });
            }];
        }
    }];
}

- (NSObject<Renderable> *)renderableForViewController:(UIViewController *)viewController {
    UINavigationController *navVC = (UINavigationController *)viewController;
    CXPViewController *vc = (CXPViewController *)navVC.topViewController;
    
    return vc.page;
}

- (NSIndexPath *)indexPathForFrontViewController {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    NSObject<Renderable> *frontVCRenderable = [self renderableForViewController:self.revealViewController.frontViewController];
    for (int i = 0; i < self.renderables.count; i++) {
        NSObject<Renderable> *renderable = self.renderables[i];
        if ([renderable.itemId isEqualToString:frontVCRenderable.itemId]) {
            indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            return indexPath;
        }
    }
    
    return indexPath;
}

@end
