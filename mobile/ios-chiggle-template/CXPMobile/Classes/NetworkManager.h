//
//  NetworkManager.h
//  CXPMobile
//
//  Created by Oleksandr Perepelitsyn on 2/12/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkManager : NSObject

+ (instancetype)sharedInstance;

- (void)logoutWithCompletion:(void (^)(BOOL success))completion;

@end
