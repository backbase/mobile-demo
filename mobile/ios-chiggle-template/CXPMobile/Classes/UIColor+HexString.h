//
//  UIColor+HexString.h
//  CXPMobile
//
//  Created by Oleksandr Perepelitsyn on 1/22/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexString)

// Supports the following HEX strings: #RGB, #ARGB, #RRGGBB, #AARRGGBB
+ (UIColor *)colorWithHexString:(NSString *)hexString;

@end
