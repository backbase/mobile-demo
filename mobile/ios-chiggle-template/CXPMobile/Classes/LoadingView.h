//
//  LoadingView.h
//  CXPMobile
//
//  Created by Oleksandr Perepelitsyn on 1/27/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingView : UIView

@property (nonatomic, strong) UIColor *color;

- (void)startAnimation;
- (void)stopAnimation;

@end
