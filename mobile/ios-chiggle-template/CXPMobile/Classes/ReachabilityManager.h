//
//  NetworkManager.h
//  CXPMobile
//
//  Created by Oleksandr Perepelitsyn on 2/11/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReachabilityManager : NSObject

+ (instancetype)sharedInstance;

- (BOOL)isInternetConnectionAvailable;
- (BOOL)isRemoteHostReachable;
- (BOOL)checkIfInternetConnectionAvailableAndShowAlert:(BOOL)showAlert;

@end
