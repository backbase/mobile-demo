//
//  Constants.h
//  CXPMobile
//
//  Created by Oleksandr Perepelitsyn on 2/9/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#pragma mark - Text Constants

static NSString *const kCHGDeviceIsJailbrokenAlertTitle = @"Device is jailbroken";
static NSString *const kCHGDeviceIsJailbrokenAlertMessage = @"For your own safety we don't allow users with jailbroken devices to use this application.";

static NSString *const kCHGModelLoadingErrorAlertTitle = @"Error while loading model";
static NSString *const kCHGModelLoadingErrorAlertMessage = @"The app model couldn't be loaded. This is most likely because of a missing or incorrect implemented model. Please inform the organisation. Restart or reinstall the application to continue using it.";

static NSString *const kCHGSecurityPolicyViolationAlertTitle = @"Security policy violation";
static NSString *const kCHGSecurityPolicyViolationAlertMessage = @"The app's security policy has been violated. Please inform the organisation. Restart or reinstall the application to continue using it.";

static NSString *const kCHGGenericErrorAlertTitle = @"Error";
static NSString *const kCHGAppIsUnavailableAlertMessage = @"Oops! Chiggle is not currently available. Please try again later.";
static NSString *const kCHGNoInternetConnectionAlertMessage = @"No internet connection detected. Chiggle requires a working internet connection.";

static NSString *const kCHGOkTitle = @"Ok";
static NSString *const kCHGTryAgainTitle = @"Try Again";

#pragma mark - Numeric Constants

static NSTimeInterval const kCHGLoadingTimeout = 10.0;
static NSInteger const kCHGSuccessStatusCode = 200;

#pragma mark - CXP Properties

static NSString *const kCHGPropertyBackground = @"background";
static NSString *const kCHGPropertyShouldHideNavBar = @"shouldHideNavBar";
static NSString *const kCHGPropertyMobileNavBarColor = @"mobileNavBarColor";
static NSString *const kCHGPropertyNavBarTitle = @"navBarTitle";

#pragma mark - Other Constants

static NSString *const kCHGConfigFilePath = @"assets/backbase/conf/configs.json";

#endif /* Constants_h */
