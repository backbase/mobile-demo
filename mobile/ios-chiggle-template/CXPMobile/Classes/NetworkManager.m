//
//  NetworkManager.m
//  CXPMobile
//
//  Created by Oleksandr Perepelitsyn on 2/12/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import "NetworkManager.h"

@interface NetworkManager ()
@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, copy) NSString *serverUrl;
@end

@implementation NetworkManager

+ (instancetype)sharedInstance {
    static NetworkManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        if (self) {
            [sharedInstance setup];
        }
    });
    return sharedInstance;
}

- (void)setup {
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.session = [NSURLSession sessionWithConfiguration:config];
    self.serverUrl = [[CXP configuration] serverURL];
}

#pragma mark - Public methods

- (void)logoutWithCompletion:(void (^)(BOOL success))completion {
    NSString *urlString = [NSString stringWithFormat:@"%@/portalserver/j_spring_security_logout", self.serverUrl];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"POST";
    NSURLSessionDataTask *dataTask =[self.session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            if (error == nil && httpResponse.statusCode == kCHGSuccessStatusCode) {
               if (completion) {
                   completion(YES);
               }
            } else {
                if (completion) {
                    completion(NO);
                }
            }
       }];
    [dataTask resume];
}

@end
