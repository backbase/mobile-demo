//
//  NetworkManager.m
//  CXPMobile
//
//  Created by Oleksandr Perepelitsyn on 2/11/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import "ReachabilityManager.h"
#import "Reachability.h"

@interface ReachabilityManager ()
@property (nonatomic, strong) Reachability *internetReachability;
@end

@implementation ReachabilityManager

+ (instancetype)sharedInstance {
    static ReachabilityManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        if (self) {
            [sharedInstance setup];
        }
    });
    return sharedInstance;
}

- (void)setup {
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
    [self reactToReachability:self.internetReachability];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
}

- (void)reachabilityChanged:(NSNotification *)notif {
    Reachability *reachability = [notif object];
    NSParameterAssert([reachability isKindOfClass:[Reachability class]]);

    [self reactToReachability:reachability];
}

- (void)reactToReachability:(Reachability *)reachability {
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
//    if (reachability == self.internetReachability) {
        if (networkStatus == NotReachable) {
            [self showAlertWithTitle:kCHGGenericErrorAlertTitle message:kCHGNoInternetConnectionAlertMessage];
        }
//    }
}

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message {
    if ([UIAlertController class]) { // iOS 8,9
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *tryAgain = [UIAlertAction actionWithTitle:kCHGOkTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                 [alert dismissViewControllerAnimated:YES completion:nil];
             }];
        [alert addAction:tryAgain];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alert animated:YES completion:nil];
    } else { // iOS 7 and older
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:kCHGOkTitle
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - Public methods

- (BOOL)isInternetConnectionAvailable {
    NetworkStatus networkStatus = [self.internetReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        return NO;
    } else {
        return YES;
    }
}

- (BOOL)isRemoteHostReachable {
    NSString *serverUrl = [[CXP configuration] serverURL];
    Reachability *hostReachability = [Reachability reachabilityWithHostName:serverUrl];
    if (hostReachability == NotReachable) {
        return NO;
    } else {
        return YES;
    }
}

- (BOOL)checkIfInternetConnectionAvailableAndShowAlert:(BOOL)showAlert {
    if (![self isInternetConnectionAvailable] && showAlert) {
        [self showAlertWithTitle:kCHGGenericErrorAlertTitle message:kCHGNoInternetConnectionAlertMessage];
    }
    
    return [self isInternetConnectionAvailable];
}

//#pragma mark - UIAlertViewDelegate
//
//- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
//{
//    
//}

#pragma mark -

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

@end
