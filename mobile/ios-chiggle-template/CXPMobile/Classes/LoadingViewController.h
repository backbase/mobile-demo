//
//  LoadingViewController.h
//  CXPMobile
//
//  Created by Oleksandr Perepelitsyn on 1/22/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingViewController : UIViewController

@property (nonatomic, copy) void (^retryBlock)(void);
@property (nonatomic, readonly) BOOL isAlertShown;

@end
