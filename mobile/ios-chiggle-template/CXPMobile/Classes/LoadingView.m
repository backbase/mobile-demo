//
//  LoadingView.m
//  CXPMobile
//
//  Created by Oleksandr Perepelitsyn on 1/27/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import "LoadingView.h"

@implementation LoadingView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        _color = [UIColor colorWithRed:254.0/255.0 green:212.0/255.0 blue:49.0/255.0 alpha:1.0]; // Default color
    }
    
    return self;
}

- (void)startAnimation {
    [self setupAnimationInLayer:self.layer size:self.bounds.size color:self.color];
}

- (void)stopAnimation {
    self.layer.sublayers = nil;
}

#pragma mark - Private methods

- (void)setupAnimationInLayer:(CALayer *)layer size:(CGSize)size color:(UIColor *)color {
    CFTimeInterval duration = 1.25;
    CFTimeInterval beginTime = CACurrentMediaTime();
    NSArray *beginTimes = @[@0.0, @0.2, @0.4];
    CAMediaTimingFunction *timingFunction = [CAMediaTimingFunction functionWithControlPoints:0.21 :0.53 :0.56 :0.8];
    
    // Scale animation
    CAKeyframeAnimation *scaleAnimation = [CAKeyframeAnimation new];
    scaleAnimation.keyPath = @"transform.scale";
    scaleAnimation.keyTimes = @[@0.0, @0.7];
    scaleAnimation.timingFunction = timingFunction;
    scaleAnimation.values = @[@0.0, @1.0];
    scaleAnimation.duration = duration;
    
    // Opacity animation
    CAKeyframeAnimation * opacityAnimation = [CAKeyframeAnimation new];
    opacityAnimation.keyPath = @"opacity";
    opacityAnimation.keyTimes = @[@0.0, @0.7, @1.0];
    opacityAnimation.timingFunctions = @[timingFunction, timingFunction];
    opacityAnimation.values = @[@1.0, @0.7, @0.0];
    opacityAnimation.duration = duration;
    
    // Animation
    CAAnimationGroup *animation = [CAAnimationGroup new];
    animation.animations = @[scaleAnimation, opacityAnimation];
    animation.duration = duration;
    animation.repeatCount = HUGE;
    animation.removedOnCompletion = NO;
    
    // Draw circles
    for (int i = 0; i < 3; i++) {
        CALayer *circle = [self createCircleLayerForSize:size color:color];
        CGRect frame = CGRectMake((layer.bounds.size.width - size.width) / 2, (layer.bounds.size.height - size.height) / 2, size.width, size.height);
        
        animation.beginTime = beginTime + (CFTimeInterval)[beginTimes[i] doubleValue];
        circle.frame = frame;
        [circle addAnimation:animation forKey:@"animation"];
        [layer addSublayer:circle];
    }
}

- (CALayer *)createCircleLayerForSize:(CGSize)size color:(UIColor *)color {
    CAShapeLayer *layer = [CAShapeLayer new];
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    [path addArcWithCenter:CGPointMake(size.width / 2, size.height / 2) radius:size.width / 2 startAngle:0.0 endAngle:(2.0 * M_PI) clockwise:NO];
    
    layer.fillColor = nil;
    layer.strokeColor = color.CGColor;
    layer.lineWidth = 2.0;
    layer.backgroundColor = nil;
    layer.path = path.CGPath;
    
    return layer;
}

@end
