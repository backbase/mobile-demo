//
//  CXPViewController.m
//  CXPMobile
//
//  Created by Backbase R&D B.V.
//

#import "CXPViewController.h"
#import "UIColor+HexString.h"
#import "SWRevealViewController.h"

@implementation CXPViewController

- (id)initWithRenderable:(NSObject<Renderable> *)renderable {
    self = [super init];
    if (self) {
        _page = renderable;
    }
    return self;
}

- (void)dealloc {
    // Unregister any observers
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setupNavigationBarTransparency];

    SWRevealViewController *revealController = [self revealViewController];
    if (revealController) {
        [revealController panGestureRecognizer];
        [revealController tapGestureRecognizer];
        
        UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"] style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
        self.navigationItem.leftBarButtonItem = revealButtonItem;
    }
    
    // Set background image if presented
    NSString *background = [self.page preferenceForKey:kCHGPropertyBackground];
    if (background) {
        UIImage *image = [UIImage imageNamed:background];
        if (image) {
            UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:image];
            backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
            backgroundImageView.frame = self.view.bounds;
            backgroundImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            backgroundImageView.clipsToBounds = YES;
            [self.view addSubview:backgroundImageView];
        }
    }
}

- (void)setupNavigationBarTransparency {
    self.automaticallyAdjustsScrollViewInsets = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setupNavBar];

    // Create renderable object
    NSObject<Renderable> *renderableObject = self.page;
    
    // Set back button title to blank string
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    // Create renderer
    NSError *error = nil;
    NSObject<Renderer> *renderer = [CXPRendererFactory rendererForItem:renderableObject error:&error];
    if (error || !renderer) {
        [CXP logError:self
              message:[NSString stringWithFormat:@"Error while creating renderer: %@",
                                                 error.localizedDescription ?: @"Unknown error"]];
        return;
    }

    // Render page
    BOOL result = [renderer start:self.view error:&error];
    if (!result || error) {
        [CXP logError:self
              message:[NSString stringWithFormat:@"Error while loading page: %@",
                                                 error.localizedDescription ?: @"Unknown error"]];
        return;
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    // Set status bar style when the view controller does not have a nav bar
    return UIStatusBarStyleLightContent;
}

- (void)setupNavBar
{
    // Check if page should be without nav bar
    NSString *shouldHideNavBar = [self.page preferenceForKey:kCHGPropertyShouldHideNavBar];
    if (shouldHideNavBar && [shouldHideNavBar isEqualToString:@"true"]) {
        // Hide nav bar
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        // Make view be present beneath status bar
        self.edgesForExtendedLayout = UIRectEdgeNone;
    } else {
        // Show nav bar
        [self.navigationController setNavigationBarHidden:NO animated:YES];
        self.edgesForExtendedLayout = UIRectEdgeAll;
        // Set navigation bar color
        [self customizeNavBar];
    }
}

- (void)customizeNavBar {
    // Set the title of the navigation item to match the page name
    // Check if there is a page preference (empty string is an option)
    NSString *navBarTitle = [self.page preferenceForKey:kCHGPropertyNavBarTitle];
    if (navBarTitle) {
        self.navigationItem.title = navBarTitle;
    } else {
        self.navigationItem.title = self.page.itemName;
    }
    // Set nav bar texts and coloring
    self.navigationController.navigationBar.backItem.title = @"";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    
    // Forces status bar to become UIStatusBarStyleLightContent
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack; // http://stackoverflow.com/questions/19022210/preferredstatusbarstyle-isnt-called/19513714#19513714
    
    // Set nav bar background color
    NSString *hexColorString = [self.page preferenceForKey:kCHGPropertyMobileNavBarColor];
    if (hexColorString) {
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:hexColorString];
    }
}

@end
