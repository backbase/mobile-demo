//
//  LoadingViewController.m
//  CXPMobile
//
//  Created by Oleksandr Perepelitsyn on 1/22/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import "LoadingViewController.h"
#import "LoadingView.h"

@interface LoadingViewController () <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet LoadingView *loadingIndicatorView;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, readwrite) BOOL isAlertShown;

@end

@implementation LoadingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.isAlertShown = NO;
    [self scheduleTimer];
    [self.loadingIndicatorView startAnimation];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self invalidateTimer];
    
    [super viewWillDisappear:animated];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)scheduleTimer
{
    self.timer = [NSTimer scheduledTimerWithTimeInterval:kCHGLoadingTimeout
                                                  target:self
                                                selector:@selector(timeoutElapsed:)
                                                userInfo:nil
                                                 repeats:NO];
}

- (void)invalidateTimer
{
    [self.timer invalidate];
    self.timer = nil;
}

- (void)timeoutElapsed:(NSTimer *)timer
{
    [self.loadingIndicatorView stopAnimation];
    [self showTimeoutAlertDialog];
}

- (void)showTimeoutAlertDialog
{    
    if ([UIAlertController class]) { // iOS 8,9
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:kCHGGenericErrorAlertTitle
                                                                       message:kCHGAppIsUnavailableAlertMessage
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *tryAgain = [UIAlertAction actionWithTitle:kCHGTryAgainTitle style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction *action) {
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                       [self dismissActions];
                                                   }];
        [alert addAction:tryAgain];
        [self presentViewController:alert animated:YES completion:nil];
    } else { // iOS 7 and older
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:kCHGGenericErrorAlertTitle
                                                         message:kCHGAppIsUnavailableAlertMessage
                                                        delegate:self
                                               cancelButtonTitle:kCHGTryAgainTitle
                                               otherButtonTitles:nil, nil];
        [alert show];
    }
    
    self.isAlertShown = YES;
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [self dismissActions];
}

- (void)dismissActions
{
    if (self.retryBlock) {
        self.retryBlock();
    }
    [self scheduleTimer];
    [self.loadingIndicatorView startAnimation];
    self.isAlertShown = NO;
}

@end
