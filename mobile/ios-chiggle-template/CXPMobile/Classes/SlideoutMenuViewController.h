//
//  SlideoutMenuViewController.h
//  CXPMobile
//
//  Created by Oleksandr Perepelitsyn on 2/8/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@interface SlideoutMenuViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, SWRevealViewControllerDelegate>

- (instancetype)initWithRenderables:(NSArray *)renderables;

@property (nonatomic, copy, readonly) NSArray *renderables;

@end
