/**
 *  ----------------------------------------------------------------
 *  Copyright © Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : main.js
 *  Description: ${widget.description}
 *  ----------------------------------------------------------------
 */

define(function(require, exports, module) {
    'use strict';

    function run(widget) {

        var $body = $(widget.body)

        var $img = $body.find('img');

        var imgStr = getBase64Image($img[0]);
        localStorage.img = imgStr


        enableIce(widget);
        // The widget needs to inform it's done loading so preloading works as expected
        gadgets.pubsub.publish('cxp.item.loaded', {
            id: widget.id
        });
    }


    function getBase64Image(img) {
        var canvas = document.createElement("canvas");
        canvas.width = img.width;
        canvas.height = img.height;

        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0);

        var dataURL = canvas.toDataURL("image/png");

        return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
    }



    function enableIce(widget) {

        if (typeof be !== 'undefined' && be.ice && bd && bd.designMode == 'true') {


            widget.iceConfig = be.ice.config;

            var isMasterpage = be.utils.module('top.bd.PageMgmtTree.selectedLink')['isMasterPage'],
                isManageable = isMasterpage || (
                    widget.model.manageable === 'true' ||
                    widget.model.manageable === '' ||
                    widget.model.manageable === undefined
                );

            if (isManageable && be.ice.controller) {
                var templateUrl = String(widget.getPreference('templateUrl')),
                    enableEditing = function() {

                        // it is possible to swap template for editorial
                        // here is an example for image template
                        if (templateUrl.match(/\/image\.html$/)) {
                            templateUrl = templateUrl.replace(/\/image\.html$/, '/image-editorial.html');
                        }

                        return be.ice.controller.edit(widget, templateUrl)
                            .then(function($dom) {
                                $(widget.body).find('.bp-g-include').html($dom);
                                return $dom;
                            });
                    };

                enableEditing();
            }

        } else {
            // Hide broken images on live
            $('img[src=""], img:not([src])', widget.body).addClass('be-ice-hide-image');
        }
    }



    module.exports = run;
});