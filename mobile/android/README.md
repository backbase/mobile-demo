# CXP Mobile - Android drawer template

## What is in the box
This project contain a fully functional Android app that can be used as starting point for any project.

### This repository contains:

- An Android app
- The latest version of the CXP Mobile SDK library
- Demo widgets

### Screenshots

![Alt text](./screenshots/welcome.png?raw=true "open the recently cloned project")

![Alt text](./screenshots/drawer.png?raw=true "open the recently cloned project")

![Alt text](./screenshots/changelog-list-view.png?raw=true "open the recently cloned project")

![Alt text](./screenshots/version-view.png?raw=true "open the recently cloned project")

## Prerequisites
- Java RE is up and running (run java -version on your terminal to validate this)
- A Git client is installed
- Android Studio

## Usage
- Clone this repo on your local machine
- Open up the project in Android Studio

![Alt text](./screenshots/android-studio.png?raw=true "open the recently cloned project")

- Run the app