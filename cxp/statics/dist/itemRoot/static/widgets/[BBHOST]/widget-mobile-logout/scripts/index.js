/**
 * ------------------------------------------------------------------------
 * widgetGwtLogin entry point
 * ------------------------------------------------------------------------
 */
(function(window, factory) {
    'use strict';
    var name = 'widgetGwtLogout';
    if (typeof module === 'object' && module.exports) {
        // only CommonJS-like environments that support module.exports,
        module.exports = factory(name);
    } else {
        // Browser globals (root is window)
        window[name] = factory(name);
    }
}(this, function(name) {

    'use strict';

    var bus = window.gadgets.pubsub;
    var ng = window.angular;

    /**
     * Main Controller
     * @param {object} widget cxp widget instance
     * @param {object} $http angular ajax request library
     */
    
   function MainCtrl(widget, $http) {
        var ctrl = this; //self this controller

        function buildRequest() {
            var serverUrl = b$.portal.config.serverRoot;
            var LOGOUTENDPOINT = 'j_spring_security_logout';

            var req = {
                method: 'POST',
                url: serverUrl + '/' + LOGOUTENDPOINT,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json',
                    'Req-X-Auth-Token': 'JWT'
                },
                //https://docs.angularjs.org/api/ng/service/$httpParamSerializerJQLike
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                }
            };

            return req;
        }

        ctrl.logout = function() {
            var req = buildRequest();

            $http(req)
                .success(function(res) {
                    bus.publish('navigation:user-auth');
                });
        };

    };


    /**
     * Error Controller
     * Binds the widget errors to the view
     * @param {object} widget cxp widget instance
     */
    function ErrorCtrl(widget) {}

    /**
     * Create Angular Module
     * @param  {object} widget widget instance
     * @param  {array} deps   angular modules dependencies
     * @return {object}        angular module
     */
    function createModule(widget, deps) {
        return ng.module(name, deps || [])
            .value('widget', widget)
            .controller('MainCtrl', ['widget', '$http', MainCtrl])
            .controller('ErrorCtrl', ['widget', ErrorCtrl]);
    }

    /**
     * Main Widget function
     * @param  {object} widget instance
     * @return {object}        Application object
     * @public
     */
    function App(widget, deps) {

        gadgets.pubsub.publish('cxp.item.loaded', {
            id: widget.model.name
        });


        var obj = Object.create(App.prototype);
        var args = Array.prototype.slice.call(arguments);
        obj.widget = widget;
        obj.module = createModule.apply(obj, args);
        return obj;
    }

    /**
     * Widget proto
     * @type {object}
     */
    App.prototype = {
        bootstrap: function() {
            ng.bootstrap(this.widget.body, [this.module.name]);
            return this;
        },
        destroy: function() {
            this.module = null;
        }
    };

    return App;
}));
