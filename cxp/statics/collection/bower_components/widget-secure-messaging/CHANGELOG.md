### v2.1.7 - `30/09/2015, 3:05pm`
* - lazy loaded Angular custom directives are broken in IE8  

### v2.1.6 - `28/08/2015, 2:44pm`
* Fix chrome property in model.xml  


### v2.1.5 - `25/08/2015, 10:42pm`
#### add tag into info.json for styleguide filtering  
* add tag for styleguide menu filtering  


### v2.1.4 - `20/08/2015, 3:37pm`
* Add cxp.item.loaded event Build dist assets  
* Add cxp.item.loaded event  


### v2.1.3 - `10/08/2015, 5:59pm`
#### Remove repository from bower.json  


### v2.1.2 - `30/07/2015, 5:32pm`
#### Update icon preference to use itemRoot.  
* Fix reference to chrome in model.xml  
* LF-156: Clean up model.xml for 5.6 compatability.  
* LF-111-remove-b$-from-widget-secure-messaging  


### v2.1.1 - `29/07/2015, 6:29pm`
* Fix reference to chrome in model.xml  
* LF-156: Clean up model.xml for 5.6 compatability.  
* LF-111-remove-b$-from-widget-secure-messaging  


### v2.1.0 - `29/07/2015, 5:39pm`
#### Update model.xml for CXP 5.6 compatibility  
* LF-156: Clean up model.xml for 5.6 compatability.  
* LF-111-remove-b$-from-widget-secure-messaging  


## [2.0.0] - 2015-05-12 (note: generated from git logs)

 - EBANK-247 Show sent messages in sent list immediatly. - Fix sent message delete problems
 - LPES-3657: i18n: added sk-SK
 - EBANK-247: Updating dist after build.
 - NoJira - Fix CSS problems with message content wraping
 - NoJira - fix message content wrap problems
 - use lpWidget
 - EBANK-247 Add Sent Letters to template. - Fetch sent threads - Extend factory to support sent threads GET
 - EBANK-247 Refactor widget to run standalone
