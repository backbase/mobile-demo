# Launchpad Retail Collection

Frontend dependency management for Launchpad items. This repo is used by LPC/Packages repo
to assemble and deploy to artifactory an "Retail Package".

## Versioning

All static assets widgets/features/containers/pages use semantic versioning. The versions
are defined in bower.json, as well as the versions of the dependencies. This could be
switched to package.json (and npm) when NPM supports a flat dependency structure like bower does.

When a bundle of assets is stable it's possible to give each asset a single version, for
example Launchpad will tag stable static assets release every two weeks with LP-0.13.X.