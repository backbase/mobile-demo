### v3.0.6 - `30/09/2015, 5:57pm`
* Fix mistake in property  
* Change showFooter by hideFooter to not break the previous behaviour on portal  


### v3.0.5 - `30/09/2015, 3:05pm`
* - new.transfer.html template content is moved into index.html. We had g:incude there, which is a part of serverside rendering, so it cant be in a template;  

### v3.0.4 - `29/09/2015, 10:04am`
* Be able to remove the footer buttons to be used from native buttons. Add pubsub to submit the transfer if the footer buttons are  not shown. Send the loaded event to notify the Mobile SDK Add the index.dev.html so that it can be tested in local  


### v3.0.3 - `28/09/2015, 5:55pm`
* LF-369 migrate form persistance from core  

### v3.0.2 - `25/08/2015, 10:41pm`
#### add tag into info.json for styleguide filtering  
* add tag for styleguide menu filtering  


### v3.0.1 - `10/08/2015, 5:59pm`
#### Remove repository from bower.json  


### v3.0.0 - `07/08/2015, 4:54pm`
* LF-162: Updated new-transfer widget to be compatible with module-transactions 2.  
* add missing template and fix model.xml path  


### v2.2.1 - `29/07/2015, 6:29pm`
* Fix reference to chrome in model.xml  
* LF-156: Clean up model.xml for 5.6 compatability.  


### v2.2.0 - `29/07/2015, 5:39pm`
#### Update model.xml for CXP 5.6 compatibility  
* LF-156: Clean up model.xml for 5.6 compatability.  


## [2.0.0] - 2015-05-12 (note: generated from git logs)

 - LPES-3664: disable currency selection issue
 - LPES-3657: i18n: added sk-SK
 - add previous changes
 - use lpWidget
 - LPES-3539: del edit
