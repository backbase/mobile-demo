/**
*  ----------------------------------------------------------------
*  Copyright © Backbase B.V.
*  ----------------------------------------------------------------
*  Author : Backbase R&D - Amsterdam - New York
*/

'use strict';

var utils = global.utils;

module.exports = function(config) {

    config = config || {
        name: 'widget-new-transfer',
        title: 'New Transfer'
    };

    var widget = this;

    widget.name = config.name;
    widget.title = config.title;
    /**
    * Prepare all elements
    * @return {promise} Return widget.elements
    */
    widget.get = function() {
      var d = utils.q.defer();
        utils.getWidget(widget.title).then(function(res) {
            widget.chrome = res.chrome;
            widget.body = res.body;
            d.resolve(widget);
        });
        return d.promise;
    };

    /**
    * The widget should be visible on the page
    * @return {Boolean}
    */
    widget.isVisible = function () {
      return widget.body.isDisplayed();
    };

    widget.selectedAccountName = function () {
        return widget.body.element(by.name('accountId')).element(by.css('.lp-acct-name')).getText();
    };

  //clicking on account drop-down to see the list op available accounts
    widget.clickAccDropdown = function() {
        widget.body.element(by.css('.lp-large-account-select-size button')).click();
    };

  //the list of Sara's accounts in drop-down
    widget.accountsList = function(index) {
        var accounts = by.repeater('option in group.options');
        if(utils.isNumber(index)) {
            return widget.body.element(accounts.row(index));
        } else {
            return widget.body.all(accounts);
        }
    };

  //clicking on country drop-down
    widget.clickCurrencyDropdown = function() {
        return widget.body.element(by.css('.lp-currency-amount-input .currency-select button')).click();

    };

  //returns the list of countries for IBAN
    widget.currencyList = function(index) {

        var curr = by.css('div.currency-select a');
        if(utils.isNumber(index)) {
            return widget.body.element(curr.row(index));
        } else {
            return widget.body.all(curr);
        }
    };

    //type in random counterparty name
    widget.typeRandomCounterpartyName = function() {
        var field = element(by.model('counterpartyName'));
        field.clear();
        field.sendKeys(utils.randomString());
    };

    //clicking on toggleShowContacts in counter-party-filter to reveal the available contacts
    widget.getContacts = function() {
        return widget.body.element(by.css('[ng-click="toggleShowContacts()"]')).click();
    };

    //returns the list of contacts
    widget.contactList = function(index) {
        var contacts = by.repeater('contact in counterpartyList');
        if(utils.isNumber(index)) {
            return widget.body.element(contacts.row(index));
        } else {
            return widget.body.all(contacts);
        }
    };

    //returns the list of accounts in accounts drop-down
    widget.listSarasAccounts = function(index) {
        var accountItems = by.repeater('account in contact.accounts');
        if(utils.isNumber(index)) {
            return widget.body.element(accountItems.row(index));
        } else {
            return widget.body.all(accountItems);
        }
    };

    //opening the list of Lisa's accounts in counterpartyList
    widget.openAllAccounts = function() {
        var toggle = widget.body.all(by.css('p.toggle-accounts')).then(function(elm){
            elm[0].click();
        });
    };

	widget.closeAllAccounts = function() {
		var toggle = widget.body.all(by.css('p.toggle-accounts')).then(function(elm){
			elm[1].click();
		});
	};

	widget.selectBusinessAccount = function() {
		widget.getContacts();
		widget.openAllAccounts();
		widget.listSarasAccounts(1).click();
		widget.getContacts();
		widget.closeAllAccounts();
	};
	widget.selectPersonalAccount = function() {
		widget.getContacts();
		widget.openAllAccounts();
		widget.listSarasAccounts(0).click();
		widget.getContacts();
		widget.closeAllAccounts();
	};

  //clicking on IBAN country dropdown
	widget.getCountries = function() {
		return widget.body.element(by.css('.lp-iban-country-dropdown button')).click();
	};

  //returns the list of countries
	widget.countryList = function(index) {
		var countries = by.repeater('option in group.options track by $index');
		if(utils.isNumber(index)) {
			return widget.body.element(countries.row(index));
		} else {
			return widget.body.all(countries);
		}
	};

  //search field in the coutry list drop-down
	widget.isSearchCountriesDisplayed = function() {
		return widget.body.element(by.model('filter.value')).isDisplayed();
	};

  //clicking on calendar in One Time tab
	widget.getCalendar = function() {
		return widget.body.element(by.name('scheduleDate')).click();
	};

	//the calendar in One Time tab
	widget.isDatePickerDisplayedOneTimeTab = function() {
		return widget.body.element(by.xpath('(//*[@ng-model="date"])[1]')).isDisplayed();
	};

  //the calendar in Scheduled tab
  widget.isDatePickerDisplayedScheduledTab = function() {
		return widget.body.element(by.xpath('(//*[@ng-model="date"])[2]')).isDisplayed();
	};

  //switching to Scheduled tab
	widget.getScheduledTab = function() {
		return widget.body.element(by.css('[ng-click="setScheduledTransfer(\'scheduled\')"]')).click();
	};

  //the Frequency button is present
	widget.isFrequencyDisplayed = function() {
		return widget.body.element(by.css('.lp-st-frequency button')).isDisplayed();
	};

	//clicking on Frequency button
	widget.clickFrequency = function() {
		return widget.body.element(by.css('.lp-st-frequency button')).click();
	};

  //return list of items in the Frequency drop-down. This function can be called after widget.getFrequency() is called.
	widget.frequencyList = function(index) {
		var items = by.repeater('option in group.options');
		if(utils.isNumber(index)) {
			return widget.body.element(items.row(index));
		} else {
			return widget.body.all(items);
		}
	};

  //open calendar to select start date
	widget.startDateCalendar = function() {
		return widget.body.element(by.css('[ng-click="openStartCalendar($event)"]')).click();
	};

  //switch to one-time payment form
	widget.getOneTimeTab = function() {
		return widget.body.element(by.css('[ng-click="setScheduledTransfer(\'one-time\')"]')).click();
	};

	//open Email tab
	widget.clickEmailTab = function() {
		widget.body.element(by.css('[active="activeTransferTab.p2pEmail"]')).click();
	};

	//email field input
	widget.typeRandomEmailAddress = function() {
		var email = widget.body.element(by.model('email'));
		email.clear();
		email.sendKeys(utils.randomString() + '@backbase.com');
	};

	/**
	INPUT FIELDS
  **/

	//Whole amount field
  widget.wholeAmount = function() {
		return widget.body.element(by.model('wholeAmount'));
  };

	//Type in random amount in range 0-1000
	widget.sendWholeAmount = function() {
		widget.wholeAmount().clear();
		widget.wholeAmount().sendKeys(utils.random(0, 1000));
	};
	//Type in random amount in range 1000-100000
	widget.sendWholeBigAmount = function() {
		widget.wholeAmount().clear();
		widget.wholeAmount().sendKeys(utils.random(1000, 100000));
	};

  //Coins field
  widget.decimalAmount = function() {
		return widget.body.element(by.model('decimalAmount'));
	};

	//Type in random amount
	widget.sendDecimalAmount = function() {
		widget.decimalAmount().clear();
		widget.decimalAmount().sendKeys(utils.random(0, 99));
	};


	//Select random account
	widget.randomAccount = function() {
		widget.clickAccDropdown();
		var randomAccountItem = widget.accountsList().get(utils.random(0, 1));
		randomAccountItem.click();
	};

	//Sending random currency

	widget.sendRandomCurrency = function() {
		widget.clickCurrencyDropdown();
		var randomCurrencyItem = widget.currencyList().get(utils.random(0, 5));
		randomCurrencyItem.click();
	};

	//random clicks to submit random amount
	widget.randomClicks = function() {

			var arrayRandomClicks = [

				'randomAccount',
				'sendWholeAmount',
				'sendDecimalAmount',
				'selectBusinessAccount',
				'sendRandomCurrency'
			];

			var arrayLength = arrayRandomClicks.length;

			utils.shuffle(arrayRandomClicks).forEach(function(fn, i){
				this[fn]();
			}.bind(this));
	};

	//Submit transaction
	widget.submitTransaction = function() {
		return widget.body.element(by.name('submitForm')).click();
	};

};
