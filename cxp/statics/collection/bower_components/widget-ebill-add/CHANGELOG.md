### v2.1.5 - `30/09/2015, 3:05pm`
* LPMAINT-35: fix lp-template for ie8  
* - lazy loaded Angular custom directives are broken in IE8  
* Fix the index.dev.html. Fix a problem when there is no error that was breaking the widget. Change the styles to add space in the payment form between the description text-area and the buttons.  

### v2.1.4 - `25/08/2015, 10:41pm`
#### add tag into info.json for styleguide filtering  
* add tag for styleguide menu filtering  


### v2.1.3 - `20/08/2015, 3:37pm`
* Add cxp.item.loaded event Build dist assets  
* Add cxp.item.loaded event  


### v2.1.2 - `10/08/2015, 5:59pm`
#### Remove repository from bower.json  


### v2.1.1 - `29/07/2015, 6:29pm`
* Fix reference to chrome in model.xml  
* LF-156: Clean up model.xml for 5.6 compatability.  
* Add cxp.item.loaded pubsub event  


### v2.1.0 - `29/07/2015, 5:39pm`
#### Update model.xml for CXP 5.6 compatibility  
* LF-156: Clean up model.xml for 5.6 compatability.  
* Add cxp.item.loaded pubsub event  


### v 1.0.0
* Initial release
## [2.0.0] - 2015-05-12 (note: generated from git logs)

 - LPES-3657: i18n: added sk-SK
 - use lpWidget
 - EBANK-217 Fix styles for standalone widgets
 - EBANK-217 Update README with new config endpoints and dependencies
 - EBANK-217 Remove unexisting stylesheets
 - EBANK-231 Support scheduled transfer until cancelation - Move components to ebilling module - Show delivery date estimate - Add icon
 - EBANK-229 Add success processed payment screen - Merge urgent and normal payment functions - Add payment memo text field - Move add payee success screen to e-billing wizard step - Indentation fix
 - EBANK-234 Extend scheduled-transfer component to support custom frequency and end option lists
 - EBANK-218 Display estimated delivery date
 - EABNK-218 Urgent transfer improvements and add business days calendar endpoint
 - EBANK-227 Add Urgent transfer support
 - EBANK-230 Display errors returned by the payment service
 - EBANK-227 Use module-billing to display amount values.
 - EBANK-218 Create Payment box components. Add 4th wizard step to setup payments
 - EBANK-217 Fix for translation and api resources
 - EBANK-217 Initial incomplete pay component version
