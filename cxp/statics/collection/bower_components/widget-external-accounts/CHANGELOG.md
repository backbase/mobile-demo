### v2.1.3 - `25/08/2015, 10:41pm`
#### add tag into info.json for styleguide filtering  
* add tag for styleguide menu filtering  


### v2.1.2 - `10/08/2015, 5:59pm`
#### Remove repository from bower.json  


### v2.1.1 - `29/07/2015, 6:29pm`
* Fix reference to chrome in model.xml  
* LF-156: Clean up model.xml for 5.6 compatability.  


### v2.1.0 - `29/07/2015, 5:39pm`
#### Update model.xml for CXP 5.6 compatibility  
* LF-156: Clean up model.xml for 5.6 compatability.  


## [2.0.0] - 2015-05-12 (note: generated from git logs)

 - remove widget Instance, not used
 - use lpWidget
 - LPES-3348: i18n
