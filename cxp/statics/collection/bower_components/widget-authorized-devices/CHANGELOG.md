### v1.2.1 - `24/09/2015, 11:50am`
* NGUSEOLB-561: Use font awesome trash icon  

### v1.2.0 - `28/08/2015, 3:45pm`
* NGUSEOLB-368: Add template for mobile modal dialog
* NGUSEOLB-368: Change design of authorized devices widget
* NGUSEOLB-368: Fix standalone development mode


### v1.1.3 - `25/08/2015, 10:41pm`
#### add tag into info.json for styleguide filtering
* add tag for styleguide menu filtering


### v1.1.2 - `10/08/2015, 5:59pm`
#### Remove repository from bower.json


### v1.1.1 - `29/07/2015, 6:28pm`
* Fix reference to chrome in model.xml
* LF-156: Clean up model.xml for 5.6 compatability.


### v1.1.0 - `29/07/2015, 5:39pm`
#### Update model.xml for CXP 5.6 compatibility
* LF-156: Clean up model.xml for 5.6 compatability.


### v 1.0.0

* Initial release
