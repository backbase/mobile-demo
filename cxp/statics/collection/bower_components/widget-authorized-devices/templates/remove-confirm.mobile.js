define(function(require, exports, module) {
    'use strict';

    module.exports = [
    '<div class="authorized-devices-modal-mobile">',
        '<div class="panel-body">',
            '<div class="modal-body">',
                '<div class="content">',
                    '<span lp-i18n="This device will be removed from your authorized devices."></span>',
                    '<span lp-i18n="We will ask for a one-time passcode next time you access online banking from this device."></span>',
                    '<span lp-i18n="Do you want to delete this device?"></span>',
                '</div>',
                '<button type="button" ng-click="remove()" class="btn btn-mobile remove" lp-i18n="Delete device"></button>',
            '</div>',
            '<div class="modal-footer">',
                '<button type="button" ng-click="cancel()" class="btn btn-mobile cancel" lp-i18n="Cancel"></button>',
            '</div>',
        '</div>',
    '</div>'
    ].join('');
});
