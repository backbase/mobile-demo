define(function(require, exports, module) {
    'use strict';

    module.exports = [
    '<div class="authorized-devices-modal">',
        '<div class="panel-body">',
            '<div class="modal-header" lp-i18n="Confirm delete device"></div>',
            '<div class="modal-body">',
                '<p lp-i18n="This device will be removed from your authorized devices."></p>',
                '<p lp-i18n="We will ask for a one-time passcode next time you access online banking from this device."></p>',
                '<p lp-i18n="Do you want to delete this device?"></p>',
            '</div>',
            '<div class="modal-footer">',
                '<button type="button" ng-click="cancel()" class="btn btn-link" lp-i18n="Cancel"></button>',
                '<button type="button" ng-click="remove()" class="btn btn-primary" lp-i18n="Delete device"></button>',
            '</div>',
        '</div>',
    '</div>'
    ].join('');
});
