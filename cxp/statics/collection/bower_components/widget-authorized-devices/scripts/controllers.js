define(function(require, exports) {

    'use strict';

    // @ngInject
    exports.MainCtrl = function(lpWidget, $scope, $modal, lpDevices, lpCoreUtils, lpCoreError) {

        var ctrl = this;
        var lpDevicesModel = lpDevices.api();
        ctrl.deviceRevokeEnabled = lpWidget.getPreference('deviceRevokeEnabled') || false;
        ctrl.noDevicesFound = false;
        ctrl.devicesLoading = false;
        ctrl.devicesLoadingError = false;

        // store for devices list
        ctrl.devices = [];

        // load all devices from server
        var loadDevices = function () {
            ctrl.devicesLoading = true;
            ctrl.devicesLoadingError = false;

            lpDevicesModel.getAll()
                .then(function(devices) {
                    ctrl.devices = devices;
                    ctrl.devicesLoading = false;
                    if (devices && devices.length) {
                        ctrl.noDevicesFound = false;
                    } else {
                        ctrl.noDevicesFound = true;
                    }
                }, function(error) {
                    ctrl.devicesLoading = false;
                    ctrl.devicesLoadingError = true;
                    lpCoreError.captureException(error);
                });
        };

        var removeConfirmTmpl = '';
        if (lpCoreUtils.isMobileDevice()) {
            removeConfirmTmpl = require('../templates/remove-confirm.mobile');
        } else {
            removeConfirmTmpl = require('../templates/remove-confirm');
        }
        ctrl.showRemoveConfirm = function(item) {
            var modalInstance = $modal.open({
                template: removeConfirmTmpl,
                controller: 'RemoveConfirmationCtrl',
                windowClass: lpCoreUtils.isMobileDevice() ? 'modal-mobile' : '',
                resolve: {
                    item: function() {
                        return item;
                    }
                }
            });

            modalInstance.result.then(ctrl.removeAuthorizedDevice);
        };

        // revoke specific device from list
        ctrl.removeAuthorizedDevice = function(device) {
            lpDevicesModel.revoke(device.id)
                .then(function() {
                    loadDevices();
                }, function(error) {
                    lpCoreError.captureException(error);
                });
        };

        loadDevices();
    };

    // @ngInject
    exports.RemoveConfirmationCtrl = function($scope, $modalInstance, item) {
        $scope.remove = function() {
            $modalInstance.close(item);
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    };
});
