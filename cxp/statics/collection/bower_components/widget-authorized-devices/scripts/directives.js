/**
 * Directives
 * @module directives
 */
define(function (require, exports, module) {
    'use strict';

    // @ngInject
    exports.lpYesNoButton = function ($templateCache) {

        $templateCache.put('lp-yes-no-button.html',
            '<div ng-show="show">' +
            '   <button ng-show="!trigger" ng-click="(trigger = true)" class="btn btn-link">{{ label | translate }}</button>' +
            '   <div ng-hide="!trigger">' +
            '       <div class="btn btn-link" ng-click="(trigger = false)">' +
            '           <div class="button-content" lp-i18n="Cancel"></div>' +
            '       </div>' +
            '       <div class="btn btn-danger" ng-click="callback()">' +
            '           <div class="button-content">{{ label | translate }}</div>' +
            '       </div>' +
            '   </div>' +
            '</div>'
        );

        return {
            restrict: 'AE',
            scope: {
                show: '=',
                label: '@',
                callback: '&'
            },
            template: $templateCache.get('lp-yes-no-button.html')
        };
    };

});
