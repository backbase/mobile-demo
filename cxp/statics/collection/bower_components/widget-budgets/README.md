# Budgets

## Information

| name                  | version           | bundle           |
| ----------------------|:-----------------:| ----------------:|
| widget-budgets        | 2.3.0             | Banking          |

## Brief Description

The information on this page will be completed shortly.

## Dependencies

* base
* core
* ui

## Dev Dependencies

* angular-mocks ~1.2.28
* config

## Preferences


##Events