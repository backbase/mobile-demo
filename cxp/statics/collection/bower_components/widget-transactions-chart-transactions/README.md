# Transactions Charts

## Information

| name                                      | version           | bundle         |
| ------------------------------------------|:-----------------:| --------------:|
| widget-transactions-chart-transactions    | 2.0.2             | Banking        |

## Brief Description


## Dependencies

* [core][core-url]
* [module-transactions][module-transactions-url]

## Dev Dependencies

* [angular-mocks ~1.2.28][angular-mocks-url]
* [config][config-url]

## Preferences


[core-url]: http://stash.backbase.com:7990/projects/lpm/repos/foundation-core/browse/
[config-url]: https://stash.backbase.com/projects/LP/repos/config/browse
[angular-mocks-url]: https://github.com/angular/bower-angular-mocks/
[module-transactions-url]: https://stash.backbase.com/projects/LPM/repos/module-transactions/browse/
