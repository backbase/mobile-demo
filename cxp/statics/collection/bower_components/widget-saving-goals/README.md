# Saving Goals

## Information

| name                  | version           | bundle           |
| ----------------------|:-----------------:| ----------------:|
| widget-saving-goals    | 2.3.0            | PFM              |

## Brief Description

Provides the ability to create and manage saving goals, define an amount and a target date.

## Dependencies

* base
* core
* ui

## Dev Dependencies

* angular-mocks ~1.2.28
* config

## Preferences

Get widget preference `widget.getPreference(string)`

* **savingAccountsDataSrc**: The endpoint URL to retrieve saving accounts data
* **savingGoalsDataSrc**: The endpoint URL to retrieve saving goals data
* **minMonths**: Defines the minimum duration of a savings goal
* **showAccountSelect**: Show/Hide account selection option


##Events

_This widget does not subscribe/publish any events._

## Test

## Build
