# E-Bill Inbox

## Information

| name                  | version           | bundle           |
| ----------------------|:-----------------:| ----------------:|
| widget-ebill-inbox    | 2.1.5 			| Ebilling        |

## Brief Description

Provides the ability to accept new e-Bill requests, view unpaid e-Bills, pay e-Bills and also decline. Moreover, it displays additional information about the selected e-Bill, such as Payee, Bill Reference Number, and Amount.

## Dependencies

* base
* core
* ui
* module-ebilling

## Dev Dependencies

* angular-mocks ~1.2.28
* config

## Preferences

Get widget preference `widget.getPreference(string)`

* **accountsDataSrc**: The URL endpoint to retrieve the list of accounts
* **debitOrdersSrc**: The URL endpoint to retrieve the list of debit orders
* **mandatesSrc**: The URL endpoint to retrieve the list of mandates
   

##Events

The following is a list of pub/sub event which the widget subscribes to:

_This widget does not subscribe to any events._

The following is a list of pub/sub event which the widget publishes to:

* **lp.widget.e-bill-inbox:sync** - Published when the widget refreshes

## Templates

Widget uses templates with the following keys:

* **ebill-inbox** - Main widget template.
* **details** - Bill details template.
* **newbills-details** - New bills details.
* **payment** - Payment tab template.

To redefine template create preference with this format: widgetTemplate_{templateKey}.

For example, for main template create property `widgetTemplate_ebill-inbox` with the value equal to a path to load template from. The path can either be local relative path or external absolute path (http:// and https:// protocols).