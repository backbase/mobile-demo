/**
 *  ----------------------------------------------------------------
 *  Copyright © Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : main.spec.js
 *  Description:
 *  ----------------------------------------------------------------
 */

var widget = require('../../scripts/main');

require('angular-mocks');

var ngModule = window.module;
var ngInject = window.inject;
// Mock __WIDGET__ object
var Widget = require('./widget.mock');

// mocks
var accounts = require('./mocks/accounts');

/*----------------------------------------------------------------*/
/* Widget unit tests
/*----------------------------------------------------------------*/
describe('Widget accounts dropdown', function() {
    var lpWidget;
    /*----------------------------------------------------------------*/
    /* Mock modules/Providers
    /*----------------------------------------------------------------*/
    
    beforeEach(ngModule(widget.name, function($provide) {
        $provide.value('lpWidget',  new Widget());
    }));

    /*----------------------------------------------------------------*/
    /* Main Module
    /*----------------------------------------------------------------*/
    describe('Module', function() {
        it('should be an object', function() {
            expect(widget).toBeObject();
        });
    });

    /*----------------------------------------------------------------*/
    /* UNIT TEST for Controllers
    /*----------------------------------------------------------------*/
    describe('Controllers', function() {

        var createController, scope, ctrl;

        beforeEach(inject(function($controller, $rootScope) {
            scope = $rootScope.$new();
            createController = function(ctrlName) {
                return $controller(ctrlName, {
                    $scope: scope
                });
            };
        }));

        // AccountsDropdownController
        describe('AccountsDropdownController', function() {
            beforeEach(function(){
                ctrl = createController('AccountsDropdownController');
                ctrl.model = {
                    accounts: accounts,
                    selected: null
                };
            });

            it('should exists', function() {
                expect(ctrl).toBeObject();
            });

            it('AccountsDropdownModel should be defined', inject(function(AccountsDropdownModel) {
                expect(AccountsDropdownModel).toBeDefined();
            }));

            it('AccountsDropdownModel has loadAccounts function', inject(function(AccountsDropdownModel) {
                expect(AccountsDropdownModel.loadAccounts).toBeFunction();
            }));

            it('AccountsDropdownModel: loadAccounts returns promise', inject(function(AccountsDropdownModel) {
                var promise = AccountsDropdownModel.loadAccounts();
                expect('then' in promise).toBeTrue();
            }));

            it('AccountsDropdownUtils should be defined', inject(function(AccountsDropdownUtils) {
                expect(AccountsDropdownUtils).toBeDefined();
            }));

            it('AccountsDropdownUtils should return AA id', inject(function(AccountsDropdownUtils) {
                expect(AccountsDropdownUtils.getAllAccountsId).toBeFunction();
                expect(AccountsDropdownUtils.getAllAccountsId()).toBeString();
            }));

            it('AccountsDropdownUtils should add AA item', inject(function(AccountsDropdownUtils) {
                var fnAdd = AccountsDropdownUtils.addAllAccountsItem;
                var fnId = AccountsDropdownUtils.getAllAccountsId;
                
                expect(fnAdd).toBeFunction();
                expect(fnAdd([])).toBeArray();
                expect(fnAdd([]).length === 1).toBeTrue();
                expect(fnAdd([])[0].id === fnId()).toBeTrue();
            }));
        });

    });

});

