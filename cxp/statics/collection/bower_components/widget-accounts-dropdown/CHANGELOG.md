### v1.5.3 - `25/08/2015, 10:41pm`
#### add tag into info.json for styleguide filtering  
* add tag for styleguide menu filtering  


### v1.5.2 - `20/08/2015, 11:01am`
#### add mock and theme for standalone dev  


### v1.5.1 - `10/08/2015, 5:59pm`
#### Remove repository from bower.json  


### v1.5.0 - `04/08/2015, 4:57pm`
* Add `cxp.item.loaded` event


### v1.4.1 - `29/07/2015, 6:28pm`
* Fix reference to chrome in model.xml
* LF-156: Clean up model.xml for 5.6 compatability.


### v1.4.0 - `29/07/2015, 5:39pm`
#### Update model.xml for CXP 5.6 compatibility
* LF-156: Clean up model.xml for 5.6 compatability.


## [1.3.0]
 - Add possibility to select an account programmatically

## [1.2.2]
 - Fix `showAllAccountsItem` preference

## [1.2.1]
 - Fix checking if all accounts special account is selected

## [1.0.0]
 - Initial release
