define(function (require, exports, module) {
    'use strict';

    // @ngInject
    exports.AccountsDropdownModel = function (lpWidget, $q, lpCoreUtils, lpAccounts, AccountsDropdownUtils, lpCoreBus, lpCoreError) {

        var initialAccountId = lpWidget.getPreference('initialAccountId') || '';
        var showAllAccountsItem = lpCoreUtils.parseBoolean(lpWidget.getPreference('showAllAccountsItem')) || false;

        var model = {
            accounts: [],
            selected: {}
        };

        var findAccountById = function (id) {
            return model.accounts.filter(function (account) { return account.id === id; })[0] || null;
        };

        // loading accounts list and isolate it from sharing
        var loadAccounts = function () {
            var deferred = $q.defer();

            lpAccounts.load().then(function (accounts) {
                accounts = lpCoreUtils.clone(accounts);

                if (showAllAccountsItem) {
                    accounts = AccountsDropdownUtils.addAllAccountsItem(accounts);
                }

                model.accounts = accounts;

                // select initial account (from preference - if any)
                if (initialAccountId) {
                    model.selected = findAccountById(initialAccountId);
                } else if (showAllAccountsItem) {
                    model.selected = findAccountById(AccountsDropdownUtils.getAllAccountsId());
                } else if (accounts.length) {
                    model.selected = accounts[0];
                }

                lpCoreBus.subscribe('launchpad-retail.accountSelected', function (data) {
                    var account = findAccountById(data.accountId);
                    if (account && account !== model.selected) {
                        model.selected = account;
                        lpCoreBus.publish('accounts-dropdown.select-account', account);
                    }
                });

                lpCoreBus.subscribe('launchpad-retail.cardSelected', function (data) {
                    var account = findAccountById(data.account.id);
                    if (account && account !== model.selected) {
                        model.selected = account;
                        lpCoreBus.publish('accounts-dropdown.select-account', account);
                    }
                });

                deferred.resolve(model);
            }, function (err) {
                deferred.reject(err);
            });

            // Provide backward compatibility
            deferred.promise.success = deferred.promise.then;
            deferred.promise.error = deferred.promise['catch'];

            return deferred.promise;
        };

        // API
        return {
            loadAccounts: loadAccounts
        };
    };
});

