### v2.0.3 - `30/09/2015, 3:05pm`
* - lazy loaded Angular custom directives are broken in IE8  

### v2.0.2 - `25/08/2015, 10:42pm`
#### add tag into info.json for styleguide filtering  
* add tag for styleguide menu filtering  


### v2.0.1 - `10/08/2015, 6:00pm`
#### Remove repository from bower.json  


### v2.0.0 - `07/08/2015, 5:23pm`
#### Deprecate module-transactions-2.  

### v1.3.2 - `31/07/2015, 4:34pm`
* NGUSEOLB-129: put relative path to module transactions base styles  
* fix modeule-transactions-2  

### v1.3.1 - `29/07/2015, 6:29pm`
* Fix reference to chrome in model.xml  
* LF-156: Clean up model.xml for 5.6 compatability.  


### v1.3.0 - `29/07/2015, 5:39pm`
#### Update model.xml for CXP 5.6 compatibility  
* LF-156: Clean up model.xml for 5.6 compatability.  


## [1.2.1]

 - Fix show transaction icons


## [1.0.0]
 - Initial release
