### v3.0.4 - `30/09/2015, 6:02pm`
* Change showFooter by hideFooter to not break the previous behaviour on portal  


### v3.0.3 - `29/09/2015, 1:14pm`
* Be able to remove the footer buttons to be used as native in the app Add the notification for mobile sdk to know what the widget is loaded.  


### v3.0.2 - `25/08/2015, 10:42pm`
#### add tag into info.json for styleguide filtering  
* add tag for styleguide menu filtering  


### v3.0.1 - `10/08/2015, 5:59pm`
#### Remove repository from bower.json  


### v3.0.0 - `07/08/2015, 1:20pm`
* LF-205: Support newest module transactions  


### v2.1.1 - `29/07/2015, 6:29pm`
* Fix reference to chrome in model.xml  
* LF-156: Clean up model.xml for 5.6 compatability.  


### v2.1.0 - `29/07/2015, 5:39pm`
#### Update model.xml for CXP 5.6 compatibility  
* LF-156: Clean up model.xml for 5.6 compatability.  


### v 1.0.0
* Initial release
## [2.0.0] - 2015-05-12 (note: generated from git logs)

 - LPES-3657: i18n: added sk-SK
 - use lpWidget rename css file
 - LPES-3348: i18n
