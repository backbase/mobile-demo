/**
 *  ----------------------------------------------------------------
 *  Copyright © Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 */

'use strict';

var utils = global.utils;

module.exports = function(config) {

    config = config || {
        name: 'widget-alert-messages',
        title: 'Alert Messages'
    };

    var widget = this;

    widget.name = config.name;
    widget.title = config.title;
    /**
     * Prepare all elements
     * @return {promise} Return widget.elements
     */
    widget.get = function() {
        var d = utils.q.defer();
        utils.getWidget(widget.title).then(function(res) {
            widget.chrome = res.chrome;
            widget.body = res.body;
            d.resolve(widget);
        });
        return d.promise;
    };
    /**
     * The widget should be visible on the page
     * @return {Boolean}
     */
    widget.isVisible = function() {
        return widget.body.isDisplayed();
    };

    /**
     * clicking on the button to create a new automation
     */
    widget.createNewAutomation = function () {
        widget.body.element(by.css('.create-new-button')).click();
    };

    /**
     * Automation list wrapper
     */
    widget.automationListWrapper = function () {
        return widget.body.element(by.css('.lp-automations-list')).get();
    };

    /**
     * Clicking on the toggle to expend/collapse the list of rules/automations
     */
    widget.clickToggleAutomationList = function () {
        widget.automationListWrapper.element(by.css('[ng-click="toggleListView()"]')).click();
    };

    /**
     * Clicking on the link (in the list) to create a new automation if the list if empty
     */
    widget.createNewAutomationLink = function () {
        widget.automationListWrapper.element(by.css('')).click();
    };


};
