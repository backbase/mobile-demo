/**
 *  ----------------------------------------------------------------
 *  Copyright © Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : main.js
 *  Description: ${widget.description}
 *  ----------------------------------------------------------------
 */

define(function (require, exports, module) {

    'use strict';

    module.name = 'widget-accounts';

    var base = require('base');
    var core = require('core');
    var ui = require('ui');

    var accounts = require('module-accounts');

    var deps = [
        core.name,
        ui.name,
        accounts.name
    ];

    // @ngInject
    function run(lpCoreBus, lpWidget) {
        lpCoreBus.publish('cxp.item.loaded', {
            id: lpWidget.model.name
        });
    }

    module.exports = base.createModule(module.name, deps)
        .controller(require('./controllers'))
        .run(run);
});
