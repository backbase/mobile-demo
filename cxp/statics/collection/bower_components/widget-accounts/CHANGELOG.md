### v2.4.7 - `30/09/2015, 3:05pm`
* - lazy loaded Angular custom directives are broken in IE8  

### v2.4.6 - `25/08/2015, 10:41pm`
#### add tag into info.json for styleguide filtering  
* add tag for styleguide menu filtering  


### v2.4.5 - `21/08/2015, 3:48pm`
* LF-237:Fix model.xml to show acc two separate panel bydefault  


### v2.4.4 - `20/08/2015, 3:37pm`
* add theme as dev dependency  
* Add less file and build dist assets  
* Build dist assets  
* Put back cxp.item.loaded in run method.  
* Put back cxp.item.loaded in run method.  
* Put back cxp.item.loaded in run method.  


### v2.4.3 - `10/08/2015, 5:59pm`
#### Remove repository from bower.json  


### v2.4.2 - `07/08/2015, 2:46pm`
#### Remove dependency on module-transactions as it's not needed.  


### v2.4.1 - `29/07/2015, 6:28pm`
* Fix reference to chrome in model.xml  
* LF-156: Clean up model.xml for 5.6 compatability.  
* Add index dev page to develop the widget with data from api...  


### v2.4.0 - `29/07/2015, 5:39pm`
#### Update model.xml for CXP 5.6 compatibility  
* LF-156: Clean up model.xml for 5.6 compatability.  
* Add index dev page to develop the widget with data from api...  


### v 1.0.0
* Initial release

## [2.0.0] - 2015-05-12 (note: generated from git logs)

 - LPES-3657: i18n: added sk-SK
 - widget-instance replace
 - LPES-0000: nonexistent-account-groups

## [2.2.0] - 2015-07-24

 - Added `cxp.item.loaded` event

## [2.3.0] - 2015-07-24

 - Added `preferredBalanceView` preference
