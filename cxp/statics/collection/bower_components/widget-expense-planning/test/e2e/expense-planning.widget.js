/**
*  ----------------------------------------------------------------
*  Copyright © Backbase B.V.
*  ----------------------------------------------------------------
*  Author : Backbase R&D - Amsterdam - New York
*/

'use strict';

var utils = global.utils;

module.exports = function(config) {

	config = config || {
		name: 'widget-expense-planning',
		title: 'Expense Planning'
	};

	var widget = this;

	widget.name = config.name;
	widget.title = config.title;
	/**
	* Prepare all elements
	* @return {promise} Return widget.elements
	*/
	widget.get = function() {
		var d = utils.q.defer();
		utils.getWidget(widget.title).then(function(res) {
			widget.chrome = res.chrome;
			widget.body = res.body;
			d.resolve(widget);
		});

		//List of months in Year View
		widget.monthList = function(index) {
			var items = by.repeater('month in months');
			if (utils.isNumber(index)) {
				return widget.body.element(items.row(index));
			} else {
				return widget.body.all(items);
			}
		};

		widget.dateList = function(index) {
			var dates = by.repeater('day in week');
			if(utils.isNumber(index)) {
				return widget.body.element(dates.row(index));
			} else {
				return widget.body.all(dates);
			}
		};

		return d.promise;
	};
	/**
	* The widget should be visible on the page
	* @return {Boolean}
	*/
	widget.isVisible = function() {
		return widget.body.isDisplayed();
	};

	widget.btnToday = function() {
		return element(by.buttonText('Today')).isDisplayed();
	};

  widget.calendar = function() {
		return element(by.css('.cal-outer')).isDisplayed();
	};

  //arrow buttons to navigate to previous or next months in the calendar
  widget.prevMonth = function() {
		return element(by.css('[ng-click="navigate.prev()"]'));
	};

  widget.nextMonth = function() {
		return element(by.css('[ng-click="navigate.next()"]'));
	};

	widget.agendaBtn = function() {
		return element(by.css('[ng-click="changeView(\'agenda\')"]')).isDisplayed();
	};

	//the Week button to switch to current week view of the calendar
	widget.weekBtn = function() {
		return element(by.css('[ng-click="changeView(\'week\')"]')).isDisplayed();
	};

	widget.weekBtnClick = function() {
		widget.weekBtn().click();
	};

	widget.weekView = function()	{
		return element(by.css('.week-view')).isDisplayed();
	};


	//the Month button to switch to current month view of the calendar
	widget.monthBtn = function() {
		return element(by.css('[ng-click="changeView(\'month\')"]')).isDisplayed();
	};

	widget.monthBtnClick = function() {
		widget.monthBtn().click();
	};

	widget.monthView = function() {
		return element(by.css('.month-view')).isDisplayed();
	};

	//Returns 'Year' button to switch to current year view of the calendar
	widget.yearBtn = function() {
		return element(by.css('[ng-click="changeView(\'year\')"]')).isDisplayed();
	};

	//Clicking on a 'Year' button
	widget.yearBtnClick = function() {
		widget.yearBtn().click();
	};

	//Returns a overview of the current Year
	widget.yearView = function() {
		return element(by.css('.year-view')).isDisplayed();
	};

	//Returns the names of a month
	widget.monthName = function(index) {
		return widget.monthList(index).all(by.css('.cal-month-text')).getText();
	};

  //Returns the number of dates displayed in the month view
  widget.getNumberOfDates = function() {
		return widget.dateList().count();
	};

	widget.selectedMonthName = function() {
		return element(by.css('.cal-date.pull-left.ng-binding')).getText();
	};


};
