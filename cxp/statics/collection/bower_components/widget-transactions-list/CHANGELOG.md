### v2.0.6 - `25/08/2015, 10:42pm`
#### add tag into info.json for styleguide filtering  
* add tag for styleguide menu filtering  


### v2.0.5 - `20/08/2015, 3:37pm`
* Add cxp.item.loaded event Build dist assets  
* Add cxp.item.loaded event  


### v2.0.4 - `13/08/2015, 4:06pm`
* Fix launchpad overriding in index.dev.html  


### v2.0.3 - `10/08/2015, 2:17pm`
* NGUSEMB-369 Update transactions list on account select
* Fix standalone mode


### v2.0.2 - `10/08/2015, 6:00pm`
#### Remove repository from bower.json


### v2.0.1 - `10/08/2015, 5:00pm`
#### Remove CSS links to module-transactions-2.


### v2.0.0 - `07/08/2015, 5:19pm`
#### Deprecate module-transactions-2.


### v1.3.0 - `07/08/2015, 3:12pm`
* NGUSEMB-304 Add scrollOffset preference


### v1.2.3 - `06/08/2015, 11:08am`
#### NGUSEMB-327 Fix layout of transactions list on mobile
* NGUSEMB-327 Fix layout of transactions list on mobile


### v1.2.2 - `31/07/2015, 4:17pm`
* Fix lack of module transactions styles


### v1.2.1 - `29/07/2015, 6:29pm`
* Fix reference to chrome in model.xml
* LF-156: Clean up model.xml for 5.6 compatability.


### v1.2.0 - `29/07/2015, 5:39pm`
#### Update model.xml for CXP 5.6 compatibility
* LF-156: Clean up model.xml for 5.6 compatability.


## [1.1.2]

 - Fix show transaction icons

## [1.0.0]

 - Initial release
