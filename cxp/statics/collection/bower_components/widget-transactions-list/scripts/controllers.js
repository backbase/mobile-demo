/**
 * Controllers
 * @module controllers
 */
define(function (require, exports) {

    'use strict';

    // @ngInject
    exports.MainCtrl = function($scope, $element, $timeout, lpWidget, lpCoreUtils, lpCoreBus, lpUIResponsive, lpTransactionsCategory, lpTransactions, lpAccounts, PreferenceService) {
        var ctrl = this;

        $scope.lpAccounts = lpAccounts;

        var scopeApply = function() {
            $timeout(function() {
                $scope.$apply();
            });
        };

        var checkSelectedAccount = function(account) {
            lpCoreUtils.forEach($scope.lpAccounts.accounts, function(currentAccount){
                // checking for 'originType' here isolates reaction to accounts-dropdown widget only
                if(account.accountId === currentAccount.id && !account.originType){
                    currentAccount.allAccounts = account.allAccounts;
                    $scope.lpAccounts.selected = currentAccount;
                }
            });

            scopeApply();
        };

        var bindBusListeners = function() {
            lpCoreBus.subscribe('launchpad-retail.accountSelected', function(account) {
                checkSelectedAccount(account);
            });

            lpCoreBus.subscribe('transactions-search:search:filter', function(filters) {
                ctrl.data.transactions.setFilters(filters);
                ctrl.data.transactions.loadTransactions(ctrl.data.accounts.selected);
            });
            lpCoreBus.subscribe('transactions-search:search:clear', function() {
                ctrl.data.transactions.clearFilters();
                ctrl.data.transactions.loadTransactions(ctrl.data.accounts.selected);
            });
            lpCoreBus.subscribe('transactions-search:search:update', function() {
                ctrl.data.transactions.updateFilters();
                ctrl.data.transactions.loadTransactions(ctrl.data.accounts.selected);
            });
            lpCoreBus.subscribe('transactions-search:sort:change', function(value) {
                ctrl.data.transactions.sort = value.sort;

                if (ctrl.data.transactions.transactions && ctrl.data.transactions.transactions.length) {
                    ctrl.data.transactions.loadTransactions(ctrl.data.accounts.selected);
                }
            });
        };

        // Store data used by the widget
        ctrl.data = {
            transactions: lpTransactions.api(),
            transactionsCategory: lpTransactionsCategory.api(),
            accounts: lpAccounts
        };

        // Load configured preferences by the widget
        lpCoreUtils.assign($scope, {
            showCategories: true,
            previewAll: false,
            showDatesAllTransactions: lpCoreUtils.parseBoolean(lpWidget.getPreference('showDatesAllTransactions')),
            hideDetailsPreference: lpCoreUtils.parseBoolean(lpWidget.getPreference('hideTransactionDetails')),
            offsetTopCorrection: lpCoreUtils.parseInt(lpWidget.getPreference('scrollOffset')),
            showTransactionIcons: lpCoreUtils.parseBoolean(lpWidget.getPreference('showTransactionIcons')),
            showScrollbar: lpCoreUtils.parseBoolean(lpWidget.getPreference('showScrollbar'))
        });

        ctrl.data.transactionsCategory.getAll();

        ctrl.data.accounts.load()
        .then(function(accounts) {
            $scope.lpAccounts.accounts = accounts;

            bindBusListeners();

            if(!ctrl.data.accounts.selected && ctrl.data.accounts.accounts && ctrl.data.accounts.accounts.length > 0) {
                var selectedAccount = ctrl.data.accounts.findByAccountNumber(lpWidget.getPreference('defaultAccount')) || ctrl.data.accounts.accounts[0];
                ctrl.data.accounts.selected = selectedAccount;
            }
        });

        PreferenceService.read().success(function(response) {
            if (response && !lpCoreUtils.isUndefined(response.pfmEnabled)) {
                $scope.showCategories = lpCoreUtils.parseBoolean(response.pfmEnabled);
            }
        });

        lpUIResponsive.enable($element)
            .rule({
                'max-width': 200,
                then: function() {
                    $scope.categoryLayout = 'tile';
                    $scope.categorySmallLayout = false;
                    $scope.responsiveClass = 'lp-tile-size';
                    scopeApply();
                }
            })
            .rule({
                'min-width': 201,
                'max-width': 375,
                then: function() {
                    $scope.accountSelectSize = 'small';
                    $scope.categoryLayout = 'small';
                    $scope.categorySmallLayout = true;
                    $scope.responsiveClass = 'lp-small-size';
                    scopeApply();
                }
            })
            .rule({
                'min-width': 376,
                'max-width': 600,
                then: function() {
                    $scope.accountSelectSize = 'large';
                    $scope.categoryLayout = 'medium';
                    $scope.categorySmallLayout = false;
                    $scope.responsiveClass = 'lp-medium-size';
                    scopeApply();
                }
            })
            .rule({
                'min-width': 601,
                then: function() {
                    $scope.categoryLayout = 'large';
                    $scope.categorySmallLayout = false;
                    $scope.responsiveClass = 'lp-large-size';
                    scopeApply();
                }
            });
    };
});
