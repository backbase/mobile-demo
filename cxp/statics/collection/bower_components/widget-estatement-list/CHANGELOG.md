### v2.1.6 - `01/10/2015, 6:14pm`
* removing space  
* Version bump  
* fixing directive for ie8  

### v2.1.5 - `01/10/2015, 5:38pm`
* fixing directive for ie8  

### v2.1.4 - `25/08/2015, 10:41pm`
#### add tag into info.json for styleguide filtering  
* add tag for styleguide menu filtering  


### v2.1.3 - `12/08/2015, 4:59pm`
#### Cosmetic improvements: wrapped list items into .list-group in order to make proper styles applied.


### v2.1.2 - `10/08/2015, 5:59pm`
#### Remove repository from bower.json  


### v2.1.1 - `29/07/2015, 6:29pm`
* Fix reference to chrome in model.xml  
* LF-156: Clean up model.xml for 5.6 compatability.  
* LF-106-remove-b$-from-estatement-list  


### v2.1.0 - `29/07/2015, 5:39pm`
#### Update model.xml for CXP 5.6 compatibility  
* LF-156: Clean up model.xml for 5.6 compatability.  
* LF-106-remove-b$-from-estatement-list  

### v 1.0.0
* Initial release
## [2.0.0] - 2015-05-12 (note: generated from git logs)

 - LPES-3657: i18n: added sk-SK
 - use run to set config
