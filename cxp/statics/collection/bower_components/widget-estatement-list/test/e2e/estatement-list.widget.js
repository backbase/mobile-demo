/**
 *  ----------------------------------------------------------------
 *  Copyright © Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 */

'use strict';

var utils = global.utils;

module.exports = function(config) {

    config = config || {
        name: 'widget-estatements',
        title: 'Estatements'
    };

    var widget = this;

    widget.name = config.name;
    widget.title = config.title;
    /**
     * Prepare all elements
     * @return {promise} Return widget.elements
     */
    widget.get = function() {
        var d = utils.q.defer();
        utils.getWidget(widget.title).then(function(res) {
            widget.chrome = res.chrome;
            widget.body = res.body;
            d.resolve(widget);
        });
        return d.promise;
    };
    /**
     * The widget should be visible on the page
     * @return {Boolean}
     */
    widget.isVisible = function() {
        return widget.body.isDisplayed();
    };

    //enrol for Estatements button
    widget.clickEnrolBtn = function() {
        widget.body.element(by.css('[ng-show="mainCtrl.isNotEnrolled()"]')).click();
    }

    //list of Estatements
    widget.estatementList = function(index) {
        var items = by.repeater('item in mainCtrl.estatementList');
        if (utils.isNumber(index)) {
            return widget.body.element(items.row(index));
        } else {
            return widget.body.all(items);
        }
    };
};
