### v2.1.7 - `30/09/2015, 6:06pm`
* Change showFooter by hideFooter to not break the previous behaviour on portal  


### v2.1.6 - `30/09/2015, 3:31pm`
* - lazy loaded Angular custom directives are broken in IE8  

### v2.1.5 - `29/09/2015, 10:38am`
* Be able to remove the footer buttons for mobile apps to use native buttons instead Fix the index.dev.html file.  

### v2.1.4 - `28/08/2015, 4:04pm`
* LF-265: Fixed bus flush method to unsubscribe


### v2.1.3 - `25/08/2015, 10:42pm`
#### add tag into info.json for styleguide filtering
* add tag for styleguide menu filtering


### v2.1.2 - `10/08/2015, 5:59pm`
#### Remove repository from bower.json


### v2.1.1 - `29/07/2015, 6:29pm`
* Fix reference to chrome in model.xml
* LF-156: Clean up model.xml for 5.6 compatability.


### v2.1.0 - `29/07/2015, 5:39pm`
#### Update model.xml for CXP 5.6 compatibility
* LF-156: Clean up model.xml for 5.6 compatability.


### v 1.0.0
* Initial release
## [2.0.0] - 2015-05-12 (note: generated from git logs)

 - LPES-3657: i18n: added sk-SK
 - LPES-3602: no 200 code check
 - remove unused methods
 - add config in run function
 - LPES-3555: Review Transfers: create page objects file for protractor tests
 - LPES-3539: del edit
 - LPES-3534: info icon should not show up if no orders
 - LPES-3533: no date visible fix
