/* globals define */

define( function (require, exports, module) {
    'use strict';

    // var _ = require('base').utils;

    /**
     * Ebilling list item directive
     * @type {Array} angular js directive
     */
     //@ngInject
    exports.payeeListItem = function ($timeout, $compile, lpCoreUtils, lpWidget, PayeeModel, PayeeService, CalendarService) {

        var linkFn = function (scope, $element, $attrs, modelCtrl) {
            scope.payee.amount = scope.payee.eBill ? scope.payee.eBill.amountDue : 0;
            scope.payeeModel = PayeeModel;

            if (scope.payee.accountMask) {
                scope.payee.accountMask = '******** ' + scope.payee.accountMask;
            }

            scope.eligibleForEbills = function() {
                return scope.payee.eBillsStatus === 'ELIGIBLE';
            };

            scope.eBillErrorsExist = function () {
                var errors = [
                    'ERROR_CREDENTIAL_UPDATE_REQUIRED',
                    'ERROR_NO_LONGER_SUPPORTED',
                    'ERROR_USER_ACTION_REQUIRED'
                ];
                return errors.indexOf(scope.payee.eBillsStatus) !== -1;
            };

            scope.openPayementDetails = function() {
                scope.selectedPayee = scope.isPayementDetailsOpen() ? null : scope.payee;
            };

            scope.isPayementDetailsOpen = function() {
                return scope.selectedPayee === scope.payee;
            };

            scope.confirmSettings = function() {
                console.log('TODO: Implement navigation to E-Bill settings confirmation');
            };

            scope.setupEBills = function() {
                console.log('TODO: Implement navigation to E-Bill setup');
            };
        };
        var templatesDir = lpCoreUtils.getWidgetBaseUrl(lpWidget) + '/templates';
        return {
            restrict: 'EA',
            require: '?ngModel',
            link: linkFn,
            templateUrl: templatesDir + '/list-item.ng.html',
            scope: {
                payee: '=ngModel',
                selectedPayee: '=',
                afterPaymentCallback: '&'
            }
        };
    };

    //@ngInject
    exports.payeeContent = function ($timeout, $compile, lpCoreUtils, lpWidget, PayeeModel, PayeeService, CalendarService) {
        var templatesDir = lpCoreUtils.getWidgetBaseUrl(lpWidget) + '/templates';

        var linkFn = function (scope, $element, $attrs, modelCtrl) {
            scope.payeeModel = PayeeModel;

            var updateDeliveryDate = function(startDate) {
                if (startDate) {
                    var daysToDeliver = scope.payment.urgentTransfer ? 1 : scope.payee.businessDaysToDeliver;
                    CalendarService.getBusinessDay(startDate, daysToDeliver)
                        .then(function(result) {
                            scope.payment.estDeliveryDate = CalendarService.formatDate(result);
                        });
                }
            };

            scope.$watch('payment.urgentTransfer', function() {
                updateDeliveryDate(scope.payment.scheduleDate);
            });

            scope.$watch('payment.scheduleDate', function(startDate) {
                updateDeliveryDate(startDate);
            });

            var blankPayment = {
                amount: 0,
                scheduleDate: new Date(),
                account: PayeeModel.defaultAccount,
                currencySym: '$',
                isScheduledTransfer: false,
                scheduledTransfer: {
                    startDate: new Date(),
                    intervals: [],
                    timesToRepeat: 1
                },
                memo: ''
            };

            var resetPayment = function() {
                lpCoreUtils.forEach(blankPayment, function(value, key) {
                    scope.payment[key] = value;
                });
            };

            var createAPayment = function() {
                if (scope.payee.payment) {
                    scope.payment = scope.payee.payment;
                } else {
                    scope.payment = lpCoreUtils.cloneDeep(blankPayment);
                    scope.payee.payment = scope.payment;
                }
            };

            var isValidPayment = function(payment) {
                scope.payee.warnings = [];
                if (!payment.account) { scope.payee.warnings.push({ code: 'ERROR_SELECT_ACCOUNT' }); }
                if (!payment.scheduleDate) { scope.payee.warnings.push({ code: 'ERROR_ENTER_PROCESSING_DATE' }); }
                if (!payment.amount) { scope.payee.warnings.push({ code: 'ERROR_ENTER_AMOUNT' }); }

                if (payment.isScheduledTransfer) {
                    if (!payment.scheduledTransfer.frequency) { scope.payee.warnings.push({ code: 'ERROR_SELECT_FREQUENCY' }); return false; }
                    if (payment.scheduledTransfer.endOn === 'after') {
                        if (!payment.scheduledTransfer.timesToRepeat) { scope.payee.warnings.push({ code: 'ERROR_ENTER_TIMES_TO_REPEAT' }); }
                        if (payment.scheduledTransfer.frequency === 'OneTime' && payment.scheduledTransfer.timesToRepeat !== 1) { scope.payee.warnings.push({ code: 'ERROR_TIMES_TO_REPEAT_SHOULD_BE_1' }); }
                        if (payment.scheduledTransfer.frequency !== 'OneTime' && payment.scheduledTransfer.timesToRepeat === 1) { scope.payee.warnings.push({ code: 'ERROR_TIMES_TO_REPEAT_SHOULD_BE_MORE_THAN_1' }); }
                    }
                }

                if (scope.payee.warnings.length > 0) { return false; }
                return true;
            };

            scope.pay = function() {
                if (isValidPayment(scope.payment)) {
                    scope.payee.processing = true;
                    // scope.payee.opened = false;
                    scope.selectedPayee = null;

                    PayeeService.submitPayment(scope.payee, scope.payment)
                        .then(function(response) {

                            scope.afterPaymentCallback();

                            scope.payee.response = {};
                            scope.payee.response.action = 'success';
                            scope.payee.response.message = 'ALERT_PAYMENT_SUBMITTED';
                            resetPayment();

                            $timeout(function() {
                                delete scope.payee.response;
                            }, 3000);
                        }, function(response){
                            // scope.payee.opened = true;
                            scope.selectedPayee = scope.payee;
                            scope.payee.errors = response.data.errors;
                        })['finally'](function() {
                            scope.payee.processing = false;
                        });
                }
            };

            scope.showUrgentTransfer = function() {
                return scope.payee.canExpeditePayments && scope.payee.paymentMethod === PayeeModel.paymentMethods.CHECK;
            };

            createAPayment();
        };

        return {
            restrict: 'EA',
            link: linkFn,
            templateUrl: templatesDir + '/list-item-content.ng.html'
        };
    };

});
