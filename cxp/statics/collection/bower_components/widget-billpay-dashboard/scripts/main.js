/**
* TODO add description
*/
define( function (require, exports, module) {

    'use strict';

    module.name = 'widget-billpay-dashboard';

    /**
     * Dependencies
     */
    var base = require('base');
    var core = require('core');
    var ui = require('ui');
    var ebilling = require('module-ebilling');

    var deps = [
        core.name,
        ui.name,
        ebilling.name
    ];

    // @ngInject
    function run(lpCoreBus, lpWidget) {
        if(lpWidget.model && lpWidget.model.name){
            lpCoreBus.publish('cxp.item.loaded', {id: lpWidget.model.name});
        }
    }


    module.exports = base.createModule(module.name, deps)
        .controller(require('./controllers'))
        // .config(require('./config'))
        .service(require('./models'))
        .directive(require('./directives'))
        .run(run);
});
