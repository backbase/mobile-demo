/* globals define */

/**
 * Controllers
 * @module controllers
 */
define(function(require, exports) {

    'use strict';

    // @ngInject
    exports.PayeeListCtrl = function($q, lpCoreHttpInterceptor, PayeeModel, PayeeService, CalendarService) {

        var ctrl = this;

        ctrl.selectedPayee = null;

        ctrl.fetchPayees = function() {
            return PayeeService.fetchPayees().then(function(response) {
                ctrl.payees = response.data.payees;
                ctrl.isLoading = false;
            }, function() {
                throw 'Failed to load payees.';
            });
        };

        ctrl.fetchAccounts = function() {
            return PayeeService.fetchAccounts().then(function(results) {
                PayeeModel.accounts = results;
            }, function() {
                throw 'Failed to load accounts.';
            });
        };

        ctrl.fetchFrequencies = function() {
            return PayeeService.fetchFrequencies().then(function(results) {
                PayeeModel.frequencies = results;
            }, function() {
                throw 'Failed to load periods.';
            });
        };

        ctrl.openManagePayees = function () {
            console.log('TODO: Implement navigation to Manage Payees page');
        };

        /**
         * Initialize method
         */
        ctrl.payees = [];

        ctrl.init = function() {
            ctrl.error = false;
            ctrl.isLoading = true;
            $q.all([ctrl.fetchPayees(), ctrl.fetchAccounts(), ctrl.fetchFrequencies()])
                ['catch'](function(error) {
                    ctrl.error = error;
                })
                ['finally'](function() {
                    ctrl.isLoading = false;
                });
        };

        // Initialize widget
        ctrl.init();

        // Prevent notitification widget to react to server errors
        lpCoreHttpInterceptor.configureNotifications({
            ignore: [/frequencies|accounts|payees|payments/]
        });
    };
});
