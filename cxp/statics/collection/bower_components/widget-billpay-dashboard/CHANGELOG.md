### v2.0.3 - `30/09/2015, 3:05pm`
* - lazy loaded Angular custom directives are broken in IE8  

### v2.0.2 - `24/09/2015, 2:46pm`
* NGUSEOLB-490 Fix payee list item layout  


### v2.0.1 - `24/09/2015, 10:18am`
* NGUSEOLB-490 Update minified script  


### v2.0.0 - `24/09/2015, 9:32am`
* NGUSEOLB-490 Vertical align payee name  
* NGUSEOLB-490 Show message when no payees exist  
* NGUSEOLB-490 Remove testing code  
* NGUSEOLB-490 Expand payment details NOT on top of other list items  
* NGUSEOLB-490 Remove unused controller  
* NGUSEOLB-490 Show EBill errors  
* NGUSEOLB-490 Widget payee list redesign  


### v1.2.1 - `08/09/2015, 3:25pm`
* Implemented proper error handling for failed services.
* Change widget name for consistency.
* Cleanup, removed dummy controller and $scope.

### v1.2.0 - `07/09/2015, 3:26pm`
* Implemented proper error handling.


### v1.1.4 - `25/08/2015, 10:41pm`
#### add tag into info.json for styleguide filtering  
* add tag for styleguide menu filtering  


### v1.1.3 - `20/08/2015, 3:37pm`
* Add cxp.item.loaded event Build dist assets  
* Add cxp.item.loaded event  


### v1.1.2 - `10/08/2015, 5:59pm`
#### Remove repository from bower.json  
* NoJira - Fix issue that sometimes payees with ebills are not displayed  


### v1.1.1 - `29/07/2015, 6:28pm`
* Fix reference to chrome in model.xml  
* LF-156: Clean up model.xml for 5.6 compatability.  
* Fix problem with overflow if the text was too long to fit in the width of the viewport.  
* Change the width of columns if ebill data doesn't comes from server to show more characters of name.  


### v1.1.0 - `29/07/2015, 5:39pm`
#### Update model.xml for CXP 5.6 compatibility  
* LF-156: Clean up model.xml for 5.6 compatability.  
* Fix problem with overflow if the text was too long to fit in the width of the viewport.  
* Change the width of columns if ebill data doesn't comes from server to show more characters of name.  


### v 1.0.1
* Ready for standalone development
* Fix reset payment box after succesful payment

### v 1.0.0
* Initial release
