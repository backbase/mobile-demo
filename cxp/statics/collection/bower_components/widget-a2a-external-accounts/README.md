# A2A External Accounts widget


## Information
| name                      | version | bundle |
| --------------------------|---------| -------|
| a2a-external-accounts 	| 1.2.0   | ebill  |


## Dependencies
* [Wealth](https://stash.backbase.com/projects/LPW/repos/widget-a2a-external-accounts/browse)


## Events
None

## Preferences
| name                     | label       | description  | default value                                                                         |
| -------------------------|-------------| -------------|---------------------------------------------------------------------------------------|
| -		    | 	-  | -  | 	- |

## Directives
| name                      | attrs.type | description   |
| --------------------------|------------|---------------|
|                           |            |               |
