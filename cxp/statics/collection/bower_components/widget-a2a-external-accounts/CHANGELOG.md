### v1.2.0 - `24/08/2015, 2:17pm`
* NGUSEOLB-351: Add text for rejected accounts
* NGUSEOLB-351: Add info messages when creating and activationg accounts
* NGUSEOLB-351: Fix standalone widget development


### v1.1.4 - `25/08/2015, 10:41pm`
#### add tag into info.json for styleguide filtering
* add tag for styleguide menu filtering


### v1.1.3 - `19/08/2015, 4:21pm`


### v1.1.2 - `10/08/2015, 5:59pm`
#### Remove repository from bower.json


### v1.1.1 - `29/07/2015, 6:28pm`
* Fix reference to chrome in model.xml
* LF-156: Clean up model.xml for 5.6 compatability.


### v1.1.0 - `29/07/2015, 5:39pm`
#### Update model.xml for CXP 5.6 compatibility
* LF-156: Clean up model.xml for 5.6 compatability.


### v 1.0.0
* Initial release
