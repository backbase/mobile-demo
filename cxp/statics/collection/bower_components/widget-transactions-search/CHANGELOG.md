### v2.0.3 - `25/08/2015, 10:42pm`
#### add tag into info.json for styleguide filtering  
* add tag for styleguide menu filtering  


### v2.0.2 - `20/08/2015, 3:37pm`
* Add cxp.item.loaded event Rebuild dist assets  
* Add cxp.item.loaded event  


### v2.0.1 - `10/08/2015, 6:00pm`
#### Remove repository from bower.json  


### v2.0.0 - `10/08/2015, 3:39pm`
* LF-162: Deprecate module-transactions-2.  

### v1.2.0 - `04/08/2015, 4:57pm`
* Add `cxp.item.loaded` event

### v1.1.1 - `29/07/2015, 6:29pm`
* Fix reference to chrome in model.xml
* LF-156: Clean up model.xml for 5.6 compatability.

### v1.1.0 - `29/07/2015, 5:39pm`
#### Update model.xml for CXP 5.6 compatibility
* LF-156: Clean up model.xml for 5.6 compatability.

## [1.0.0]
 - Initial release
