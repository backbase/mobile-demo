### v1.2.0 - `29/09/2015, 11:31am`
* NGUSEOLB-305: enable password validations  
* NGUSEOLB-305: Add module users dependency  
* NGUSEOLB-89: Use input fields directive from enrollment module  
* NGUSEOLB-89: Fix standalone and caret version in bower  

### v1.1.3 - `25/08/2015, 10:42pm`
#### add tag into info.json for styleguide filtering  
* add tag for styleguide menu filtering  


### v1.1.2 - `10/08/2015, 5:59pm`
#### Remove repository from bower.json  


### v1.1.1 - `29/07/2015, 6:29pm`
* Fix reference to chrome in model.xml  
* LF-156: Clean up model.xml for 5.6 compatability.  


### v1.1.0 - `29/07/2015, 5:39pm`
#### Update model.xml for CXP 5.6 compatibility  
* LF-156: Clean up model.xml for 5.6 compatability.  


### v 1.0.0
* Initial release
