/**
 * Models
 * @module models
 */
define( function (require, exports) {

    'use strict';

    /**
     * @constructor
     * @ngInject
     */
    var WidgetModel = function WidgetModel(lpWidget, lpUserSettings) {
        // Service path
        var userSettingsPasswordUrl = lpWidget.getResolvedPreference('userSettingsPasswordUrl');

        // Data Store
        // password component
        this.oldPassword = null;
        this.newPassword = null;

        // Initialize
        this.widget = lpWidget;

        this.setNewPassword = function(oldPassword, newPassword) {
            this.oldPassword = oldPassword;
            this.newPassword = newPassword;
        };

        this.saveNewPassword = function(){
            return lpUserSettings.changePassword(userSettingsPasswordUrl, this.oldPassword, this.newPassword).then(function(response) {
                return response;
            });
        }.bind(this);
    };
    /**
     * Export Models
     */
    exports.WidgetModel = WidgetModel;

});




