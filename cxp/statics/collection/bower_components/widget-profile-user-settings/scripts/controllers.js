/**
 * Controllers
 * @module controllers
 */
define(function (require, exports) {

    'use strict';

    /**
     * @name MainCtrl
     *
     * @constructor
     *
     * @description
     * Main controller for the widget
     *
     * @ngInject
     */
    exports.MainCtrl = function MainCtrl(WidgetModel, lpWidget, lpCoreUtils, lpCoreError, lpCoreBus, $q) {
        var ctrl = this;

        ctrl.model = WidgetModel;
        ctrl.utils = lpCoreUtils;
        ctrl.error = lpCoreError;
        ctrl.widget = lpWidget;

        var passwordValid = false;
        ctrl.valid = false;

        var checkValidity = function() {
            ctrl.valid = passwordValid;
        };

        ctrl.onPasswordValid = function(valid, oldPassword, newPassword) {
            if (valid) {
                ctrl.model.setNewPassword(oldPassword, newPassword);
            }

            passwordValid = valid;
            checkValidity();
        };

        // Show a message when everything has been succesfully saved
        ctrl.showSuccesfulSavedMessage = function() {
            ctrl.saved = true;
        };

        // Save changed settings
        ctrl.saveChanges = function() {
            ctrl.saving = true;
            var promises = [];
            promises.push(ctrl.model.saveNewPassword());

            $q.all(promises).then(function() {
                ctrl.saving = false;
                ctrl.showSuccesfulSavedMessage();
            });
        };
    };
});
