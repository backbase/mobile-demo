/**
 * Directives
 * @module directives
 */
define(function (require, exports) {
    'use strict';

    /**
     * @name lpValidationOnBlur
     *
     * @description
     * Adds the flag `$touched` to the model of a field to indicate that it has been blurred.
     * It also adds/removes the class `ng-touched` to the field element.
     */
    exports.lpValidationOnBlur = function() {
        return {
            restrict: 'AE',
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                var touched = function() {
                    if (ctrl.$modelValue) {
                        ctrl.$touched = true;
                        elm.off('blur', touched);
                    }
                    scope.$apply();
                };

                scope.$watch(function() {
                    return ctrl.$touched;
                }, function(newValue) {
                    elm[newValue ? 'addClass' : 'removeClass']('ng-touched');
                }, true);

                ctrl.$touched = false;
                elm.on('blur', touched);
            }
        };
    };

    /**
     * @name lpValidationPasswordLength
     *
     * @description
     * Validates the value of the field has a length between 8 and 22 characters.
     */
    exports.lpValidationPasswordLength = function() {
        return {
            restrict: 'AE',
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                ctrl.$parsers.push(function(viewValue) {
                    if (viewValue && viewValue.length > 7 && viewValue.length < 21) {
                        ctrl.$setValidity('lpValidationPasswordLength', true);
                        return viewValue;
                    }

                    ctrl.$setValidity('lpValidationPasswordLength', false);
                    return viewValue;
                });
            }
        };
    };

    /**
     * @name lpValidationOneLetterOneNumber
     *
     * @description
     * Validates the value of the field contains at least one letter and one number.
     */
    exports.lpValidationOneLetterOneNumber = function() {
        var ONE_LETTER_REGEXP = /[a-zA-Z]/;
        var ONE_NUMBER_REGEXP = /\d/;

        return {
            restrict: 'AE',
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                ctrl.$parsers.push(function(viewValue) {
                    if (ONE_LETTER_REGEXP.test(viewValue) && ONE_NUMBER_REGEXP.test(viewValue)) {
                        ctrl.$setValidity('lpValidationOneLetterOneNumber', true);
                        return viewValue;
                    }

                    ctrl.$setValidity('lpValidationOneLetterOneNumber', false);
                    return viewValue;
                });
            }
        };
    };

    /**
     * @name lpValidationSpecialCharacter
     *
     * @description
     * Validates the value of the field contains at least one of the following
     * special characters:
     *     @ # * ( ) + { } / ? ~ ; , .  - _
     */
    exports.lpValidationSpecialCharacter = function() {
        var SPECIAL_CHAR_REGEXP = /[@#*()+{}\/?~;,\.\-_]/;

        return {
            restrict: 'AE',
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                ctrl.$parsers.push(function(viewValue) {
                    if (SPECIAL_CHAR_REGEXP.test(viewValue)) {
                        ctrl.$setValidity('lpValidationSpecialCharacter', true);
                        return viewValue;
                    }

                    ctrl.$setValidity('lpValidationSpecialCharacter', false);
                    return viewValue;
                });
            }
        };
    };

    /**
     * @name lpValidationConfirmPassword
     *
     * @description
     * Validates the value of the field is exactly the same as the value of the
     * field `newPassword`
     */
    exports.lpValidationConfirmPassword = function() {
        return {
            restrict: 'AE',
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                ctrl.$parsers.push(function(viewValue) {
                    if (scope.mainCtrl.newPassword === viewValue) {
                        ctrl.$setValidity('lpValidationConfirmPassword', true);
                        return viewValue;
                    }

                    ctrl.$setValidity('lpValidationConfirmPassword', false);
                    return viewValue;
                });
            }
        };
    };
});
