/**
 *  ----------------------------------------------------------------
 *  Copyright © Backbase B.V.
 *  ----------------------------------------------------------------
 *  Filename : main.js
 *  Description: widget-profile-user-settings
 *  ----------------------------------------------------------------
 */

define( function (require, exports, module) {

    'use strict';

    module.name = 'widget-profile-user-settings';

    var base = require('base');
    var core = require('core');
    var ui = require('ui');
    var users = require('module-users');
    // TODO: Use ui component lp-labeled-input-field when migrated
    var enrollment = require('module-enrollment');

    var deps = [
        core.name,
        ui.name,
        users.name,
        enrollment.name
    ];

    /**
     * @ngInject
     */
    function run() {
        // Module is Bootstrapped
    }

    module.exports = base.createModule(module.name, deps)
        .constant('WIDGET_NAME', module.name )
        .directive( require('./directives/password') )
        .controller( require('./controllers') )
        .service( require('./models') )
        .run( run );
});
