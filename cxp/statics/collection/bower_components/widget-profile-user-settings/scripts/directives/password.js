/**
 * Password directive
 * @module directives
 */
define(function (require, exports) {
    'use strict';

    // @ngInject
    exports.lpProfileUserSettingsPassword = function($q, lpCoreBus, lpCoreUtils) {
        return {
            restrict: 'AE',
            template: '<div lp-template="\'templates/password.html\'"></div>',
            scope: {
                onValid: '&'
            },
            link: function(scope, element, attrs) {
                var oldPassword = null;
                var newPassword = null;
                var confirmPassword = null;
                var isValidExternal = false;

                // Change password values holder
                scope.fields = [{
                    name: 'currentPassword',
                    required: true,
                    type: 'password',
                    pattern: '/^[a-zA-Z0-9.\'\-_\\s]{8,32}$/',
                    errorMsg: 'Password must be between 8 and 32 characters',
                    placeholder: 'Current password',
                    value: null
                }, {
                    name: 'newPassword',
                    required: true,
                    externalVerification: 'verifyPassword',
                    type: 'password',
                    pattern: '/^[a-zA-Z0-9.\'\-_\\s]{8,32}$/',
                    errorMsg: 'Password must be between 8 and 32 characters',
                    placeholder: 'New password',
                    value: null
                }, {
                    name: 'confirmPassword',
                    required: true,
                    type: 'password',
                    mirror: 'newPassword',
                    pattern: '',
                    errorMsg: 'Password does not match',
                    placeholder: 'Confirm password',
                    value: null
                }];

                scope.validations = {
                    verifyPassword: function(model) {
                        var value = model.newPassword;
                        var valid = true;
                        var message = '';

                        if (!value.match(/[A-Z]/g)) {
                            valid = false;
                            message = 'Password must contain at least 1 capital letter';
                        } else if (!value.match(/[0-9]/g)) {
                            valid = false;
                            message = 'Password must contain at least 1 number';
                        }

                        return $q.when({ valid: valid, message: message });
                    }
                };

                var checkValidity = function() {
                    var valid = oldPassword && newPassword && confirmPassword && isValidExternal;
                    scope.onValid( {
                        valid: !!valid,
                        oldPassword: oldPassword,
                        newPassword: newPassword
                    });
                };

                lpCoreBus.subscribe('lp-enrollment:validation:external', function(validator) {
                    if (validator.name === 'newPassword') {
                        isValidExternal = validator.result;
                        checkValidity();
                    }
                });

                scope.$watch('fields[0].value', function(newValue) {
                    if (newValue) {
                        oldPassword = newValue;
                        checkValidity();
                    }
                });

                scope.$watch('fields[1].value', function(newValue) {
                    if (newValue) {
                        newPassword = newValue;
                        checkValidity();
                    }
                });

                scope.$watch('fields[2].value', function(newValue) {
                    if (newValue) {
                        confirmPassword = newValue;
                        checkValidity();
                    }
                });
            }
        };
    };
});
