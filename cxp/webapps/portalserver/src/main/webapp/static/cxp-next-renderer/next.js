(function () {
    "use strict";

    /*
     The next UI renderer.
     Compatible with the webView env in the sdk.
     Supports : Pages rendering, navigation, navigation triggered by widgets.
     */

    //these configs are messy, it works but needs refactoring
    var options = {
        contextRoot: '/portalserver',
        portalName: window.localStorage.portalName,
        remoteContextRoot: 'http://localhost:7777/portalserver/',
        localModel: ""
    }


    var appModel;
    var pagesHash = {};
    var defaultPage;
    var defaultTitle = document.title;
    var appPlaceholder = document.getElementById('app');
    var AUTHEVENT = "navigation:user-auth";

    init();
    addEventListeners();


    //Get the model and init
    function init() {
        if (!window.localStorage.portalName) {
            alert('please set your portal name on the lille cog up there on the right');
            return;
        }

        var model = options.localModel || options.remoteContextRoot + 'portals/' + window.localStorage.portalName + '/mobile/model';
        $.getJSON(model, function (data) {
            appModel = data;
            buildNav(appModel);
            render(pagesHash[pageHash()], true);
            preload();

        }).fail(function (error) {
            console.error("The model failed to load. Probably a portal name mismatch. The current p[ortal name is:" + options.portalName, error);
        });
        addHashListener();

    }

    //builds nav and panel holders
    function buildNav(appModel) {
        var navItems = [];
        var panels = [];

        //Push all the pages to an hashMap, easy to get them later
        //Also render Placeholder for all the widgets
        appModel.pages.forEach(function (page, i) {
            var name = page.name;
            var id = page.id;
            pagesHash[id] = page;
            panels.push("<div id=" + id + " class='nextApp'></div>");


            var preloadOnDemandVal = getPreference(page, 'preloadOnDemand');

            if (preloadOnDemandVal) {
                preloadOnDemand(preloadOnDemandVal, page)
            }

        });

        //iterate on the links model to build our nav
        appModel.sitemap.forEach(function (navNode, i) {
            if (navNode.name == "Main Navigation" && navNode.children.length > 0) {
                //set default page
                defaultPage = navNode.children[0].itemRef;
                //iterate on every children, for now it doesn't go deeper in the nav, only this level
                buildAndRegister(navNode, true);
            }
        });

        function buildAndRegister(navNode, processChildren) {
            navNode.children.forEach(function (link, i) {

                registerNav(link);

                if (processChildren) {
                    navItems.push(buildNavItem(link));
                }

                if (link.children && link.children.length > 0) {
                    buildAndRegister(link, false);
                }

            });
        }

        //register nav
        function registerNav(el) {
            //We subscribe for an event with the page navigation preference
            gadgets.pubsub.subscribe(getPreference(pagesHash[el.itemRef], 'navigation'), function () {
                //we only change the hash, all the navigation is handled by hash change
                window.location.hash = "/" + el.itemRef;
            });

        }



        //return nav html
        function buildNavItem(el) {
            return "<li><a href=" + '#/' + el.itemRef + " class='nav-link'>" + el.name + "</a></li>";
        }

        //Concatenate nav html
        var navItemsHtml = "<ul class='nextNav'>" + navItems.join("") + "</ul>";

        //Concatenate widget Placeholders html
        var panelsHtml = "<div class='nextApps'>" + panels.join("") + "</div>";
        //Insert nav and panels in the DOM
        appPlaceholder.innerHTML = panelsHtml + navItemsHtml;

    }

    // We reload the model on login/logout - like the sdk
    gadgets.pubsub.subscribe(AUTHEVENT, function () {
        reloadModel();
    });


    function reloadModel() {
        //reset all the things
        window.removeEventListener('hashchange', onHashChange)
        window.widgets = {};
        history.replaceState({}, document.title, "index.html");

        // Start again
        init();
    }


    function render(page, vis) {
        var $currentPanel = $('#' + page.id);
        if (vis) {
            $currentPanel.addClass('vis').siblings().removeClass('vis');
            window.scrollTo(0, 0);
            document.title = defaultTitle + " - " + page.name.toUpperCase();
        }

        //We only render if the panel is empty
        if ($currentPanel.children().length < 1) {
            //render using the ios renderer, the arguments are a bit obscure
            renderWidget(page.id, options.contextRoot, options.remoteContextRoot, page, [], "none", {}, options.portalName);
        }
        page.loaded = true;

    }

    function preload() {
        appModel.pages.forEach(function (page, i) {
            var preload = getPreference(page, "preload");
            if (!page.loaded && preload && JSON.parse(preload)) {
                render(page, false);
            }
        });
    }


    //Create listeners for preloadOnDemand
    function preloadOnDemand(val, page) {
        gadgets.pubsub.subscribe(val, function () {
            console.log('Someone asked this page to be preloaded on deman', val, page)
            if (!page.loaded) {
                render(page, false);
            }
        });
    }


    function onHashChange() {
        render(pagesHash[pageHash()], true);
    }

    function addHashListener() {
        window.addEventListener('hashchange', onHashChange);
    }

    function pageHash() {
        var pageName = window.location.hash ? window.location.hash.substr(2) : defaultPage;
        var checkedPage = pagesHash[pageName] ? pageName : defaultPage;
        return checkedPage;
    }

    function addEventListeners() {
        //for settings
        var $settings = $('.next-settings');
        var $settingIn = $settings.find('.next-settings-input');
        $settingIn.val(window.localStorage.portalName || "please add a portal name here");

        $settings.on('click', '.next-settings-toogler', function (ev) {
            $settings.toggleClass('vis');
        });

        $settingIn.on('blur', function (ev) {
            window.localStorage.portalName = ev.target.value;
            init();
        });
    }

    //utils
    function getPreference(item, prefName) {
        var value;
        item.preferences.forEach(function (pref) {
            if (pref.name == prefName) {
                value = pref.value;
            }
        });
        return value;
    }
}());
