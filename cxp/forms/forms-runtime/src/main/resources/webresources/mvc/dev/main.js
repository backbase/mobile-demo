"use strict";

var app = angular.module("source", ["jsonFormatter"]);

app.controller('MainCtrl', function ($scope, $window) {
  $scope.model = $window.model;

  //this returns the root element in the model so tbe form directive can start rendering the form tree
  $scope.getRootElement = function () {
    var page = {};
    if ($scope.model && $scope.model.elements) {
      page = $scope.model.elements.filter(function (element) {
          return element.type === 'page';
        })[0] || page;
    }
    return page;
  };

  //returns an element from the model, given a key
  $scope.lookupElement = function (elementKey) {
    var element = null;
    if ($scope.model.elements) {
      element = $scope.model.elements.filter(function(element) {
          return element.key === elementKey;
        })[0] || element;
    }
    return element;
  };

  $scope.buildTree = function(el) {
    if (!el || !el.children) {
      return el;
    }

    var l = angular.copy(el);

    l.children = l.children.map(function (el) {
      return $scope.buildTree($scope.lookupElement(el));
    });

    return l;
  };

  $scope.modelTree = $scope.buildTree($scope.getRootElement());
});