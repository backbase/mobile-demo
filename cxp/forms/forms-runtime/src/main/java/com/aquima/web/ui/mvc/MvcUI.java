package com.aquima.web.ui.mvc;

import com.aquima.interactions.composer.ICompositeElement;
import com.aquima.interactions.composer.IPage;
import com.aquima.interactions.foundation.DataType;
import com.aquima.interactions.foundation.exception.InvalidArgumentException;
import com.aquima.interactions.foundation.io.IResourceManager;
import com.aquima.interactions.foundation.text.ILanguage;
import com.aquima.interactions.framework.resource.DefaultResourceManager;
import com.aquima.interactions.portal.IPortalSession;
import com.aquima.interactions.project.IValueFormatDefinition;
import com.aquima.web.IAquimaSession;
import com.aquima.web.api.model.page.*;
import com.aquima.web.config.PluginManager;
import com.aquima.web.ui.IAquimaUI;
import com.aquima.web.ui.RenderContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
class MvcUI implements IAquimaUI {

    private static final Logger LOGGER = LoggerFactory.getLogger(MvcUI.class);
    private static final String TEMPLATE_PATH = "/templates/pagesource.vm";
    private static final String RELATIVE_WEB_RESOURCES_PATH = "../../../../webresources";

    private final Map<String, MvcRenderer> rendererByTheme = new HashMap();
    private final IResourceManager resourceManager = new DefaultResourceManager();
    private final MvcExtensionManager extensionManager;
    private final ModelFactory modelFactory;
    private final PluginManager pluginManager;

    @Autowired
    public MvcUI(MvcConfiguration configuration, MvcExtensionManager extensionManager, ModelFactory modelFactory, PluginManager pluginManager) {
        String webResourcePath = String.format("%s/%s/", 
                new Object[] {RELATIVE_WEB_RESOURCES_PATH, pluginManager.getWebResourcesCacheKey()});

        this.extensionManager = extensionManager;
        for (MvcThemeConfiguration theme : configuration.getThemes()) {
            MvcRenderer renderer = new MvcRenderer(theme.getName(), theme.getTemplateGroupFile(), theme.getTemplateName(), this.resourceManager, this.extensionManager, modelFactory, configuration.isDevelopmentMode(), webResourcePath);
            this.rendererByTheme.put(theme.getName().toLowerCase(), renderer);
        }

        this.modelFactory = modelFactory;
        this.pluginManager = pluginManager;
    }

    public IMvcExtensionManager getExtensionManager() {
        return this.extensionManager;
    }

    public String getName() {
        return "mvc";
    }

    public String getUrl(String sessionId) {
        return String.format("session/%s/mvc/index.html", new Object[]{sessionId});
    }

    public boolean hasCache() {
        return true;
    }

    public boolean resetCache() {
        for (MvcRenderer renderer : this.rendererByTheme.values()) {
            renderer.reset();
        }
        return true;
    }

    public void render(Writer target, String theme, String sessionId, IPage currentPage, ILanguage currentLanguage, ILanguage defaultLanguage, int sessionTimeout) throws Exception {
        MvcRenderer renderer = (MvcRenderer) this.rendererByTheme.get(theme.toLowerCase());
        if (renderer == null) {
            throw new InvalidArgumentException("Theme '" + theme + "' does not exists.");
        }

        renderer.render(target, sessionId, currentPage, currentLanguage, defaultLanguage, sessionTimeout);
    }

    public void writeSource(ICompositeElement element, RenderContext context, Writer result) throws IOException {
        final IAquimaSession aquimaSession = context.getSession();
        final IPortalSession portalSession = aquimaSession.getPortalSession();

        IValueFormatDefinitionProvider formatProvider = new IValueFormatDefinitionProvider() {
            public IValueFormatDefinition getFormatDefinition(DataType dataType) {
                return aquimaSession.getValueFormatDefinition(dataType);
            }
        };

        LanguageConfiguration language = new LanguageConfiguration(portalSession.getCurrentLanguage().getCode());
        language.getPatterns().put(DataType.DATE.getName(), formatProvider.getFormatDefinition(DataType.DATE).getPattern().toLowerCase());
        language.getPatterns().put(DataType.DATETIME.getName(), formatProvider.getFormatDefinition(DataType.DATETIME).getPattern().toLowerCase());

        PageContent content = this.modelFactory.convert(portalSession.getCurrentPage(), portalSession.getConfiguration().getDefaultLanguage().getCode(), portalSession.getCurrentLanguage().getCode(), portalSession.getCurrentLanguage().getDefaultFormatter());
        PageModel model = new PageModel(language, new ArrayList(content.getElements().values()));
        ObjectWriter w = new ObjectMapper().writerWithDefaultPrettyPrinter();

        String devWebResPath = "../../webresources/" + pluginManager.getWebResourcesCacheKey() + "/mvc/dev";
        String modelJson = w.writeValueAsString(model);
        writePageSource(result, devWebResPath, modelJson);
    }

    private void writePageSource(Writer result, String devWebResPath, String modelJson) {
        VelocityEngine engine = new VelocityEngine();
        engine.setProperty("resource.loader", "class");
        engine.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        engine.init();
        Template template = engine.getTemplate(TEMPLATE_PATH, "UTF-8");
        VelocityContext context = new VelocityContext();
        context.put("devResPath", devWebResPath);
        context.put("model", modelJson);
        template.merge(context, result);
        LOGGER.debug("Created page source for session debugging");
    }

    public String getSourceContentType() {
        return "text/html";
    }

    public List<String> getThemes() {
        return new ArrayList(this.rendererByTheme.keySet());
    }
}