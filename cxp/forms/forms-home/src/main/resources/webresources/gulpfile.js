var gulp = require('gulp'),
    watch = require('gulp-watch')
util = require('util'),
    rimraf = require('rimraf'),
    path = require('path'),
    minifyHTML = require('gulp-minify-html'),
    //jshint = require('gulp-jshint'),
    //bump = require('gulp-bump'),
    //git = require('gulp-git'),
    //insert = require('gulp-insert'),
    //rename = require('gulp-rename'),
    ngAnnotate = require('gulp-ng-annotate'),
    wrap = require('gulp-wrap'),
    concat = require('gulp-concat'),
    //uglify = require('gulp-uglify'),
    fs = require('fs');

var releaseType = process.env.RELEASE_TYPE;

var minifyHTMLOptions = {

};

var PACKAGE_MESSAGE =
    '/**\n' +
    ' * %s - Copyright © %s Backbase B.V - All Rights Reserved\n' +
    ' * Version %s\n' +
    ' * %s\n' +
    ' * @license\n' +
    ' */\n';

// Source main folder
var srcBasePath = './themes';

// destination main folder
var destBasePath = '../../../../target/forms-home/local/webresources/static/dbe';

var templateDestSubdolder = '/html/templates/forms/';
var jsDestSubfolder = '/modules/forms/';

var templateSrcSelector = '/scripts/templates/**/*.html';
var jsSrcSelector = '/scripts/**/*.js';

function getThemesFolders(dir) {
    return fs.readdirSync(dir)
        .filter(function (file) {
            return fs.statSync(path.join(dir, file)).isDirectory();
        });
}

gulp.task('cleanJs', function () {
    console.log('Clean Js');
    return rimraf.sync(destBasePath + jsDestSubfolder);
});

gulp.task('cleanTemplate', function () {
    console.log('Clean template');
    return rimraf.sync(destBasePath + templateDestSubdolder);
});

// Creates regular and minified distributions as UMD modules
gulp.task('packageJs', [ 'cleanJs' ], function () {
    var folders = getThemesFolders(srcBasePath);

    var tasks = folders.map(function (folder) {
        console.log('Packaging theme ' + folder);

        return gulp.src(srcBasePath + '/' + folder + jsSrcSelector)
            .pipe(concat('forms-angular-ui.js'))
            .pipe(wrap({
                src: './umd-wrapper.tmpl'
            }))
            .pipe(gulp.dest(destBasePath + jsDestSubfolder + folder))
    });
});

// Creates regular and minified distributions as UMD modules
gulp.task('packageTemplate', [ 'cleanTemplate' ], function () {
    var folders = getThemesFolders(srcBasePath);

    var tasks = folders.map(function (folder) {
        console.log('Packaging template ' + folder);

        return gulp.src(srcBasePath + '/' + folder + templateSrcSelector)
            .pipe(minifyHTML(minifyHTMLOptions))
            .pipe(gulp.dest(destBasePath + templateDestSubdolder + folder))
    });
});


//watches and runs package when src changes. Useful for running unit tests against the build
gulp.task('watch', [], function () {
    console.log('Watching for changes...');
    gulp.watch(srcBasePath + '/**/' + jsSrcSelector, [ 'packageJs' ]);
    gulp.watch(srcBasePath + '/**/' + templateSrcSelector, [ 'packageTemplate' ]);
});

gulp.task('default', [ 'cleanJs', 'packageJs', 'cleanTemplate', 'packageTemplate']);
