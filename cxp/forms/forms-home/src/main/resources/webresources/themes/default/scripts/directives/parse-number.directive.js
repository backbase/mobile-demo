(function () {
    'use strict';

    var app = angular.module('forms-ui'),
        BB_PARSENUMBER = 'parseNumber';

    app.directive(BB_PARSENUMBER, function () {
        return {
            require: 'ngModel',
            priority: 2,
            link: function (scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function (value) {
                    return '' + value;
                });
                ngModel.$formatters.push(function (value) {
                    return parseFloat(value);
                });
            }
        };
    });
})();

