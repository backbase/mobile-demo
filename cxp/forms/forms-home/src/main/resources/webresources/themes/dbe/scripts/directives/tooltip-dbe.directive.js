(function () {
    'use strict';

    var app = angular.module('forms-ui'),
        BB_TOOLTIP = 'tooltipDbe';

    app.directive(BB_TOOLTIP, /*@ngInject*/ function ($log, formUtil) {
        return {
            restrict: 'A',
            scope: {
                element: '='
            },
            link: function (scope, element) {
                var explain = scope.element.explainText || element.closest('.explain-item').attr('data-explain-text');

                if (explain) {

                    scope.$watch('element.explainText', function () {

                        var buildTooltip = function (element, event, hasExplanationOnField) {

                            element.tooltip({
                                trigger: 'manual',
                                position: 'top',
                                container: element,
                                title: explain,
                                template: '<div class="tooltip" role="tooltip">\
                                        <div class="tooltip-arrow"></div>\
                                        <a class="tooltip-close glyphicon glyphicon-remove"></a>\
                                        <div class="tooltip-inner"></div>\
                                      </div>'
                            });

                            $(document).on('show.bs.tooltip', function (e) {
                                element.not(e.target).tooltip('hide');
                            });

                            if(event==='click'){

                            }


                            if (!hasExplanationOnField) {
                                element.parent().addClass('has-explain-text');
                                element.on('click',function(){
                                    element.tooltip('toggle');
                                });
                            }else{
                                element.on('focus',function(){
                                    element.tooltip('show');
                                });
                            }
                        };

                        element.find('.tooltip-dbe').remove();
                        element.find('.has-explain-text').removeClass('has-explain-text');

                        if (explain) {
                            var hasExplanationOnField = scope.element.styles.indexOf("explain_text_on_field") != -1;

                            if (hasExplanationOnField) {
                                buildTooltip(element, true);
                            } else {
                                var elementFocus = angular.element('<a class="tooltip-dbe dbe dbe-info"></a>');
                                element.find('label').append(elementFocus);
                                buildTooltip(elementFocus);
                            }
                        }
                    })

                }

            }
        };
    });
})();