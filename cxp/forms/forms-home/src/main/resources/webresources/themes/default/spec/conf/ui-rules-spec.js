describe('UI Rules', function () {

	// Load your module.
	beforeEach(module('forms-ui'));
	var md = new MobileDetect(window.navigator.userAgent);

	var uiRules,
		formUtil;

	beforeEach(inject(function($injector) {
		uiRules  = $injector.get('uiRules');
		formUtil = $injector.get('formUtil');
	}));

	it('maps the right template for a given element', function() {

		//shortcut
		var elements = uiRules.elements;
		var find = formUtil.findTemplate;
		var t, el = {};

		//page
		el.type = 'page';
		t = find(el, elements);
		expect(t).toBe('elements/page.html');

		//container
		el.type = 'container';
		t = find(el, elements);
		expect(t).toBe('elements/container.html');

		//contentitem
		el.type = 'contentitem';
		t = find(el, elements);
		expect(t).toBe('elements/contentitem.html');

		//failedelement
		el.type = 'failedelement';
		t = find(el, elements);
		expect(t).toBe('elements/failedelement.html');

		//field
		el.type = 'field';
		t = find(el, elements);
		expect(t).toBe('elements/field.html');

		//textitem
		el.type = 'textitem';
		t = find(el, elements);
		expect(t).toBe('elements/textitem.html');

		//asset
		el.type = 'asset';
		t = find(el, elements);
		expect(t).toBe('elements/asset.html');

		//button
		el.type = 'button';
		t = find(el, elements);
		expect(t).toBe('elements/button.html');
	});

	it('maps the right control template for a given element', function() {

		//shortcut
		var controls = uiRules.controls;
		var find = formUtil.findTemplate;
		var t, el = {};

		//input text
		el.dataType = 'text';
		el.hasDomain = false;
		t = find(el, controls);
		expect(t).toBe('controls/input-text.html');

		//input currency
		el.dataType = 'currency';
		t = find(el, controls);
		expect(t).toBe('controls/input-currency.html');

		//input date
		el.dataType = 'date';
		t = find(el, controls);
		if(md.mobile())
			expect(t).toBe('controls/input-date.html');
		else
			expect(t).toBe('controls/input-date-desktop.html');


		//input datetime
		el.dataType = 'datetime';
		t = find(el, controls);
		if(md.mobile())
			expect(t).toBe('controls/input-datetime.html');
		else
			expect(t).toBe('controls/input-datetime-desktop.html');


		//input integer
		el.dataType = 'integer';
		el.hasDomain = false;
		t = find(el, controls);
		expect(t).toBe('controls/input-integer.html');

		//input percentage
		el.dataType = 'percentage';
		el.hasDomain = false;
		t = find(el, controls);
		expect(t).toBe('controls/input-percentage.html');

		//input checkbox
		el.dataType = 'boolean';
		t = find(el, controls);
		expect(t).toBe('controls/input-checkbox.html');

		//checkboxgroup
		el.hasDomain = true;
		el.multiValued = true;
		t = find(el, controls);
		expect(t).toBe('controls/checkboxgroup.html');

		//select
		el.hasDomain = true;
		el.multiValued = false;
		t = find(el, controls);
		expect(t).toBe('controls/dropdown-select.html');
	})
});