(function() {
    'use strict';

    var controlsRules = [{
        template: 'controls/dropdown-select.html',
        test: function(el) {
            //adasdasdadsa
            return el.type == 'field' && el.hasDomain && !el.multiValued && el.styles.indexOf('dropdown')>-1;
        }
    },{
        template: 'controls/radiogroup.html',
        test: function(el) {
            return el.type == 'field'
                && el.hasDomain
                && !el.multiValued
                && el.domain.length < 3
                && el.styles.indexOf('dropdown') == -1
                && el.styles.indexOf('Autocomplete') == -1
                && el.dataType != 'entity';
        }
    },{
        template: 'controls/input-date-splitted.html',
        test: function(el) {
            return el.dataType === 'date';
        }
    },{
        template: 'controls/input-telephone.html',
        test: function(el) {
            return el.styles.indexOf('BB_Telephone') != -1;
        }
    },{
        template: 'controls/input-iban.html',
        test: function(el) {
            return el.styles.indexOf('BB_Iban') != -1;
        }
    },{
        template: 'controls/input-password.html',
        test: function(el) {
            return el.styles.indexOf('BB_Password') != -1;
        }
    }];

    var elementsRules = [{
        template: 'elements/field.html',
        test: function(el) {
            return el.type == 'field';
        }
    },{
        template: 'breadcrumbs/breadcrumb-container.html',
        test: function(el) {
            return el.styles.indexOf('breadcrumb') != -1;
        }
    },{
        template: 'containers/fieldset.html',
        test: function(el) {
            return el.styles.indexOf('fieldset') != -1;
        }
    },{
        template: 'containers/panel.html',
        test: function(el) {
            return el.type === 'container'
                && el.contentStyle === 'BB_Panel';
        }
    },{
        template: 'containers/explain-item.html',
        test: function(el) {
            return el.type === 'container'
              && el.contentStyle === 'BB_Explain_Item';
        }
    },{
        template: 'elements/text-back-button.html',
        test: function(el) {
            return el.type === 'textitem' && el.styles.indexOf('BB_TextBackButton') != -1;
        }
    },{
        template: 'elements/loan-simulator.html',
        test: function(el) {
            return el.contentStyle === 'BB_LoanSimulator';
        }
    },{
        template: 'elements/loan-simulator-slider.html',
        test: function(el) {
            return el.styles.indexOf('BB_LoanSimulator_Slider') != -1;
        }
    },{
        template: 'elements/loan-simulator-cost.html',
        test: function(el) {
            return el.styles.indexOf('loan_simulator_cost') != -1;
        }
    },{
        template: 'containers/media_forms.html',
        test: function(el) {
            return el.styles.indexOf('BB_Media_Forms') != -1;
        }
    },{
        template: 'elements/loan-overview.html',
        test: function(el) {
            return el.styles.indexOf('loans_overview') != -1;
        }
    },{
        template: 'elements/icon.html',
        test: function(el) {
            return el.styles.indexOf('BB_Icon') != -1;
        }
    },{
        template: 'contentitems/link.html',
        test: function(el) {
            return el.type === 'textitem' && (el.styles.indexOf('LinkToPortalPage') > -1 || el.styles.indexOf('LinkToPortalFormServer') > -1);
        }
    },{
        template: 'containers/alert.html',
        test: function(el) {
            return el.styles.indexOf('alert') > -1;
        }
    },{
        template: 'elements/redirect.html',
        test: function(el) {
            return el.type === 'textitem' && el.styles.indexOf('RedirectToPortalPage') != -1;
        }
    }];


    var app = angular.module('forms-ui');

    app.config(["$provide", "$logProvider", function($provide, $logProvider) {

        $provide.decorator('uiRules', ["$delegate", function($delegate) {
            var uiRules = $delegate;
            uiRules.elements = uiRules.elements.concat(elementsRules);
            uiRules.controls = uiRules.controls.concat(controlsRules);
            return uiRules;
        }]);

    }]);

})();