(function() {
    'use strict';

    var BB_ERROR = 'error';

    var app = angular.module('forms-ui');

    /**
     * Element directive (bbControl)
     * This directive is designed to render single controls. It expects and element object as an attribute
     */
    app.directive(BB_ERROR,
        /*@ngInject*/ function( $log, templateBase, debugEnabled) {
            return {
                restrict: 'EA',
                require: '^bbForm',
                templateUrl: templateBase + '/form/error.html',
                scope: {
                    error : '='
                },
                link: function(scope, element, attrs, formsController) {
                    scope.lookupMessage = formsController.lookupMessage;
                }
            };
        });
})();