(function() {
    'use strict';

    var app = angular.module('forms-ui');

    app.directive('loansOverview', /*@ngInject*/ function($filter) {
        return {
            restrict: 'C',
            require : '^bbForm',
            link: function (scope, element, attr, formsController) {
                var overview = [];

                angular.forEach(scope.element.children, function(element){
                    var element = formsController.lookupElement(element);
                    var value = element.values.length ? element.values[0] : undefined;

                    overview.push({
                        label : element.questionText,
                        value : $filter('loanFilter')(value, element.dataType)
                    });
                })

                scope.overview = overview;
            }
        };
    });
})();


