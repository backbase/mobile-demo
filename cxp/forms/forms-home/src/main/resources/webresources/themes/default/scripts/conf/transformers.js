(function() {
    'use strict';

    var app = angular.module('forms-ui');

    app.factory('transformers', function(){
        return {
            marshalers:[
                function serializeDates(event, config){
                    var formats = config.language.patterns;

                    if(event.values && event.values.dataType  && event.values.dataType.indexOf('date')>-1){
                        event.values = event.values.map(function(val){
                            var m = moment(val);
                            return m.format((event.values.dataType === 'date') ? formats.date : formats.datetime);
                        });
                    }

                    return event;
                }
            ],
            unmarshalers:[
                function unserializeDates(element, config){
                    var formats = config.language.patterns;

                    if(element.type == 'field' && element.dataType.indexOf('date')>-1){
                        element.values = element.values.map(function(val){
                            return moment(val, (element.dataType === 'date')? formats.date : formats.datetime).toDate();
                        });
                    }
                    return element;
                },
                function displayLength(element){
                    if(element.type == 'field' && element.displayLength == -1){
                        delete element.displayLength;
                    }
                    return element;
                }
            ]
        };
    });
})();