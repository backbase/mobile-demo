/*jslint browser: true*/
(function () {
  'use strict';

  var BB_FORM = 'bbForm';

  var app = angular.module('forms-ui');

  /**
   * Form Directive (bbForm)
   * Root directive for rendering a form. This is the public interface for rendering forms.
   */
  app.directive(BB_FORM, /*@ngInject*/ function (templateBase) {
    return {
      restrict: 'EA',
      templateUrl: templateBase + '/form/form.html',
      scope: {
        sessionId: '=',
        runtimeUrl: '=',
        project: '=',
        flow: '=',
        version: '=',
        languageCode: '=',
        theme: '=',
        parameters: '='
      },
      controller: /*@ngInject*/ function ($scope, $window, $cookies, $timeout, $log, $filter, debugEnabled, formFactory, formUtil, formConsts, messages) {
        var sessionOpts;
        $scope.model = {};
        $scope.config = {};

        $scope.formError = false;
        $scope.loading = true;
        $scope.debugEnabled = debugEnabled;

        //setup messages
        $scope.messages = formUtil.getLocalizedMessages($scope.languageCode, messages);

        //starts the rendering process given the form options
        function loadModel() {
          $scope.loading = true;

          //intro logging
          if ($scope.sessionId) {
            $log.info('%s: Starting form with session id: [%s]', BB_FORM, $scope.sessionId);
          } else {
            $log.info('%s: Starting form for [%s]', BB_FORM, $scope.project);
          }

          if (debugEnabled) {
            $log.info(BB_FORM + ': Debug mode is on');
            $log.debug(BB_FORM + ': Using form options: ');
            $log.debug('\tProject: ', $scope.project);
            $log.debug('\tVersion: ', $scope.version);
            $log.debug('\tFlow: ', $scope.flow);
            $log.debug('\tLanguage Code: ', $scope.languageCode);
            $log.debug('\tSession ID: ', $scope.sessionId);
          }

          sessionOpts = {
            sessionId: $scope.sessionId,
            theme: $scope.theme,
            project: $scope.project,
            flow: $scope.flow,
            version: $scope.version,
            languageCode: $scope.languageCode
          };

          //form factory will take care of starting a session and getting a model
          formFactory.init($scope.runtimeUrl, sessionOpts, $scope.parameters);

          return formFactory.getModel()
            .then(function (model) {
              $scope.formError = false;

              $log.info('%s: Initial model retrieved.', BB_FORM);

              $scope.model = model;

              $scope.$evalAsync(function () {
                angular.element(document).triggerHandler('bbf.refresh', {deltas: []});
              });
            }, function (err) { //catch (ie8 safe)
              $scope.formError = err;
              //TODO: form is not cleared if there is a runtime error when formOpts are changed
              $scope.model = null;
              $log.error(err);
            })
            .finally(function () {
              $scope.loading = false;
            });
        };
        loadModel();


        //this returns the root element in the model so tbe form directive can start rendering the form tree
        $scope.getRootElement = function () {
          var page = $filter('filter')($scope.model.elements, {type : 'page'}, true);
          return page ? page[0] : {};
        };

        //returns an element from the model, given a key
        $scope.lookupElement = function (elementKey) {
          var element = $filter('filter')($scope.model.elements, {key : elementKey}, true);
          return element ? element[0] : undefined;
        };

        //returns a UI message, given a message key
        $scope.lookupMessage = function (messageKey) {
          var message = $scope.messages && $scope.messages[messageKey];
          return message || '';
        };


        var refreshCallback = function (deltas, element) {
          $timeout(function () {
            angular.element(document).triggerHandler('bbf.refresh', { event: element.key, deltas: deltas });
          }, 0);
        };

        var refreshCallbackOnError = function (e) { //catch (ie8 safe)
          if (e.type === formConsts.SESSION_EXPIRED_ERR) {
            $scope.model = null;
          } else {
            $log.error(e);
          }
          //timeout forces the null model to go through a digest, so the isolated scopes
          //of the bbElements will be recompiled
          $timeout(function () {
            loadModel();
          });
        };


        //sends the current model to the server and applies the delta update
        $scope.refresh = //formUtil.debounce(300,false,
          function (element, data) {
            if (element) {
              if (data){
                formFactory.applyChanges($scope.model, data)
                  .then(function(deltas){
                    refreshCallback(deltas, element)
                  },function(e){
                    refreshCallbackOnError(e);
                  })
                  .finally(function () {
                    $scope.loading = false;
                  });
              }
              //handle refresh events
              else if (element.refresh || element.type === 'button') {
                $log.debug('%s: Refreshing field [scope.refresh()]', BB_FORM);
                $scope.loading = true;

                formFactory.updateModel($scope.model, element)
                  .then(function(deltas){
                    refreshCallback(deltas, element)
                  },function(e){
                    refreshCallbackOnError(e);
                  })
                  .finally(function () {
                    $scope.loading = false;
                  });
              }
            }
          };
        //);

        // check for a specific style
        $scope.hasStyle = function (elem, style) {
          return elem.styles.indexOf(style) > -1;
        };

        // check for a specific style
        $scope.hasStyleContaining = function (elem, style) {
          return $scope.stylesContaining(elem,style).length > 0;
        };

        // check for a specific style
        $scope.stylesContaining = function (elem, style) {
          return elem.styles.filter(function (e) {
            return e.indexOf(style) > -1;
          });
        };

        this.refresh = $scope.refresh;
        this.setConfig = function (config) {
          $scope.config = config;
        };
        this.hasError = $scope.hasError;
        this.hasStyle = $scope.hasStyle;
        this.hasStyles = $scope.hasStyles;
        this.hasStyleContaining = $scope.hasStyleContaining;
        this.stylesContaining = $scope.stylesContaining;
        this.lookupElement = $scope.lookupElement;
        this.lookupMessage = $scope.lookupMessage;
      }
    };
  });
})();