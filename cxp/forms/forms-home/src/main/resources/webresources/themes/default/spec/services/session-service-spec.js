describe('Session service', function () {

	// Load your module.
	beforeEach(module('forms-ui'));

	var sessionService, $httpBackend, $scope;

	beforeEach(inject(function($injector) {
		$httpBackend = $injector.get('$httpBackend');
		$scope = $injector.get('$rootScope').$new();
		sessionService = $injector.get('sessionService');
	}));

	//full actual html page resonse
	var sessionPageHtml = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml">\n<head>\n<meta http-equiv="X-UA-Compatible" content="IE=EDGE" /><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>\n<style type="text/css">html, body {height:100%;margin:0;}</style><link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/run/VAADIN/themes/development/favicon.ico" /><link rel="icon" type="image/vnd.microsoft.icon" href="/run/VAADIN/themes/development/favicon.ico" /><title>Kinderbijslag&#32;Wetwijziging</title><script type="text/javascript" src="../../../current-web-resources/dev/js/jquery.js"></script><script type="text/javascript" src="../../../current-web-resources/dev/js/development.js"></script>\n</head>\n<body scroll="auto" class="v-generated-body">\n<script type="text/javascript">\n//<![CDATA[\nif(!vaadin || !vaadin.vaadinConfigurations) {\ nif(!vaadin) { var vaadin = {} \nvaadin.vaadinConfigurations = {};\nif (!vaadin.themesLoaded) { vaadin.themesLoaded = {}; }\nvaadin.debug = true;\n}\nvaadin.vaadinConfigurations["RuntimeservervaadinsessionTools9f383affa49b4bd5b5a6986e06f6b20f-2009578112"] = {appUri:\'/run/server/vaadin/sessionTools-9f383aff-a49b-4bd5-b5a6-986e06f6b20f\', standalone: true, themeUri:"/run/VAADIN/themes/development", versionInfo : {vaadinVersion:"6.8.10",applicationVersion:"NONVERSIONED"},"comErrMsg": {"caption":"Communication problem","message" : "Take note of any unsaved data, and <u>click here<\/u> to continue.","url" : null},"authErrMsg": {"caption":"Authentication problem","message" : "Take note of any unsaved data, and <u>click here<\/u> to continue.","url" : null}};\n//]]>\n</script>\n<iframe tabIndex=\'-1\' id=\'__gwt_historyFrame\' style=\'position:absolute;width:0;height:0;border:0;overflow:hidden;\' src=\'javascript:false\'></iframe>\n<script language=\'javascript\' src=\'/run/VAADIN/widgetsets/com.vaadin.terminal.gwt.DefaultWidgetSet/com.vaadin.terminal.gwt.DefaultWidgetSet.nocache.js?1424258157724\'></script>\n<script type="text/javascript">\n//<![CDATA[\nif(!vaadin.themesLoaded[\'development\']) {\nvar stylesheet = document.createElement(\'link\');\nstylesheet.setAttribute(\'rel\', \'stylesheet\');\nstylesheet.setAttribute(\'type\', \'text/css\');\nstylesheet.setAttribute(\'href\', \'/run/VAADIN/themes/development/styles.css\');\ndocument.getElementsByTagName(\'head\')[0].appendChild(stylesheet);\nvaadin.themesLoaded[\'development\'] = true;\n}\n//]]>\n</script>\n<script type="text/javascript">\n//<![CDATA[\nsetTimeout(\'if (typeof com_vaadin_terminal_gwt_DefaultWidgetSet == "undefined") {alert("Failed to load the widgetset: /run/VAADIN/widgetsets/com.vaadin.terminal.gwt.DefaultWidgetSet/com.vaadin.terminal.gwt.DefaultWidgetSet.nocache.js?1424258157724")};\',15000);\n//]]>\n</script>\n<div id="RuntimeservervaadinsessionTools9f383affa49b4bd5b5a6986e06f6b20f-2009578112" class="v-app v-theme-development v-app-VaadinWidgetApplication" ><div class="v-app-loading"></div></div>\n<noscript>You have to enable javascript in your browser to use an application built with Vaadin.</noscript></body>\n</html>';

	it('fails early if incomplete options are givne', function(done) {

		//missing version
		var formOpts = {
			project: 'one',
			flow: 'Start',
			languageCode: 'nl'
		};

		sessionService.startSession('/run', formOpts).then(null, function(err) {
			expect(err.message).toContain('Invalid form options');
			done();
		});

		//forces promises to resolve
		$scope.$digest();
	});

	it('starts a new session', function(done) {

		var sessionId = '9f383aff-a49b-4bd5-b5a6-986e06f6b20f';

		var formOpts = {
			project: 'one',
			flow: 'Start',
			languageCode: 'nl',
			version: '1.0'
		};

		var expectedStartUrl = '/run/server/start?project=one&flow=Start&languageCode=nl&version=1.0&ui=mvc&theme=forms';
		var expectedSubscribeUrl = '/run/server/session/' + sessionId + '/api/subscribe';

		//these will be called in sequence
		$httpBackend.expectGET(expectedStartUrl).respond(200, sessionPageHtml);
		$httpBackend.expectPOST(expectedSubscribeUrl).respond(200, '');

		sessionService.startSession('/run', formOpts).then(function(data) {
			expect(data).toBe(sessionId);
			done();
		});

		$httpBackend.flush();
	});
});