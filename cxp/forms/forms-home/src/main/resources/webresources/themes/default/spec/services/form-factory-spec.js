describe('Form factory', function () {

	// Load your module.
	beforeEach(module('forms-ui'));

	var formFactory, modelService, sessionService, $scope, $q;

	beforeEach(inject(function($injector) {
		$q = $injector.get('$q');
		$scope = $injector.get('$rootScope').$new();
		sessionService = $injector.get('sessionService');
		modelService = $injector.get('modelService');
		formFactory =  $injector.get('formFactory');
		formFactory.sessionId = null;

	}));

	var dummyModel = {
		elements: [ 'one', 'two', 'three'],
		language: {
			languageCode: "nl-NL"
		}
	};

	it('gets a form model with no starting session', function(done) {

		var sessionId = '123-abc';

		spyOn(sessionService, "startSession").and.returnValue(sessionId);
		spyOn(modelService, "getModel").and.returnValue(dummyModel);

		formFactory.init('/run', {});
		formFactory.getModel().then(function(model) {
			expect(model).toEqual(dummyModel);
			expect(sessionService.getSessionId()).toBe(sessionId);
			done();
		});

		//forces promises to resolve
		$scope.$digest();
	});

	it('gets a form model if the starting session has expired', function(done) {

		var sessionId = '123-abc';

		spyOn(sessionService, "startSession").and.returnValue(sessionId);
		spyOn(modelService, "getModel").and.callFake(function() {
			var calls = modelService.getModel.calls.count();
			if(calls === 1) {
				//first call returns an expired session
				var err = new Error();
				err.type = 'SESSION_EXPIRED';
				return $q.reject(err);
			} else if(calls === 2) {
				//second call works
				return dummyModel;
			}
		});

		formFactory.init('/run', {}, sessionId);
		formFactory.getModel().then(function(model) {
			expect(model).toEqual(dummyModel);
			expect(formFactory.getSessionId()).toBe(sessionId);
			expect(modelService.getModel.calls.count()).toEqual(2);
			done();
		});

		//forces promises to resolve
		$scope.$digest();
	});

	it('gets a form model with an active starting session', function(done) {

		spyOn(modelService, "getModel").and.returnValue(dummyModel);

		formFactory.init('/run', {}, '123-abc');
		formFactory.getModel().then(function(model) {
			expect(model).toEqual(dummyModel);
			done();
		});

		//forces promises to resolve
		$scope.$digest();
	});

	it('throws if there was an unexpected error', function(done) {

		var sessionId = '123-abc';

		var unknownError = new Error('unknown error');
		spyOn(modelService, "getModel").and.returnValue($q.reject(unknownError));

		formFactory.init('/run', {}, sessionId);
		formFactory.getModel().then(null, function(err) {
			expect(err).toBe(unknownError);
			done();
		});

		//forces promises to resolve
		$scope.$digest();
	});
});