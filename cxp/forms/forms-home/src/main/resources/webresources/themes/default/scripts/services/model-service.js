(function() {
	'use strict';

	var MODEL_SERVICE = 'modelService';

	var app = angular.module('forms-ui');

	/**
	 * Low (ish) level service for getting and updating the form model from the server
	 */
	app.factory(MODEL_SERVICE, /*@ngInject*/ function($http, $log, formUtil, transformerService) {

		function ModelService() {
			this.runtimeUrl = null;
			this.sessionId = null;
		}

		/**
		 * Initalizes the service
		 * @param runtimeUrl
		 * @param sessionId
		 */
		ModelService.prototype.init = function(runtimeUrl, sessionId) {
			this.runtimeUrl = runtimeUrl;
			this.sessionId = sessionId;
		};

		/**
		 * Gets the model from the server
		 * Throws if the session has expired
		 * @returns {Promise|*}
		 */
		ModelService.prototype.getModel = function() {
			var modelUrl = this.runtimeUrl + '/server/session/' + this.sessionId + '/api/subscribe/' + this.sessionId;
			$log.debug('%s: Sending POST to %s', MODEL_SERVICE, modelUrl);
			return $http.post(modelUrl, {}).then(function(res) {

                transformerService.setConfig({
                    language : formUtil.normalizeFormats(res.data.language)
                });

                res.data.elements = res.data.elements.map(transformerService.unmarshal);

				return  res.data;
			}).then(null, function(res) { //catch (ie8 safe)
				throw formUtil.getNormalizedErrorFromResponse(res);
			});
		};

		/**
		 * Updates the model given a list of delta changes
		 * @param deltas
		 * @returns {*|Promise}
		 */
		ModelService.prototype.updateModel = function(deltas) {
			var updateUrl = this.runtimeUrl + '/server/session/' + this.sessionId + '/api/subscription/' + this.sessionId + '/handleEvent';

            deltas.fields = deltas.fields.map(transformerService.marshal);

			$log.debug('%s: Sending POST to %s', MODEL_SERVICE, updateUrl);
			return $http.post(updateUrl, JSON.stringify(deltas)).then(function(res) {
                res.data.events = res.data.events.map(function(event){
                    $log.debug(event.changes.changes);
                    event.changes.changes = event.changes.changes.map(function(change){
											if(change.model)
												change.model = transformerService.unmarshal(change.model);

											return change
										});
                    return event;
                });

				return res.data;
			}).then(null, function(res) { //catch (ie8 safe)
				throw formUtil.getNormalizedErrorFromResponse(res);
			});
		};

		return new ModelService();
	});
})();