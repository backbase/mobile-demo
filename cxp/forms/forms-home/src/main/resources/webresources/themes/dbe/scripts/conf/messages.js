(function() {
    'use strict';

    var app = angular.module('forms-ui');

    app.config(["$provide", "$logProvider", function($provide, $logProvider) {

        $provide.decorator('messages', ["$delegate", function($delegate) {
            var messages = $delegate;
            messages.it =  {
                requiredField: 'Questo campo è obbligatorio',
                chooseOption:  '-- Seleziona --',
                defaultCurrency: '€',
                optionalField: '(facoltativo)',
                formError: 'An error occurred rendering this form'
            };
            return messages;
        }]);

    }]);

})();


