(function() {
    'use strict';

    var app = angular.module('forms-ui');

    app.directive('sliderWrapper', /*@ngInject*/ function($filter) {
        return {
            restrict: 'CA',
            require: '^bbForm',
            link: function(scope, element, attrs, formsController) {
                var typeOfFilter = scope.element.name.indexOf('Duration') != -1 ? 'month' : 'currency';

                scope.formatValue = function(index){
                    if(scope.element.domain && scope.element.domain.length)
                        return $filter('loanFilter')(scope.element.domain[index].displayValue, typeOfFilter);
                    return "";
                };

                scope.setValue = function(){
                    if(scope.element.domain && scope.element.domain.length){
                        scope.element.values[0] = scope.element.domain[scope.selected].value;
                    }
                    formsController.refresh(scope.element);
                    scope.tooltip = 'always';
                };


                var initialValue = $filter('filter')(scope.element.domain, function(domain){
                    return domain.value == scope.element.values[0];
                });

                scope.tooltip = initialValue.length ?  'always' : 'show';

                if(initialValue.length){
                    var selectedIndex = scope.element.domain.indexOf(initialValue[0]);
                    scope.selected = selectedIndex;
                }
            }
        }
    });

    app.directive('sliderButtons', /*@ngInject*/ function() {
        return {
            restrict: 'C',
            replace : true,
            template : '<div class="text-muted small">\
                            <button class="btn btn-danger btn-sm" ng-click="back()" ng-disabled="selected == 0">-</button> \
                            <button class="btn btn-danger btn-sm" ng-click="next()" ng-disabled="selected == (element.domain.length - 1)">+</button> \
                        </div>',
            link: function (scope, element, attr) {

                scope.back = function(){
                    scope.selected = scope.selected - 1;
                    scope.setValue(scope.selected);
                };

                scope.next = function(){
                    scope.selected = scope.selected + 1;
                    scope.setValue(scope.selected);
                };
            }
        }
    });


    app.directive('sliderLimits', /*@ngInject*/ function($filter) {
        return {
            restrict: 'C',
            replace : true,
            template : '<div class="row text-muted small">\
                           <div class="col-sm-6 text-left">{{min}}</div> \
                           <div class="col-sm-6 text-right">{{max}}</div> \
                        </div>',
            link: function (scope) {
                var typeOfFilter = scope.element.name.indexOf('Duration') != -1 ? 'month' : 'currency';

                scope.$watch('element.domain', function(domain){
                    if (domain && domain.length){
                        var min = domain[0].displayValue;
                        var max = domain[domain.length - 1].displayValue;
                        scope.min = $filter('loanFilter')(min, typeOfFilter);
                        scope.max = $filter('loanFilter')(max, typeOfFilter);
                    }
                });
            }
        };
    });


})();

