(function () {
    'use strict';
    var app = angular.module('forms-ui'),
        BB_TOGGLEBUTTON = 'toggleButton';

    app.directive(BB_TOGGLEBUTTON, /*@ngInject*/ function ($log, $parse, $filter, debugEnabled, formUtil, templateBase) {
        return {
            restrict: 'A',
            require: 'ngModel',
            scope: {
                element: '='
            },
            link: function (scope, element, attrs, ngModel) {
                //logging
                if (debugEnabled && scope.element) {
                    $log.debug('%s: linking toggle button: %s', BB_TOGGLEBUTTON, formUtil.elementToString(scope.element));
                    $log.debug('%s: ...using template: %s', BB_TOGGLEBUTTON, templateBase + '/controls/input-toggle.html');
                }

                var trueValue = $parse(attrs.ngTrueValue)(scope) || true;
                var falseValue = $parse(attrs.ngFalseValue)(scope) || false;

                var trueLabel = 'On';
                var falseLabel = 'Off';

                if (scope.element.hasDomain) {
                    var filter = $filter('filter');
                    trueLabel = filter(scope.element.domain, {value: trueValue})[0].displayValue;
                    falseLabel = filter(scope.element.domain, {value: falseValue})[0].displayValue;
                }

                scope.$watch(function () {
                    return ngModel.$viewValue;
                }, function (ov, nv) {
                    if (ov !== nv) {
                        element.change();
                    }
                });

                var modelState = trueValue === scope.element.values[0];

                element
                    .bootstrapToggle({
                        on: trueLabel,
                        off: falseLabel
                    })
                    .bootstrapToggle(modelState ? 'on' : 'off')
                    .change(function () {
                        var viewState = element.prop('checked');
                        if (viewState !== ngModel.$viewValue) {
                            ngModel.$setViewValue(viewState);
                            scope.$apply();
                        }
                    });
            }
        };
    });
})();