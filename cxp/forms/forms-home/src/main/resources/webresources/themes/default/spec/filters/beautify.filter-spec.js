describe('Style to Class Names Filter', function() {
	var beautify;

	beforeEach(module('forms-ui'));

	beforeEach(inject(function($injector) {
		beautify = $injector.get('$filter')('beautify');
	}));

	it('converts forms style array to a html class string', function() {

		var classString;

		classString = beautify([]);
		expect(classString).toBe('');

		classString = beautify([ 'slider' ]);
		expect(classString).toBe('slider');

		classString = beautify([ 'slider', 'date_picker', 'tool-tip']);
		expect(classString).toBe('slider date-picker tool-tip');
	});

});