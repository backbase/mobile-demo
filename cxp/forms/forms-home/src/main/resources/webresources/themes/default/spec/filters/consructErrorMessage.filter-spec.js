describe('Construct Error Message Fitler', function() {
	var constructErrorMessage;

	beforeEach(module('forms-ui'));

	beforeEach(inject(function($injector) {
		constructErrorMessage = $injector.get('$filter')('constructErrorMessage');
	}));

	it('given a single error message, it formats an element\'s error', function() {

		var element = {
			messages: [{
				type: "ERROR",
				text: "Dit is geen geldig rekeningnummer"
			}]
		};

		var formattedMessage = constructErrorMessage(element);
		expect(formattedMessage).toBe('Dit is geen geldig rekeningnummer');
	});

	it('given a single error message with a question placeholder, it formats an element\'s error', function() {

		var element = {
			questionText: 'When can I go home?',
			messages: [{
				type: "ERROR",
				text: "You answer to [Q] is not good!"
			}]
		};

		var formattedMessage = constructErrorMessage(element);
		expect(formattedMessage).toBe('You answer to "When can I go home?" is not good!');
	});


	//TODO: the implementation of this test case does not look good. What should it do?
	it('given a multiple error messages, it concatenates multiple error messages together', function() {

		var element = {
			messages: [{
				type: "ERROR",
				text: "Dit is geen geldig rekeningnummer"
			},
			{
				type: "ERROR",
				text: "Bad input"
			}]
		};

		var formattedMessage = constructErrorMessage(element);
		expect(formattedMessage).toBe('Dit is geen geldig rekeningnummerBad input');
	});

});