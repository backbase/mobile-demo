(function () {
    'use strict';

    var app = angular.module('forms-ui');

    /**
     * This list of rules contains the logic to choose an Angular template given a forms model element
     * Two sets of rules exist, 'elements' used by the bb-element directive, and 'controls' used by
     * the bb-control directive
     * These can be customized via decoration
     */
    app.factory('uiRules', function (formUtil) {
        return {
            elements: [

                //fall back
                {
                    template: 'elements/other.html',
                    test: function () {
                        return true;
                    }
                },

                ///////////////////////////////////
                // Elements
                ///////////////////////////////////

                {
                    template: 'elements/page.html',
                    test: function (el) {
                        return el.type === 'page';
                    }
                },{
                    template: 'elements/container.html',
                    test: function (el) {
                        return el.type === 'container';
                    }
                },{
                    template: 'breadcrumbs/breadcrumb-container.html',
                    test: function (el) {
                        return el.type === 'container' && el.contentStyle === 'breadcrumbcontainer';
                    }
                },{
                    template: 'elements/failedelement.html',
                    test: function (el) {
                        return el.type === 'failedelement';
                    }
                },{
                    template: 'elements/field.html',
                    test: function (el) {
                        return el.type === 'field';
                    }
                },{
                    template: 'fields/slider.html',
                    test: function (el) {
                        return  el.styles.indexOf('Slider') > -1 && el.type === 'field';
                    }
                },{
                    template: 'elements/button.html',
                    test: function (el) {
                        return el.type === 'button';
                    }
                },

                ///////////////////////////////////
                // Content items
                ///////////////////////////////////
                {
                    template: 'elements/asset.html',
                    test: function (el) {
                        return el.type === 'asset';
                    }
                },{
                    template: 'elements/contentitem.html',
                    test: function (el) {
                        return el.type === 'contentitem';
                    }
                },{
                    template: 'elements/textitem.html',
                    test: function (el) {
                        return el.type === 'textitem';
                    }
                },{
                    template: 'contentitems/table.html',
                    test: function (el) {
                        return el.type === 'contentitem' && el.contentStyle === 'Table';
                    }
                },{
                    template: 'contentitems/table-row.html',
                    test: function (el) {
                        return el.type === 'contentitem' && el.contentStyle === 'TableRow';
                    }
                },{
                    template: 'contentitems/table-cell.html',
                    test: function (el) {
                        return el.type === 'contentitem' && el.contentStyle === 'TableCell';
                    }
                },{
                    template: 'contentitems/table-header.html',
                    test: function (el) {
                        return el.type === 'contentitem' && el.contentStyle === 'TableHeader';
                    }
                },{
                    template: 'contentitems/link.html',
                    test: function (el) {
                        return el.type === 'contentitem' && el.contentStyle === 'Link' && el.children.length;
                    }
                },{
                    template: 'contentitems/list.html',
                    test: function (el) {
                        return el.type === 'contentitem' && el.contentStyle === 'List' && el.children.length;
                    }
                },{
                    template: 'contentitems/heading.html',
                    test: function (el) {
                        return el.type === 'contentitem' && (el.contentStyle === 'Heading1' ||
                            el.contentStyle === 'Heading2' || el.contentStyle === 'Heading3' ||
                            el.contentStyle === 'Heading4' || el.contentStyle === 'Heading5' ||
                            el.contentStyle === 'Heading6');
                    }
                },
                ///////////////////////////////////
                // Special Containers
                ///////////////////////////////////
                {
                    template: 'containers/lightbox.html',
                    test: function (el) {
                        return el.type === 'container' && el.contentStyle === 'BB_Lightbox';
                    }
                },{
                    template: 'containers/json.html',
                    test: function (el) {
                        return el.contentStyle === 'BB_JSON_Object';
                    }
                },{
                    template: 'containers/file-upload-container.html',
                    test: function (el) {
                        return el.type === 'container' && el.name === 'Passport_Copy_Upload';
                    }
                },
                ///////////////////////////////////
                // Other
                ///////////////////////////////////
                {
                    template: 'elements/image.html',
                    test: function (el) {
                        return el.type === 'image';
                    }
                },{
                    template: 'contentitems/base.html',
                    test: function () {
                        return false;
                    }
                },{
                    template: 'containers/refresh-container.html',
                    test: function(el) {
                        return el.type === 'container' && el.contentStyle === 'BB_RefreshContainer';
                    }
                }
            ],
            controls: [

                ///////////////////////////////////
                // Controls
                ///////////////////////////////////
                //single values
                {
                    template: 'controls/input-text.html',
                    test: function (el) {
                        return el.dataType === 'text' && !el.hasDomain;
                    }
                },{
                    template: 'controls/input-password.html',
                    test: function (el) {
                        return el.dataType === 'text' && !el.hasDomain && el.styles.indexOf('password') > -1;
                    }
                },{
                    template: 'controls/textarea.html',
                    test: function (el) {
                        return el.dataType === 'text' && !el.hasDomain && el.styles.indexOf('memo') > -1;
                    }
                },{
                    template: 'controls/input-currency.html',
                    test: function (el) {
                        return el.dataType === 'currency';
                    }
                },{
                    template: 'controls/input-currency-increment.html',
                    test: function (el) {
                        return el.dataType === 'currency' && el.styles.indexOf('increment') > -1;
                    }
                },{
                    template: 'controls/input-date.html',
                    test: function (el) {
                        return formUtil.isMobile() && el.dataType === 'date';
                    }
                },{
                    template: 'controls/input-datetime.html',
                    test: function (el) {
                        return formUtil.isMobile() && el.dataType === 'datetime';
                    }
                },{
                    template: 'controls/input-date-desktop.html',
                    test: function (el) {
                        return !formUtil.isMobile() && el.dataType === 'date';
                    }
                },{
                    template: 'controls/input-datetime-desktop.html',
                    test: function (el) {
                        return !formUtil.isMobile() && el.dataType === 'datetime';
                    }
                },{
                    template: 'controls/input-integer.html',
                    test: function (el) {
                        return el.dataType === 'integer' || el.dataType === 'number';
                    }
                },{
                    template: 'controls/input-percentage.html',
                    test: function (el) {
                        return el.dataType === 'percentage';
                    }
                },{
                    template: 'controls/input-checkbox.html',
                    test: function (el) {
                        return  el.dataType === 'boolean' && !el.multiValued && !el.hasDomain;
                    }
                },
                //multi values
                {
                    template: 'controls/dropdown-select.html',
                    test: function (el) {
                        return el.hasDomain && !el.multiValued;
                    }
                },{
                    template: 'controls/radiogroup.html',
                    test: function (el) {
                        return el.styles.indexOf('Radio') > -1 && el.hasDomain && !el.multiValued;
                    }
                },{
                    template: 'controls/radiogroup-toggle.html',
                    test: function (el) {
                        return el.styles.indexOf('ToggleButtons') > -1 && el.hasDomain && !el.multiValued;
                    }
                },{
                    template: 'controls/gender-toggle.html',
                    test: function (el) {
                        return el.styles.indexOf('GenderButtons') > -1 && el.hasDomain && !el.multiValued;
                    }
                },{
                    template: 'controls/input-toggle.html',
                    test: function (el) {
                        return  el.styles.indexOf('toggle') > -1 && el.dataType === 'boolean' && !el.multiValued;
                    }
                },{
                    template: 'controls/checkboxgroup.html',
                    test: function (el) {
                        return el.hasDomain && el.multiValued;
                    }
                },{
                    template: 'controls/checkboxgroup-toggle.html',
                    test: function (el) {
                        return el.styles.indexOf('ToggleButtons') > -1 && el.hasDomain && el.multiValued;
                    }
                },{
                    template: 'controls/autocomplete.html',
                    test: function (el) {
                        return el.styles.indexOf('Autocomplete') > -1 && el.hasDomain;
                    }
                },{
                    template: 'controls/static-control.html',
                    test: function (el) {
                        return el.styles.indexOf('StaticControl') > -1;
                    }
                },{
                    template: 'controls/dropdown-select.html',
                    test: function (el) {
                        return el.dataType === 'entity';
                    }
                }
            ]
        };
    });
})();