(function() {
	'use strict';

	var app = angular.module('forms-ui');

	/**
	 * Delta service
	 */
	app.factory('deltaService', /*@ngInject*/ function(formUtil) {

		//delta action constants
		var actions = {
			ADD: 'add',
			DELETE: 'delete',
			UPDATE: 'update'
		};

		/**
		 * Makes delta to send to the server
		 * @param originalElements
		 * @param newElements
		 * @returns {Array}
		 */
		function makeRequestDeltas(originalElements, newElements) {
			var deltas = [];
			newElements.forEach(function(newElement) {
				var key = newElement.key;
				var originalElement = formUtil.lookupElement(key, originalElements);

				if(originalElement && !angular.equals(originalElement, newElement)) {
					var values = angular.isArray(newElement.values) ? newElement.values : [ newElement.values ];
					values.dataType = newElement.dataType;

					deltas.push({
						key: key,
						values: values
					});
				}
			});
			return deltas;
		}

		//deletes an element from a list of elements given a key
		function deleteElement(elements, key) {
			for(var i = 0; i < elements.length; i++) {
				if(elements[i].key === key) {
					elements.splice(i, 1);
					break;
				}
			}
		}

		//adds an element to a list of elements
		function addElement(elements, newElement) {
			elements.push(newElement);
		}

		//updates an element in a list of elements given a key
		function updateElement(elements, key, newElement) {
			var i, prop, currElement;
			for(i = 0; i < elements.length; i++) {
				currElement = elements[i];
				if(currElement.key === key) {
					for(prop in newElement) {
						if(newElement.hasOwnProperty(prop) && newElement[prop] !== currElement[prop]) {
							currElement[prop] = newElement[prop];
						}
					}
				}
			}
		}

		/**
		 * Applies deltas received from a server response to a local model
		 * @param elements
		 * @param deltas
		 * @returns {*}
		 */
		function applyResponseDeltas(elements, deltas) {
			deltas.forEach(function(delta) {

				switch(delta.type) {

					case actions.DELETE:
						deleteElement(elements, delta.key);
						break;

					case actions.ADD:
						addElement(elements, delta.model);
						break;

					case actions.UPDATE:
						updateElement(elements, delta.key, delta.model);
						break;
				}
			});
			return elements;
		}

		// Exports
		return {
			makeRequestDeltas: makeRequestDeltas,
			applyResponseDeltas: applyResponseDeltas
		};
	});
})();