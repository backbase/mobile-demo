/*jslint browser: true*/
(function() {
  'use strict';
  var app = angular.module('forms-ui');

  app.directive('dateInput', /*@ngInject*/ function() {
    return {
      restrict: 'A',
      require: ['ngModel'],
      scope:{
        element: '=',
        format: '='
      },
      link: function(scope, element, attrs, required) {

        /*
        Want more options? Check the documentation http://eonasdan.github.io/bootstrap-datetimepicker/
        */

        var parseDateParams = function(params){
          var date = window.moment();
          if(params.compareday){
            date.add(parseFloat(params.compareday), 'days');
          }
          if(params.comparemonth){
            date.add(parseFloat(params.comparemonth), 'months');
          }
          if(params.compareyear){
            date.add(parseFloat(params.compareyear), 'years');
          }

          return date;
        };

        var format = scope.format,
            showTodayButton = format === 'DD/MM/YYYY' ? true : false,
            ngModel = required[0],
            defaultDate = scope.element.values[0] ? scope.element.values[0] : false;

        var options = {
          format: format,
          useStrict: true,
          keepInvalid: true,
          showTodayButton: showTodayButton,
          defaultDate: defaultDate,
          allowInputToggle: true
        };

        var greater = scope.element.validations.filter(function(v){
          return v.type === 'Date' && v.parameters.comparator === 'greater';
        });

        var less = scope.element.validations.filter(function(v){
          return v.type === 'Date' && v.parameters.comparator === 'less';
        });

        if(less.length){
          options.maxDate = parseDateParams(less[0].parameters);
          if(options.defaultDate > options.maxDate){
            delete options.defaultDate;
          }
        }

        if(greater.length){
          options.minDate = parseDateParams(greater[0].parameters);
          if(options.defaultDate < options.minDate){
            delete options.defaultDate;
          }
        }

        if(scope.element.styles.indexOf('view_mode_years')>-1){
          options.viewMode = 'years';
          options.showTodayButton = false;
        }

        scope.$watch(function() {
          return ngModel.$viewValue;
        }, function(ov,nv) {
          if(ov!==nv){
            element.change();
          }
        });

        element.find('input').on('change',function(){
          var date = window.moment(element.find('input').val(),scope.format,true);
          if(date.isValid()){
            ngModel.$setViewValue(date.toDate());
          } else {
            ngModel.$setViewValue(NaN);
          }
          scope.$apply();
        });

        element
          .datetimepicker(options)
          .on('dp.change', function (e) {
            if (e.date !== ngModel.$viewValue){
              ngModel.$setViewValue(e.date);
              scope.$apply();
            }
          })
          .on('dp.hide', function () {
            //Reset viewmode to years if needed
            if(scope.element.styles.indexOf('view_mode_years')>-1) {
              element.data('DateTimePicker').viewMode('years');
            }
          });
      }
    };
  });
})();