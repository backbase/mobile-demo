(function () {
    'use strict';

    var app = angular.module('forms-ui');

    /**
     * Delta service
     */
    app.factory('transformerService', /*@ngInject*/ function (transformers) {
        var self = this;
        this.config = {};
        var transform = function (model, transformerList) {
            return  transformerList.reduce(function (prev, current) {
                return current(prev, self.config);
            }, model);
        };
        return {
            init: function (config) {
                self.config = config;
            },
            getConfig: function () {
                return self.config;
            },
            setConfig: function (config) {
                angular.extend(self.config, config);
            },
            marshal: function (model) {
                return transform(model, transformers.marshalers)
            },
            unmarshal: function (model) {
                return transform(model, transformers.unmarshalers)
            }
        };
    });
})();