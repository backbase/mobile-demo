(function () {
  'use strict';
  var app = angular.module('forms-ui'),
    BB_LIST = 'contentItemList';

  app.directive(BB_LIST, /*@ngInject*/ function ($log, debugEnabled, formUtil) {
    return {
      restrict: 'E',
      require: '^bbForm',
      template: '<ul class="{{scope.element.styles | beautify}}" ng-class="{\'fa-ul\':hasStyle(element,\'list_check\')}">' +
                  '<li ng-repeat="child in children track by $index">' +
                  '<i ng-if="hasStyle(element,\'list_check\')" class="fa-li fa fa-check"></i>' +
                  '{{child}}</li>' +
                '</ul>',
      scope: {
        element: '='
      },
      link: function (scope, element, attrs, formsController) {

        //logging
        if(debugEnabled && scope.element) {
          $log.debug('%s: linking ul: %s', BB_LIST, formUtil.elementToString(scope.element));
        }

        var children = [], listItem, listItemChild, classNames = '', listCheckType = false;

        angular.forEach(scope.element.children, function(value) {

          listItem = formsController.lookupElement(value);

          if (listItem.type === 'contentitem' && listItem.contentStyle === 'ListElement') {
            listItemChild = formsController.lookupElement(listItem.children[0]);
            children.push(listItemChild.plainText);
          }
        });

        scope.children = children;
        scope.classNames = classNames;
        scope.listCheckType = listCheckType;

        scope.hasStyle = formsController.hasStyle;
      }
    };
  });
})();