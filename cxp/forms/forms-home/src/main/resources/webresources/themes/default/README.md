Forms Angular UI
===========================

An Angular client for Backbase Forms

##Usage

###Quick start

A simple usage of the Forms Angular UI directive looks like this:

```html
<!DOCTYPE html>
<html ng-app="my-form">
<head>
	<title>Forms Angular UI Quick Setup</title>
	<link rel="stylesheet" type="text/css" href="/bower_components/bootstrap/dist/css/bootstrap.css">

	<!-- Dependencies -->
	<script src="/bower_components/angular/angular.js"></script>
	<script src="/bower_components/angular-sanitize/angular-sanitize.js"></script>

	<!-- Forms Angular UI Lib -->
	<script src="/dist/forms-lite.min.js"></script>

	<script>
		var formsUi = angular.module('forms-ui');
		formsUi.value('templateBase', '/dist/templates');
		angular.module('my-form', [ 'forms-ui' ]);
	</script>
</head>
<body>
	<bb-form
		runtime-url="'/Runtime'"
		project="'export-Kinderbijslag'"
		flow="'Start'"
		version="'0.0-Wetwijziging'"
		language-code="'nl-NL'"></bb-form>
</body>
</html>
```

Forms Angular UI will connect to the Forms Runtime url, create a session, retrieve a model and render it.

The directive's attributes correspond to the query string parameters required to launch a form by the Forms Runtime
(using Angular's camel case conversion rules).

Notice that you must set the value of your `templateBase` this is where your html templates reside. Get started by
copying the templates from the `./dist` directory. To customize templates, read extending UI rules below.

###Debugging

Switching on debug mode will enable more detailed logging to the browser console:

```javascript
var formsUi = angular.module('forms-ui');
...
formsUi.config(function($provide, $logProvider) {
    var debug = true;
    $logProvider.debugEnabled(debug);
    $provide.value('debugEnabled', debug);
});
```

###Advanced usage

####Extending UI rules

Forms Angular UI uses a list of UI rules to map form element models to rendering templates. Forms Angular UI ships with a default
set of UI rules and templates based on Twitter Bootstrap styling. You can extend/customize these rules by decorating
the `uiRules` configuration:

```javascript
var formsUi = angular.module('forms-ui');
formsUi.value('templateBase', '/my-templates');

var myElementRules = [{
    template: 'my-elements/field.html',
    test: function(el) {
        return el.type = 'field';
    }
}];

var myControlRules = [{
    template: 'my-controls/slider.html',
    test: function(el) {
        return el.styles && el.styles.indexOf('slider') > -1;
    }
}];

formsUi.config(function($provide, $logProvider) {
    $provide.decorator('uiRules', function($delegate) {
        var uiRules = $delegate;
        uiRules.elements.concat(myElementRules);
        uiRules.controls.concat(myControlRules);
        return uiRules;
    });
});
```

In the example above the element rules and control rules are both extended. The element rule for *field* will take
precedence over the default rule and a new rule for a slider control is also added.

####Creating a custom element template

Element templates will have an `element` model on their scope. You can also use the `bbControl` directive to include
form controls. These will be including by selecting a template using the ui rules mentioned above`.

```html
<div id="{{element.key}}" class="form-group {{element.styles | beautify}}">

	<!-- Field label -->
	<label for="{{element.key}}" class="col-sm-6 control-label">
		{{element.questionText}}
		<span class="opt" ng-if="!element.required">{{lookupMessage('optionalField')}}</span>
	</label>

	<!-- Field control -->
	<div class="col-sm-6" ng-class="{'has-error':(element.messages.length > 0)}">

		<bb-control element="element"></bb-control>

		<div ng-if="element.messages.length > 0" class="help-block form-error">
			{{element | constructErrorMessage:formFunctions}}
		</div>
	</div>
</div>
```

####Creating a custom control template

Like an element template, `bbControl` templates have an `element` model on their scope.

```html

<input type="text"  name="{{element.name}}" class="form-control"
	   ng-disabled="{{element.disabled}}"
	   ng-readonly="{{element.readonly}}"
	   ng-class="{'empty':(!element.values || element.values=='')}"
	   ng-blur="refresh(element)"
	   ng-model="element.values"
	   ng-required="element.required"
	   novalidate>
```

Call the `refresh()` function when an input control changes

###Extending messages

Forms Angular UI uses a small set of localized client side messages, for text which is not supplied by the server.
For example the "Select an option" text in a dropdown select. You can extend these by decoration, in a similar way
to extending the UI rules:

```javascript
formsUi.config(function($provide, $logProvider) {
    $provide.decorator('messages', function($delegate) {
        var messages = $delegate;
        messages.en.selectOne = 'Custom select message';
        return messages;
    });
});
```

###Browser support

Forms Angular UI runs in Chrome, Firefox and Internet Explorer 8 and above.

####Internet Explorer 8
The code uses several ES5 features. For the Forms Angular UI to run in Internet Explorer 8 you will need
to [shim these features](https://github.com/es-shims/es5-shim)

---

##Contributing

Please see [contributing.md](contributing.md)