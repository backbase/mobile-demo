(function() {
    'use strict';

    var app = angular.module('forms-ui');

    /**
     * Converts the name of a element to a nice id
     * Replaces SomeExample.field in some-example-field
     */
    app.filter('loanFilter', function($filter) {
        return function(value, filterType) {
            if(!value){
                return '';
            }
            switch (filterType) {
                case 'currency' : {
                    value = parseFloat(value);
                    return  "€ " + $filter('currency')( value, '' ).replace(/[,.]/g, function (x) { return x == "," ? "." : ","; });
                }
                case 'percentage' :{
                    return  value.replace('.',',') + '%';
                }
                case 'month' :{
                    return  value + (value > 1 ? ' mesi' : ' mese');
                }
                default : {
                    return value;
                }
            }
        };
    });

})();



