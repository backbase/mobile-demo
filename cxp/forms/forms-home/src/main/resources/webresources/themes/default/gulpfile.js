var gulp = require('gulp'),
	watch = require('gulp-watch')
	util = require('util'),
	rimraf = require('rimraf'),
	jshint = require('gulp-jshint'),
	bump = require('gulp-bump'),
	git = require('gulp-git'),
	insert = require('gulp-insert'),
	rename = require('gulp-rename'),
	ngAnnotate = require('gulp-ng-annotate'),
	wrap = require('gulp-wrap'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	karma = require('karma');

var releaseType = process.env.RELEASE_TYPE;

var PACKAGE_MESSAGE =
	'/**\n' +
	' * %s - Copyright © %s Backbase B.V - All Rights Reserved\n' +
	' * Version %s\n' +
	' * %s\n' +
	' * @license\n' +
	' */\n';

function getVersionNumber() {
	return require('./package.json').version;
}

//jshint
gulp.task('clean', function() {
	return rimraf.sync('./dist')
});

//jshint
gulp.task('lint', function() {

	return gulp.src('./scripts/**/*.js')
		.pipe(jshint())
		.pipe(jshint.reporter("default"))
		.pipe(jshint.reporter("fail"));
});

// Creates regular and minified distributions as UMD modules
gulp.task('package', [ 'clean' ], function () {

	var meta = require('./package.json');
	var year = new Date().getFullYear();
	var packageMessage = util.format(PACKAGE_MESSAGE, meta.description, year, meta.version, meta.homepage);

	var files = [
		'./scripts/main.js',
		'./scripts/services/form-util.js',
		'./scripts/**/!(main|form-util)*.js'
	];

	return gulp.src(files)
		.pipe(concat('module-forms-angular-ui.js'))
		.pipe(ngAnnotate())
		// .pipe(wrap({
		// 	src: './umd-wrapper.tmpl'
		// }))
		.pipe(insert.prepend(packageMessage))
		.pipe(gulp.dest('./dist'))
		.pipe(rename('module-forms-angular-ui.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('./dist'));
});

//currently, simply copies the templates to the dist directory
gulp.task('package-templates', [ 'package' ], function () {
	return gulp.src('./scripts/templates/**/*.html').pipe(gulp.dest('./dist/templates'))
});

// Run tests after packaging, so we can test the minified build
gulp.task('test', [ 'package-templates' ], function (done) {
	karma.server.start({
		configFile: __dirname + '/karma.conf.js',
		singleRun: true
	}, done);
});

//watches and runs package when src changes. Useful for running unit tests against the build
gulp.task('watch', [ 'package' ], function () {
	console.log('Watching for changes...');
	gulp.watch('./scripts/**/*.js', [ 'package' ]);
});

gulp.task('default', [ 'package-templates' ]);

///////////////////////////////////////////////////////////////////////////////
// Release tasks:
// 1. Tag and push
// 2. Checkout/pull master
// 3. bump version
// 4. add and commit
// 5. push to origin
///////////////////////////////////////////////////////////////////////////////

//add commit messages to the Changelog
gulp.task('tag-release', function(done) {

	if(!releaseType) {
		throw new Error('No release type defined. You must run this task from Jenkins');
	}
	var v = getVersionNumber();
	git.tag(v, 'Tagging release: ' + v, function (err) {
		if(err) {
			console.log(err);
		} else {
			var v = getVersionNumber();
			git.push('origin', v, function (err) {
				if(err) {
					console.log(err);
				} else {
					done();
				}
			});
		}
	});
});

gulp.task('checkout-master', ['tag-release'], function(done) {

	git.checkout('master', function(err) {
		if(err) {
			console.log(err);
		} else {
			git.pull('origin', 'master', function (err) {
				if (err) {
					console.log(err);
				} else {
					done();
				}
			});
		}
	});
});

gulp.task('bump-next-version', [ 'checkout-master' ], function(){
	return gulp.src('./package.json')
		.pipe(bump({type: releaseType}))
		.pipe(gulp.dest('./'))
		.pipe(git.add())
		.pipe(git.commit('Updating development version'));
});

gulp.task('push-next-version', [ 'bump-next-version' ], function(done) {
	git.push('origin', 'master', function (err) {
		if(err) {
			console.log(err);
		} else {
			done();
		}
	});
});

gulp.task('release', ['push-next-version'], function(){
	console.log('PERFORMING RELEASE')
});