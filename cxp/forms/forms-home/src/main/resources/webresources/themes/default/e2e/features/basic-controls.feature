Feature: Forms UI Basic Controls
  As a forms modeller
  Elements modelled in Forms Studio should be rendered accordingly by the runtime

  Scenario: Input text controls
    Given a String attribute is modelled in Forms Studio and is then rendered

    When it is named "InputTests.InputStringBlank"
    Then an input tag should be rendered with the value ""

    When it is named "InputTests.InputStringDefault"
    Then an input tag should be rendered with the value "Default"

    When it is named "InputTests.InputStringMaxLength"
      And the display length is set to 5
    Then an input tag should be rendered with the value ""
      And it accepts input up to 5 characters

  Scenario: Boolean controls
    Given a Boolean attribute is modelled in Forms Studio and is then rendered

    When it is named "InputTests.InputBoolean"
    Then a checkbox should be rendered and unchecked

    When it is named "InputTests.InputBooleanOn"
    Then a checkbox should be rendered and checked

  Scenario: Short Options List
    Given a String attribute is modelled in Forms Studio and is then rendered

    When it is named "InputTests.OptionsStringShort"
    Then a select box should be rendered with the options Red, Blue and Yellow

    When it is named "InputTests.OptionsStringShortDefault"
    Then a select box should be rendered with the option Blue selected by default

  Scenario: Long Options List
    Given a String attribute is modelled in Forms Studio and is then rendered

    When it is named "InputTests.OptionsStringLong"
    Then a select box should be rendered with the options BSP, Gazelle, Sparta, Cortina, Batavus and Union

    When it is named "InputTests.OptionsStringLongDefault"
    Then a select box should be rendered with the option Cortina selected by default

  Scenario: Short Multi-value List
    Given a String attribute is modelled in Forms Studio and is then rendered

    When it is named "InputTests.MultivalueShort"
    Then 3 checkboxes should be rendered
      And have the labels: Red, Blue and Yellow
      And has selected values for the labels: [none]

    When it is named "InputTests.MultivalueShortDefault"
    Then 3 checkboxes should be rendered
      And has selected values for the labels: Red and Yellow

  Scenario: Long Multi-value List
    Given a String attribute is modelled in Forms Studio and is then rendered

    When it is named "InputTests.MultivalueLong"
    Then 6 checkboxes should be rendered
      And have the labels: BSP, Gazelle, Sparta, Cortina, Batavus and Union
      And has selected values for the labels: [none]

    When it is named "InputTests.MultivalueLongDefault"
    Then 6 checkboxes should be rendered
      And has selected values for the labels: Sparta and Batavus

  Scenario: Textarea
    Given a String attribute is modelled in Forms Studio and is then rendered
      And it has the presentation style "memo"

    When it is named "InputTests.InputStringBlank"
    Then a textarea tag should be rendered with the value ""

    When it is named "InputTests.InputStringDefault"
    Then a textarea tag should be rendered with the value "Default"

    When it is named "InputTests.InputStringMaxLength"
      And the display length is set to 5
    Then a textarea tag should be rendered with the value ""
      And it accepts input up to 5 characters