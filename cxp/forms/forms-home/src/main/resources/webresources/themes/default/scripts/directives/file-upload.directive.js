(function () {
    'use strict';

//Implemented based on:
//https://github.com/danialfarid/ng-file-upload

    var app = angular.module('forms-ui');

    app.directive('fileUpload', /*@ngInject*/ function ($log, templateBase, $timeout, sessionService, formFactory, Upload) {
        return {
            restrict: 'A',
            templateUrl: templateBase + '/controls/file-upload.html',
            require: '^bbForm',
            scope: {
                element: '='
            },
            link: function ($scope, $element, $attrs, formsController) {

                function makeUrl(runtimeUrl, sessionId, subscription, fileuploadID) {
//example of url, got from modeller
//http://localhost:8086/forms-runtime/server/session/465cd214-ff7e-4539-92c7-5b6648ae71f1/subscription...
                    return runtimeUrl + '/server/session/' + sessionId + '/subscription/' + subscription + '/fileupload/' + fileuploadID + '/';
                }

                function createTypeFilePattern(str) {
                    var pattern, arr, i, l;
                    arr = str.split('|') || [];
                    for (i = 0, l = arr.length; i < l; i++) {
                        arr[i] = '.' + arr[i];
                    }

                    pattern = arr.join(',');
                    return pattern;
                }

                var url, allowedExtensions, maxFileSize;

                allowedExtensions = createTypeFilePattern($scope.element.properties.allowedextensions);

                maxFileSize = $scope.element.properties.maxfilesize;
                maxFileSize = maxFileSize.toString();

                url = makeUrl(formFactory.runtimeUrl, sessionService.getSessionId(), sessionService.getSessionId(), $scope.element.properties.configurationid);

                $scope.maxFileSize = maxFileSize;
                $scope.allowedExtensions = allowedExtensions;
                $scope.uploaded = [];

                // upload on file select or drop
                $scope.upload = function (file) {
                    if (!file)
                        return;

                    Upload.upload({
                        url: url,
                        file: file
                    }).progress(function (evt) {
                        $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
                        $scope.progressStyle.width = $scope.progress + '%';
                        $log.info('progress: ' + $scope.progress + '% ');
                    }).success(function (data) {
                        formsController.refresh($scope.element, data);
                    }).error(function (data) {
                        formsController.refresh($scope.element, data);
                    }).finally(function () {
                        $timeout(function () {
                            $scope.progress = 0;
                            $scope.progressStyle.width = 0;
                        }, 300);
                    });
                };

                $scope.uploadError = function () {
                    if ($scope.element.children.length) {
                        return $scope.element.children.filter(function (e) {
                              var el = formsController.lookupElement(e);
                              return el.name == 'errorMessages';
                          }).length > 0;
                    }

                    return false;
                };

                $scope.filterChildren = function () {
                    return $scope.element.children.filter(function (e) {
                        var el = formsController.lookupElement(e);
                        return el.name != 'FileUploaded';
                    });
                };

                $scope.progress = 0;

                $scope.progressStyle = {
                    width: 0
                };
            }
        };
    });
})();