(function() {
    'use strict';

    var app = angular.module('forms-ui');

    app.directive('redirectToPortalPage', /*@ngInject*/ function($window) {
        return {
            restrict: 'CAE',
            link: function (scope, element, attr) {
                if(b$){
                    $window.onbeforeunload = undefined;
                    var redirectURL = b$.portal.config.serverRoot + '/' + b$.portal.portalName + scope.element.plainText;
                    redirectURL = decodeURIComponent(redirectURL.replace(/&#x([A-F0-9]{2});/gi,"%$1"));
                    $window.location.href = redirectURL;
                }
            }
        };
    });
})();


