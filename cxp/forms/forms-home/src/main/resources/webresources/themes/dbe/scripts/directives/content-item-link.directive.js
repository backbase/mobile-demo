(function() {
    'use strict';
    var app = angular.module('forms-ui'),
        BB_LINK = 'contentItemLink';

    // remove all directive
    app.config(function($provide) {
        $provide.decorator(BB_LINK + 'Directive', function($delegate) {
            $delegate.shift();
            return $delegate;
        });
    });

    app.directive(BB_LINK, /*@ngInject*/ function ($log, debugEnabled, $window, formUtil, sessionService) {
        return {
            restrict: 'E',
            require: '^bbForm',
            template: '<a class="{{linkClassNames | beautify}}" target="{{hrefTuple[2] || target}}" ng-href="{{hrefTuple[1]}}" ng-class="{\'disabled\' : disabled}"><i ng-if="iconClassName" class="{{iconClassName}}"></i> {{hrefTuple[0]}}</a>',
            scope: {
                element: '='
            },
            link: function (scope, element, attrs, formsController) {
                //logging
                if(debugEnabled && scope.element) {
                    $log.debug('%s: linking html anchor: %s', BB_LINK, formUtil.elementToString(scope.element));
                }

                var node = {},
                    iconNode = {},
                    hrefNode = {},
                    iconClassName = '',
                    linkClassNames = '',
                    hrefTuple = [],
                    iconNamespaces = {},
                    target = '_self';

                iconNamespaces = {
                    'IconFa': 'fa',
                    'IconGlyphicon': 'glyphicon'
                };

                function parseIconClassName(str) {
                    return str.replace(new RegExp('_','g'),'-');
                }

                function parseHref(str){
                    var arr = str.split('|');
                    return arr.slice(1);
                }

                if(scope.element.children){

                    angular.forEach(scope.element.children, function(value) {

                        node = formsController.lookupElement(value);

                        //create className for icon
                        if (node.type === 'contentitem' && iconNamespaces[node.contentStyle] && node.children.length) {
                            iconNode = formsController.lookupElement(node.children[0]);
                            scope.linkClassNames = iconNamespaces[node.contentStyle] + ' ' + parseIconClassName(iconNode.styles[0]);
                        }

                        //create href and text for link
                        if (node.type === 'contentitem' && node.contentStyle === 'LinkURL' && node.children.length) {
                            hrefNode = formsController.lookupElement(node.children[0]);
                            scope.hrefTuple = parseHref(hrefNode.plainText);
                            if (hrefNode.styles.length) {
                                scope.linkClassNames = hrefNode.styles;
                            }
                        }
                    });

                } else {
                    node = formsController.lookupElement(scope.element.key);

                    scope.$watch(function(){
                        return node.plainText
                    }, function(){
                        //create href and text for a portal page
                        if (node.type === 'textitem' && node.styles.indexOf('LinkToPortalPage') > -1) {
                            scope.hrefTuple = parseHref(node.plainText);
                            if (node.styles.length) {
                                scope.linkClassNames = node.styles;
                            }
                            if( typeof b$ == "undefined" ){
                                scope.disabled = true;
                            } else {
                                scope.hrefTuple[1] = b$.portal.config.serverRoot + '/' + b$.portal.portalName + scope.hrefTuple[1];
                            }
                        }

                        //create href and text for a portal page
                        if (node.type === 'textitem' && node.styles.indexOf('LinkToPortalFormServer') > -1) {
                            scope.hrefTuple = parseHref(node.plainText);
                            if (node.styles.length) {
                                scope.linkClassNames = node.styles;
                            }
                            if( typeof b$ == "undefined" ){
                                scope.disabled = true;
                            } else {
                                scope.hrefTuple[1] = scope.hrefTuple[1].replace('{{sessionId}}', sessionService.getSessionId());
                                scope.hrefTuple[1] = b$.portal.config.serverRoot + '/services/forms/server/session' + scope.hrefTuple[1];
                            }
                        }
                    });
                }

            }
        };
    });


})();