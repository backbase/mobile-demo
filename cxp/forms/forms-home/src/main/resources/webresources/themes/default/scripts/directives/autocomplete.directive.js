(function () {
    'use strict';
    var app = angular.module('forms-ui');

    app.directive('autocomplete', /*@ngInject*/ function ($log, debugEnabled, $filter) {
        return {
            restrict: 'A',
            require: '^bbForm',
            scope: {
                element: '='
            },
            link: function (scope, element, attrs, formsController) {

                var initialValue = $filter('filter')(scope.element.domain, function(domain){
                    return domain.value == scope.element.values[0];
                });

                function findByQuery(query, syncResults) {
                    var result = $filter('filter')(scope.element.domain, {displayValue: query});
                    syncResults(result);
                }

                element
                    .typeahead({
                        minLength: scope.element.displayLength,
                        highlight: true
                    },{
                        source: findByQuery,
                        limit: 10,
                        display: function (item) {
                            return item.displayValue;
                        },
                        templates: {
                            notFound: [
                                '<div class="tt-suggestion">',
                                'No results found',
                                '</div>'
                            ].join('\n')
                        }
                    });

                if (initialValue.length) {
                    element.typeahead('val', initialValue[0].displayValue);
                }


                var handleChange = function (ev, suggestion) {
                    scope.element.values[0] = suggestion.value;
                    formsController.refresh(scope.element);
                };

                element.bind('typeahead:select', handleChange)
                    .bind('typeahead:autocomplete', handleChange)
                    .bind('typeahead:close', function () {
                        var matching = $filter('filter')(scope.element.domain, function (value) {
                            return value.displayValue.toLowerCase() === element.typeahead('val').toLowerCase();
                        });
                        //No domain element with matching display value
                        if (element.typeahead('val') === '' || !matching.length) {
                            var currDomainEl = $filter('filter')(scope.element.domain, {value: scope.element.values[0]});
                            var currValue = currDomainEl.length ? currDomainEl[0].displayValue : '';
                            element.typeahead('val', currValue);
                        } else {
                            element.typeahead('val', matching[0].displayValue);
                            handleChange(null, matching[0]);
                        }
                    });
            }
        };
    });
})();