(function () {
    'use strict';
    var app = angular.module('forms-ui'),
        BB_TOGGLECHECKBOX = 'toggleCheckbox';

    app.directive(BB_TOGGLECHECKBOX, /*@ngInject*/ function ($log, $parse, debugEnabled) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                //logging
                if (debugEnabled && scope.element) {
                    $log.debug('%s: linking checkbox toggle', BB_TOGGLECHECKBOX);
                }

                scope.$watch(function () {
                    return ngModel.$viewValue;
                }, function () {
                    element.prop('checked', ngModel.$modelValue);
                });

                element.change(function () {
                    ngModel.$setViewValue(element.prop('checked'));
                });

                scope.lock = function (e) {
                    e.stopImmediatePropagation();
                    e.preventDefault();
                };

                scope.$watch(function () {
                    return $parse(attrs.ngDisabled)(scope);
                }, function (disabled) {
                    var functionName = disabled ? 'bind' : 'unbind';
                    element.parent('label')[functionName]('click tap', scope.lock);
                });
            }
        };
    });
})();