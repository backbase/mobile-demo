(function () {
    'use strict';

    var SESSION_SERVICE = 'sessionService';

    var app = angular.module('forms-ui');

    /**
     * Service for starting a session
     */
    app.factory(SESSION_SERVICE, /*@ngInject*/ function ($http, $q, $log, $interval, formConsts, debugEnabled) {

        function SessionService() {
            this.sessionId = null;
        }

        SessionService.prototype.getSessionId = function () {
            return this.sessionId;
        };

        /**
         * Starts a session, excluding the subscription step
         * @param runtimeUrl
         * @param sessionOpts
         * @returns {Promise|*}
         */
        SessionService.prototype.startSession = function (runtimeUrl, sessionOpts, formsParameters) {
            sessionOpts = sessionOpts || {};

            var invalidOpts = (!sessionOpts.project || !sessionOpts.flow || !sessionOpts.version || !sessionOpts.languageCode);
            if (invalidOpts) {
                var err = new Error('Invalid form options. Please ensure you have supplied a project, flow, version and language');
                return $q.reject(err);
            }
            var sessionPromise;

            sessionOpts.ui = sessionOpts.ui || 'mvc';
            sessionOpts.theme = sessionOpts.theme || 'forms';
            sessionOpts.create = true;

            var queryParameters = angular.extend({}, sessionOpts, formsParameters);

            var queryString = Object.keys(queryParameters).map(function (key) {
                return key + '=' + encodeURIComponent(queryParameters[key]);
            }).join('&');

            var url = runtimeUrl + '/server/start?' + queryString;
            $log.debug('%s: Sending GET to %s', SESSION_SERVICE, url);

            sessionPromise = $http.get(url).then(function (res) {
                return res.data;
            });

            return sessionPromise;
        };

        /**
         * Subcription step for new session
         * @param runtimeUrl
         * @param sessionOpts
         * @returns {Promise|*}
         */
        SessionService.prototype.subscribeSession = function (runtimeUrl, sessionOpts) {
            var self = this;
            self.sessionId = sessionOpts.sessionId;

            var subscribeUrl = runtimeUrl + '/server/session/' + self.sessionId + '/api/subscribe';
            $log.info('%s: Subscribing to session: %s.', SESSION_SERVICE, self.sessionId);
            $log.debug('%s: Sending POST to %s', SESSION_SERVICE, subscribeUrl);

            return $http.post(subscribeUrl, {}).then(function () {
                //ping session to keep it alive
                self.keepAlive(runtimeUrl);
                return self.sessionId;
            });
        };

        /**
         * Pings the session at intervals until the keepAliveOn flag is turned off.
         * @param runtimeUrl
         */
        SessionService.prototype.keepAlive = function (runtimeUrl) {
            var self = this;
            this.pingInterval = $interval(function () {
                if (!debugEnabled) {
                    $interval.cancel(self.pingInterval);
                } else {
                    var url = runtimeUrl + '/server/session/' + self.sessionId + '/api/utility/keepAlive';
                    $log.debug('Pinging session...');
                    $http.get(url).then(function () {
                        $log.debug('Pinging session... OK');
                    }).then(null, function (err) {
                        $log.debug('There was an error pinging the session. Won\'t try again');
                        $log.error(err.message);
                    });
                }
            }, formConsts.PING_INTERVAL);

        };

        return new SessionService();
    });
})();