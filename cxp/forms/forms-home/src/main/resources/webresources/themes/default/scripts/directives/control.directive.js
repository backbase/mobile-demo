(function() {
	'use strict';

	var BB_CONTROL = 'bbControl';

	var app = angular.module('forms-ui');

	/**
	 * Element directive (bbControl)
	 * This directive is designed to render single controls. It expects and element object as an attribute
	 */
	app.directive(BB_CONTROL,
	/*@ngInject*/ function($compile, $http, $templateCache, $log, formUtil, uiRules, templateBase, debugEnabled) {
		return {
			restrict: 'EA',
			require: '^bbForm',
			scope: {
				element: '='
			},
			link: function(scope, element, attrs, formsController) {
                scope.refresh = formsController.refresh;
				scope.hasStyle = formsController.hasStyle;
                scope.lookupElement = formsController.lookupElement;
                scope.lookupMessage = formsController.lookupMessage;
                scope.hasStyleContaining = formsController.hasStyleContaining;
                scope.stylesContaining = formsController.stylesContaining;

				scope.$watchCollection('[element,element.type,element.domain,element.styles,element.contentStyle]', function() {
					if(scope.element) {
						templateBase = /\/$/.test(templateBase) ? templateBase : templateBase + '/';
						var templateUrl =
							templateBase + formUtil.findTemplate(scope.element, uiRules.controls);
						//logging
						if(debugEnabled && scope.element) {
							$log.debug('%s: linking control: %s', BB_CONTROL, formUtil.elementToString(scope.element));
							$log.debug('%s: ...using template: %s', BB_CONTROL, templateUrl);
						}

						$http.get(templateUrl, { cache: $templateCache }).then(function(template) {
							element.html(template.data);
							$compile(element.contents())(scope);
						}).then(null, function() { //catch (ie8 safe)
							element.html('');
						});
					}
				});

			}
		};
	});
})();