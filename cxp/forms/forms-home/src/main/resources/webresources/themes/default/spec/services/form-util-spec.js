describe('Form util', function () {

	// Load your module.
	beforeEach(module('forms-ui'));

	var formUtil, formConsts;

	beforeEach(inject(function($injector) {
		formUtil = $injector.get('formUtil');
		formConsts = $injector.get('formConsts');
	}));

	it('clones an object', function() {

		var obj = {
			two: 2,
			three: {
				four: 5
			},
			five: [ 6, 7, 8]
		};

		var clonedObj = angular.copy(obj);

		expect(clonedObj).not.toBe(obj);
		expect(clonedObj).toEqual(obj);
	});

	it('clones an object', function() {

		var obj = {
			two: 2,
			three: {
				four: 5
			},
			five: [ 6, 7, 8]
		};

		var clonedObj = angular.copy(obj);
		expect(clonedObj).not.toBe(obj);
		expect(clonedObj).toEqual(obj);
	});

	it('finds an element which matches a given key', function() {

		var elements = [
			{
				key: 'F1',
				value: 1
			}, {
				key: 'F2',
				value: 2
			}, {
				key: 'F3',
				value: 3
			}, {
				key: 'F4',
				value: 4
			}
		];

		var found = formUtil.lookupElement('F3', elements);
		expect(found).toBe(elements[2]);
		expect(found.value).toBe(3);
	});

	it('find a best match locale from a list of available locales', function() {
		var available = [ 'en', 'en-GB', 'nl', 'fr', 'es-ES', 'en-GB-northern', 'nl-NL' ];

		expect(formUtil.getMatchingLocale('en', available)).toBe('en');
		expect(formUtil.getMatchingLocale('en-GB-southern', available)).toBe('en-GB');
		expect(formUtil.getMatchingLocale('en-GB-northern', available)).toBe('en-GB-northern');
		expect(formUtil.getMatchingLocale('fr', available)).toBe('fr');
		expect(formUtil.getMatchingLocale('fr-CA', available)).toBe('fr');
		expect(formUtil.getMatchingLocale('es-ES-1', available)).toBe('es-ES');
		expect(formUtil.getMatchingLocale('nl-NL', available)).toBe('nl-NL');
	});

	it('creates a session key unique to a type of form', function() {

		var formOpts = {
			project: 'export-Kinderbijslag',
			flow: 'Start',
			version: '0.0-Wetwijziging',
			languageCode: 'nl-NL'
		};

		var expectedKey = 'bbForms.export-Kinderbijslag.Start.0.0-Wetwijziging.nl-NL';
		var actualKey = formUtil.getFormSessionKey(formOpts);
		expect(expectedKey).toBe(actualKey);
	});

	it('converts an http error response into an error object', function() {

		var mockRes, err;

		//no error in response
		mockRes = {};
		err = formUtil.getNormalizedErrorFromResponse(mockRes);
		expect(err.type).toBe(formConsts.UNKNOWN_ERR);

		//test 1: typical error when trying to get a model with an expired session id (forms 5.4)
		mockRes = {
			data: {
				message: "Unknown session for id 'd4617e52-e41a-4ccb-ac3f-45fad3803ec0'",
				stacktrace: null,
				type: "Unknown session"
			}
		};
		err = formUtil.getNormalizedErrorFromResponse(mockRes);
		expect(err.type).toBe(formConsts.SESSION_EXPIRED_ERR);

		//test 2: typical error when trying to update an expired session (forms 5.4)
		mockRes = {
			data: {
				message: "Unknown session for id 'd4617e52-e41a-4ccb-ac3f-45fad3803ec0'",
				stacktrace: "com.aquima.web.api.exception.UnknownSubscriptionException: Unknown subscription for id '40196965-3564-472d-af64-ffc6199d07f1",
				type: "Exception"

			}
		};
		err = formUtil.getNormalizedErrorFromResponse(mockRes);
		expect(err.type).toBe(formConsts.SESSION_EXPIRED_ERR);

		//test 3: typical error when trying to update an expired session (forms 5.5)
		mockRes = {
			data: {
				errorType: "EXCEPTION"
			}
		};
		err = formUtil.getNormalizedErrorFromResponse(mockRes);
		expect(err.type).toBe(formConsts.SESSION_EXPIRED_ERR);

		//test 4: typical error when trying to get a model with an expired session id (forms 5.5)
		mockRes = {
			data: {
				errorType: "UNKNOWN_SESSION"
			}
		};
		err = formUtil.getNormalizedErrorFromResponse(mockRes);
		expect(err.type).toBe(formConsts.SESSION_EXPIRED_ERR);
	});
});