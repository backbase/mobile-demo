describe('Element directive', function() {
	var $compile,
		$httpBackend,
		scope,
		formUtil;


	beforeEach(module('forms-ui'));

	beforeEach(module(function ($provide) {
		$provide.provider('templateBase', function () {
			this.$get = function () {
				return '/path/to/templates'
			}
		});
	}));

	beforeEach(inject(function($injector){

		$compile = $injector.get('$compile');
		$httpBackend = $injector.get('$httpBackend');
		scope =  $injector.get('$rootScope').$new();
		formUtil =  $injector.get('formUtil');
	}));

	var formTemplateHtml = '<div class="form"></div>';
	var controlTemplateHtml = '<div id="{{element.key}}">{{element.values[0]}}</div>';

	it('replaces the element with the appropriate content', function() {

		spyOn(formUtil, 'findTemplate').and.returnValue('the-template.html');

		//compile the parent form so the required form controller is there
		var formEl = $compile('<div bb-form></div>')(scope);
		$httpBackend.expectGET('/path/to/templates/form/form.html').respond(200, formTemplateHtml);
		$httpBackend.flush();

		var formController = formEl.controller('bbForm');

		spyOn(formController, 'lookupElement').and.callFake(function() {
			return {
				key: 'F1',
				values: [ '321' ]
			};
		});

		//this is the is the directive to test. append it to the form directive and compile
		var elementEl = angular.element('<div bb-element element-ref="{{elementRef}}"></div>');
		formEl.append(elementEl);
		elementEl =  $compile(elementEl)(scope);
		$httpBackend.expectGET('/path/to/templates/the-template.html').respond(200, controlTemplateHtml);

		scope.elementRef = 'F1';
		scope.$digest();

		//flush now because the watch that requests the template will only invoke after the digest
		$httpBackend.flush();

		expect(formController.lookupElement).toHaveBeenCalledWith('F1');
		expect(elementEl.html()).toMatch(/id="?F1"?/); //IE8 doesn't wrap the ID in quotes
		expect(elementEl.html()).toContain('>321<');
	});
});