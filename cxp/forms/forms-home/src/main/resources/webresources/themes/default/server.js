var express = require('express'),
	serveStatic = require('serve-static'),
	httpProxy = require('http-proxy'),
	url = require('url'),
	fs = require('fs'),
	path = require('path'),
	formsConfig = require('./runtime.conf.json');

var argv = require('minimist')(process.argv.slice(2));

var app = express();

var sessionPageHtml = fs.readFileSync('./mocks/mock-session-page.html', 'utf8').replace(/RUNTIME_PATH/g, formsConfig.runtimePath);
app.use(function(req, res, next) {

	//cors stuff
	res.header('Access-Control-Allow-Origin', req.headers['Origin'] || req.headers['origin'] || '*');
	res.header('Access-Control-Allow-Methods', 'GET, POST');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Cookie');
	res.header('Access-Control-Allow-Credentials', 'true');

	next();
});

if(argv.mock) {

	//handle responses with mock json
	app.use(new RegExp('^' + formsConfig.runtimePath + '/server/start(.*)$'), function (req, res, next) {
		console.log('Sending response for session start');
		res.send(sessionPageHtml);
	});
	console.log('Serving mocked responses.');
	app.use(new RegExp('^' + formsConfig.runtimePath + '/server/session/[A-Za-z0-9-]+/api/subscribe$'), function (req, res, next) {
		console.log('Sending response for session subscribe');
		res.send('');
	});
	app.use(new RegExp('^' + formsConfig.runtimePath + '/server/session/[A-Za-z0-9-]+/api/subscribe/[A-Za-z0-9-]+$'), function (req, res, next) {
		try {
			var mockFile = path.join(__dirname, argv.mock);
			console.log('Serving mock file at: %s', mockFile);
			mockFile = JSON.parse(fs.readFileSync(mockFile, 'utf8'));
			res.json(mockFile);
		} catch(e) {
			return next(e);
		}
	});
} else {

	//handle proxied responses
	var proxy = httpProxy.createProxyServer({});
	proxy.on('error', function (err, req, res) {
		console.log(err);
		console.log(req.url);
	});

	proxy.on('proxyRes', function (proxyRes, req, res) {
		//normalize http response headers
		var key, normalKey;
		for(key in proxyRes.headers) {
			normalKey = key.replace(/^[a-z]|-[a-z]/g, function(a){
				return a.toUpperCase();
			});
			proxyRes.headers[normalKey] = proxyRes.headers[key];
			delete proxyRes.headers[key];
		}
	});

	app.use(formsConfig.runtimePath, function (req, res, next) {
		console.log('Serving proxy response');
		proxy.web(req, res, {
			target: formsConfig.runtimeServer + formsConfig.runtimePath
		});
	});
}

app.use(serveStatic(__dirname));

app.listen(5050, function() {
	console.log('The Forms lite proxy server is listening on port 5050');
});
