describe('Form directive', function() {
	var $compile,
		scope,
		$q,
		$httpBackend,
		formFactory;


	beforeEach(module('forms-ui'));

	beforeEach(module(function ($provide) {
		$provide.provider('templateBase', function () {
			this.$get = function () {
				return '/path/to/templates'
			}
		});
	}));

	beforeEach(inject(function($injector){

		$compile = $injector.get('$compile');
		$q = $injector.get('$q');
		$httpBackend = $injector.get('$httpBackend');
		formFactory = $injector.get('formFactory');
		scope =  $injector.get('$rootScope').$new();
	}));

	beforeEach(function() {

	});

	var formTemplateHtml = '<div class="bbform">Root: {{getRootElement().key}}</div>';

	it('gets the form and makes it available on scope', function(done) {

		var mockModel = {
			key: 'F1'
		};
		var mockModelPromise = $q.when(mockModel);
		spyOn(formFactory, 'getModel').and.returnValue(mockModelPromise);

		var formEl = $compile('<div bb-form></div>')(scope);
		$httpBackend.expectGET('/path/to/templates/form/form.html').respond(200, formTemplateHtml);
		$httpBackend.flush();

		mockModelPromise['finally'](function() {
			var formScope = formEl.isolateScope();

			expect(formScope.model).toBe(mockModel);
			done();
		});
		scope.$digest();
	});

	it('allows the root element (the page) of the form to be looked up', function(done) {

		var mockModel = {
			elements: [{
				key: 'C1',
				type: 'container'
			},
			{
				key: 'P1',
				type: 'page'
			},
			{
				key: 'F1',
				type: 'field'
			}]
		};
		var mockModelPromise = $q.when(mockModel);
		spyOn(formFactory, 'getModel').and.returnValue(mockModelPromise);

		var formEl = $compile('<div bb-form></div>')(scope);
		$httpBackend.expectGET('/path/to/templates/form/form.html').respond(200, formTemplateHtml);
		$httpBackend.flush();

		mockModelPromise['finally'](function() {
			expect(formEl.html()).toContain('Root: P1');
			done();
		});
		scope.$digest();
	});
});