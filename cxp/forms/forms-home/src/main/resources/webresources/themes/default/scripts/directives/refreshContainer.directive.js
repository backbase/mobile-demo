(function() {
    'use strict';

    var app = angular.module('forms-ui');

    /**
     * This directive is designed to render a Accordion Group
     */
    app.directive('refreshContainer', /*@ngInject*/ function(templateBase,$document) {

        return {
            restrict: 'CA',
            link: function(scope, element) {

                var refreshFunction = function(e,data){
                    var source = element.find('#' + data.event).not('.bb-refresh-button');
                    if(source.size() && !scope.lookupElement(data.event).messages.length > 0){
                        element.find('.bb-refresh-button').click();
                    }
                }

//                scope.$on("$destroy", function handleDestroyEvent() {
//                    $document.off('bbf.refresh', refreshFunction);
//                });

                $document.on('bbf.refresh', refreshFunction);
            }
        };
    });
})();