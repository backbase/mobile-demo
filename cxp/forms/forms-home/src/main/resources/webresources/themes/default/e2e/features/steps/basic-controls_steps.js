//http://chaijs.com/
var chai = require('chai'),
	chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
var expect = chai.expect;

module.exports = function() {

	this.Before(function(done) {
		this.currentInput = null;
		this.currentInputSet = [];
		this.displayLength = -1;
		this.currentPresentationStyle = null;

		browser.get('/examples');
		browser.executeScript('angular.element(document.getElementsByClassName("bbform")[0]).scope().resetSession();');
		element(by.buttonText('Next')).click();

		done();
	});

	this.Given('a $attrType attribute is modelled in Forms Studio and is then rendered', function (attrType, done) {
		done();
	});

	this.Given('it has the presentation style "$presentationStyle"', function(presentationStyle, done) {
		this.currentPresentationStyle = presentationStyle;
		done();
	});

	this.When('it is named "$fieldName"', function(fieldName, done) {
		var self = this;
		var tagName = this.currentPresentationStyle === 'memo' ? 'textarea' : '';
		element.all(by.css(tagName + '[name="' + fieldName + '"]')).then(function(els) {
			self.currentInput = els[0].getWebElement();
			self.currentInputSet = els.map(function(el) {
				return el.getWebElement();
			});
			done();
		});
	});

	this.When('the display length is set to $displayLength', function(displayLength, done) {
		this.displayLength = displayLength;
		done();
	});

	this.Then(/^an? (.+) tag should be rendered with the value "(.*)"$/, function (tagName, inputValue, done) {

		var self = this;
		this.currentInput.getTagName().then(function(actualTagName) {
			expect(tagName).to.equal(actualTagName);
			return actualTagName === 'textarea' ?
				self.currentInput.getText() : self.currentInput.getAttribute('value');
		}).then(function(value) {
			expect(value.trim()).to.equal(inputValue.trim());
			done();
		});
	});

	this.Then('it accepts input up to $characterCount characters', function (characterCount, done) {

		var self = this;

		var str = '';
		for(var i = 0; i < characterCount; i++) {
			str += String.fromCharCode(Math.floor(Math.random() * (126 - 31)) + 32);
		}
		this.currentInput.sendKeys(str);

		expect(this.currentInput.getAttribute('value')).to.eventually.equal(str).then(function() {
			var extraChars = 'some more chars';
			self.currentInput.sendKeys(extraChars);
			expect(self.currentInput.getAttribute('value')).to.eventually.not.equal(str + extraChars).and.notify(done);
		});
	});

	this.Then('a checkbox should be rendered and $checkedState', function (checkedState, done) {
		var state = !!(checkedState === "checked");
		expect(this.currentInput.isSelected()).to.eventually.equal(state).and.notify(done);
	});

	this.Then('a select box should be rendered with the options $optionsList', function(optionsList, done) {
		var items = splitStringList(optionsList);
		items.unshift('Choose an option');

		this.currentInput.findElements(by.css('option')).then(function(optionEls) {

			var optionTextPromises = optionEls.map(function(optionEl) {
				return optionEl.getText();
			});

			protractor.promise.all(optionTextPromises).then(function(texts) {
				expect(texts.toString()).equal(items.toString());
				done();
			});
		});
	});

	this.Then("a select box should be rendered with the option $optionText selected by default", function(optionText, done) {
		this.currentInput.findElements(by.css('option[selected]')).then(function(selectedEls) {
			expect(selectedEls[0].getText()).to.eventually.equal(optionText).and.notify(done);
		});
	});

	this.Then(/^(\d+) checkboxes should be rendered with the labels: (.+)$/, function(numCheckboxes, expectedLabels, done) {

		numCheckboxes = parseInt(numCheckboxes);
		expectedLabels = splitStringList(expectedLabels);

		expect(numCheckboxes).to.equal(this.currentInputSet.length);
		expect(expectedLabels.length).to.equal(this.currentInputSet.length);

		var labelTextPromises = this.currentInputSet.map(function(input) {
			return input.findElement(by.xpath('ancestor::label')).getText();
		});

		protractor.promise.all(labelTextPromises).then(function(labelTexts) {
			labelTexts.forEach(function(label, i) {
				expect(label).to.equal(expectedLabels[i]);
			});

			done();
		});
	});

	this.Then(/^(\d+) checkboxes should be rendered/, function(numCheckboxes, done) {

		numCheckboxes = parseInt(numCheckboxes);
		expect(numCheckboxes).to.equal(this.currentInputSet.length);
		done();
	});

	this.Then(/^have the labels: (.+)$/, function(expectedLabels, done) {

		var expectedLabels = splitStringList(expectedLabels);
		expect(expectedLabels.length).to.equal(this.currentInputSet.length);

		var labelTextPromises = this.currentInputSet.map(function(input) {
			return input.findElement(by.xpath('ancestor::label')).getText();
		});

		protractor.promise.all(labelTextPromises).then(function(labelTexts) {
			labelTexts.forEach(function(label, i) {
				expect(label).to.equal(expectedLabels[i]);
			});

			done();
		});
	});

	this.Then(/^has selected values for the labels:\s(.*)$/, function(expectedLabels, done) {

		var self = this;
		expectedLabels = expectedLabels === '[none]' ? [] : splitStringList(expectedLabels);

		var inputStatePromises = this.currentInputSet.map(function(input) {
			return input.isSelected(); //returns promise which resolves to a boolean
		});

		protractor.promise.all(inputStatePromises).then(function(inputStates) {
			return self.currentInputSet.filter(function(input, i) {
				return inputStates[i];
			});
		}).then(function(selectedInputs) {
			//nested promises are necessary because protractor promises don't have a 'spread' function
			var selectedLabelPromises = selectedInputs.map(function(input) {
				return input.findElement(by.xpath('ancestor::label')).getText();
			});
			protractor.promise.all(selectedLabelPromises).then(function(selectedLabelTexts) {
				expect(selectedLabelTexts.length).to.equal(expectedLabels.length);

				selectedLabelTexts.forEach(function(selectedLabel) {
					expect(expectedLabels.indexOf(selectedLabel) > -1).to.equal(true);
				});

				done();
			});
		});
	});

	function splitStringList(str) {
		return str.split(/,\s|\sand\s/);
	}
};


