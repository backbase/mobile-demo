(function () {
  'use strict';
  var app = angular.module('forms-ui'),
    BB_MODAL = 'modal';

  app.directive(BB_MODAL, /*@ngInject*/ function ($log, debugEnabled, formUtil, templateBase) {
    return {
      restrict: 'C',
      require: '^bbForm',
      scope: {
        element: '='
      },
      link: function (scope, element, attrs, formsController) {

        //logging
        if(debugEnabled && scope.element) {
          $log.debug('%s: linking lightbox: %s', BB_MODAL, formUtil.elementToString(scope.element));
          $log.debug('%s: ...using template: %s', BB_MODAL, templateBase + '/containers/lightbox.html');
        }

        scope.closeBtn = formsController.lookupElement(scope.element.children.filter(function (e) {
          return formsController.lookupElement(e).styles.indexOf('Close') > -1;
        })[0]);

        if (scope.element.styles.indexOf('LightBoxOpen') > -1) {
          element.modal('show');
        } else {
          element.modal('hide');
        }

        element.on('hidden.bs.modal', function () {
          if (scope.element.styles.indexOf('LightBoxOpen') > -1){
            formsController.refresh(scope.closeBtn);
          }
        });

        scope.$watchCollection('element.styles', function () {
          if (scope.element.styles.indexOf('LightBoxOpen') > -1) {
            element.modal('show');
          } else {
            element.modal('hide');
          }
        });
      }
    };
  });
})();