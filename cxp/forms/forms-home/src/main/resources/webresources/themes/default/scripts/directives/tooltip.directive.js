(function () {
    'use strict';

    var app = angular.module('forms-ui'),
        BB_TOOLTIP = 'tooltip';

    app.directive(BB_TOOLTIP, /*@ngInject*/ function ($log, debugEnabled, formUtil) {
        return {
            restrict: 'A',
            scope: {
                element: '='
            },
            link: function (scope, element) {
                //logging
                if (debugEnabled && scope.element) {
                    $log.debug('%s: linking tooltip: %s', BB_TOOLTIP, formUtil.elementToString(scope.element));
                }

                element.tooltip({title: scope.element.explainText});
            }
        };
    });
})();