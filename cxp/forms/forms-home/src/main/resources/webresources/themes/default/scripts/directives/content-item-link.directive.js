(function () {
  'use strict';
  var app = angular.module('forms-ui'),
    BB_LINK = 'contentItemLink';

  app.directive(BB_LINK, /*@ngInject*/ function ($log, debugEnabled, formUtil) {
    return {
      restrict: 'E',
      require: '^bbForm',
      template: '<a class="{{linkClassNames}} content-item" target="_blank" ng-href="{{hrefTuple[1]}}"><i class="{{iconClassName}}"></i> {{hrefTuple[0]}}</a>',
      scope: {
        element: '='
      },
      link: function (scope, element, attrs, formsController) {

        //logging
        if(debugEnabled && scope.element) {
          $log.debug('%s: linking html anchor: %s', BB_LINK, formUtil.elementToString(scope.element));
        }

        var node = {},
          iconNode = {},
          hrefNode = {},
          iconClassName = '',
          linkClassNames = '',
          hrefTuple = [],
          iconNamespaces = {};

        iconNamespaces = {
          'IconFa': 'fa',
          'IconGlyphicon': 'glyphicon'
        };

        function parseIconClassName(str) {
          return str.replace(new RegExp('_','g'),'-');
        }

        function parseHref(str){
          var arr = str.split('|');
          return arr.slice(1);
        }


        /*
          Because of restrictions of Forms Studio,
          only one presentation style for "contentItemLink",
          we use this function to parse string into css class names
         */

        function beautify(styles) {
          var str;
          //create string from array, temporary mark spaces with ','
          str = styles ? styles.join(',') : '';
          // replace '_' on '-'
          str = str.replace(/_/gi, '-');
          // split camel case with ' ';
          str = str.replace(/([a-z](?=[A-Z]))/g, '$1 ');
          // to lower case
          str = str.toLowerCase();
          return str;
        }

        angular.forEach(scope.element.children, function(value) {

          node = formsController.lookupElement(value);

          //create className for icon
          if (node.type === 'contentitem' && iconNamespaces[node.contentStyle] && node.children.length) {
            iconNode = formsController.lookupElement(node.children[0]);
            iconClassName = iconNamespaces[node.contentStyle] + ' ' + parseIconClassName(iconNode.styles[0]);
          }

          //create href and text for link
          if (node.type === 'contentitem' && node.contentStyle === 'LinkURL' && node.children.length) {
            hrefNode = formsController.lookupElement(node.children[0]);
            hrefTuple = parseHref(hrefNode.plainText);
            if (hrefNode.styles.length) {
              linkClassNames = beautify(hrefNode.styles);
            }
          }

        });

        scope.linkClassNames = linkClassNames;
        scope.iconClassName = iconClassName;
        scope.hrefTuple = hrefTuple;

      }
    };
  });
})();