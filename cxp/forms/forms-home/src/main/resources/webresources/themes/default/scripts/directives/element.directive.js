(function() {
	'use strict';

	var BB_ELEMENT = 'bbElement';

	var app = angular.module('forms-ui');

	/**
	 * Element Directive (bbElement)
	 * This directive is designed to render a Backbase forms element. It will likely contain further
	 * bbElement directives or bbControl directives
	 */
	app.directive(BB_ELEMENT,
	/*@ngInject*/ function($compile, $http, $templateCache, $log, formUtil, uiRules, templateBase, debugEnabled) {

		return {
			restrict: 'EA',
			require: '^bbForm',
			scope: {
				elementRef: '@'
            },
			link: function(scope, element, attrs, formsController) {
				scope.refresh = formsController.refresh;
				scope.hasStyle = formsController.hasStyle;
				scope.lookupElement = formsController.lookupElement;
				scope.lookupMessage = formsController.lookupMessage;
				scope.hasStyleContaining = formsController.hasStyleContaining;
				scope.stylesContaining = formsController.stylesContaining;

				var compileElement = function(){
						templateBase = /\/$/.test(templateBase) ? templateBase : templateBase + '/';
						var templateUrl =
								templateBase + formUtil.findTemplate(scope.element, uiRules.elements);
						//logging
						if(debugEnabled && scope.element) {
								$log.debug('%s: linking element: %s', BB_ELEMENT, formUtil.elementToString(scope.element));
								$log.debug('%s: ...using template: %s', BB_ELEMENT, templateUrl);
						}
						$http.get(templateUrl, { cache: $templateCache }).then(function(template) {
								element.html(template.data);
								$compile(element.contents())(scope);
						}).then(null, function() { //catch (ie8 safe)
								element.html('');
						});
				};

				scope.$watch('elementRef', function() {
					scope.element = scope.lookupElement(scope.elementRef);
					if(scope.element) {
						scope.$watchCollection('[element,element.type,element.domain,element.styles,element.contentStyle]', compileElement);
					}
				});
			}
		};
	});
})();