exports.config = {
	framework: 'cucumber',
	baseUrl: 'http://localhost:5050',
	seleniumAddress: 'http://localhost:4444/wd/hub',
	specs: [
		'e2e/features/*.feature'
	],
	cucumberOpts: {
		require: 'e2e/features/steps/*_steps.js',
		format: 'summary'
	}
};