(function () {
    'use strict';

    var app = angular.module('forms-ui'),
        BB_TOGGLERADIO = 'toggleRadio';

    app.directive(BB_TOGGLERADIO, /*@ngInject*/ function ($log, $parse, debugEnabled) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {

                //logging
                if (debugEnabled && scope.element) {
                    $log.debug('%s: linking radio toggle', BB_TOGGLERADIO);
                }

                var isInput = element[0].nodeName === 'INPUT';
                var activeElement = isInput ? element.parent() : element;

                scope.$watch(function () {
                    return ngModel.$viewValue;
                }, function () {
                    var isActive = ngModel.$modelValue === attrs.value;
                    element.prop('checked', isActive);
                    activeElement.toggleClass('active', isActive);
                });

                element.change(function () {
                    if (element.prop('checked')) {
                        ngModel.$setViewValue(attrs.value);
                    }
                });

                scope.lock = function (e) {
                    e.stopImmediatePropagation();
                    e.preventDefault();
                };

                scope.$watch(function () {
                    return $parse(attrs.ngDisabled)(scope);
                }, function (disabled) {
                    //Disable the field
                    if (disabled) {
                        element.parent('label').bind('click tap', scope.lock);
                    } else {
                        element.parent('label').unbind('click tap', scope.lock);
                    }
                });


            }
        };
    });
})();