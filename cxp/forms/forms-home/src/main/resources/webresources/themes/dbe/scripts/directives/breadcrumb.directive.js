(function() {
    'use strict';
    var app = angular.module('forms-ui');

    // remove all directive
    app.config(function($provide) {
        $provide.decorator('breadcrumbContainerDirective', function($delegate) {
            $delegate.shift();
            return $delegate;
        });
    });


    app.directive('breadcrumbDbe', /*@ngInject*/ function( $log, debugEnabled, $filter, $compile) {
        return {
            restrict: 'C',
            require: '^bbForm',
            scope : true,
            link: function(scope, element, attrs, formsController) {
                scope.refresh = formsController.refresh;
                scope.click = formsController.click;
                scope.hasStyle = formsController.hasStyle;
                scope.lookupMessage = formsController.lookupMessage;

                scope.$watchCollection('[element,element.type,element.domain,element.children]', function() {
                    var steps = [];
                    angular.forEach(scope.element.children, function(element){
                        steps.push( formsController.lookupElement(element) );
                    });
                    scope.steps = steps;
                })
            }
        };
    });
})();