(function() {
	'use strict';

	var FORM_FACTORY = 'formFactory';
	var CONSECUTIVE_MODEL_ATTEMPTS = 3;

	var app = angular.module('forms-ui');

	/**
	 * Form Factory
	 * Abstraction to get and update a form.
	 */
	app.factory(FORM_FACTORY,
	/*@ngInject*/ function($q, $log, $timeout, debugEnabled, sessionService, modelService, deltaService, formUtil, formConsts) {

		/**
		 * Constuctor
		 * @constructor
		 */
		var FormFactory = function() {
			//if the request to get model fails a consecutive number of times it will give up
			this.consecutiveModelTries = 0;
		};

		/**
			 *
			 * @param runtimeUrl
			 * @param sessionOpts
			 * @param sessionId
			 */
		FormFactory.prototype.init = function(runtimeUrl, sessionOpts, parameters) {
			this.runtimeUrl = runtimeUrl;
			this.sessionOpts = sessionOpts;
      this.parameters = parameters;
		};

		/**
		 * Gets a model. First creating a new session if necessary
		 * @returns {Promise|*}
		 */
		FormFactory.prototype.getModel = function() {
			var self = this;

			//the promise chain attempts to use a sessionId if one is supplied otherwise it will start a new session
			//if the session has expired it will clear the session id and recall this function so a new session is requested
			return $q.when(self.sessionOpts.sessionId)
				.then(function(sessionId) {
          //if no session id, start one, else continue
          if(!sessionId) {
            $log.info('%s: Attempting to start new session...', FORM_FACTORY);
            return sessionService.startSession(self.runtimeUrl, self.sessionOpts, self.parameters);
          } else {
            return sessionId;
          }
				})
				.then(function(sessionId) {
          self.sessionOpts.sessionId = sessionId;
          modelService.init(self.runtimeUrl, self.sessionOpts.sessionId);
          $log.info('%s: Subscribing to session for: %s...', FORM_FACTORY, self.sessionOpts.sessionId);
          return sessionService.subscribeSession(self.runtimeUrl, self.sessionOpts);
				})
				.then(function(sessionId) {
          self.sessionOpts.sessionId = sessionId;
          modelService.init(self.runtimeUrl, self.sessionOpts.sessionId);
          $log.info('%s: Requesting model for: %s...', FORM_FACTORY, self.sessionOpts.sessionId);
          return modelService.getModel();
				})
				.then(function(model) {
          $log.info('%s: Model retrieved.', FORM_FACTORY);
          self.currentElements = angular.copy(model.elements);
          self.consecutiveModelTries = 0;
          return model;
				})
				.then(null, function(err) { //catch (ie8 safe)
          if(err.type === formConsts.SESSION_EXPIRED_ERR && self.consecutiveModelTries <= CONSECUTIVE_MODEL_ATTEMPTS) {
              $log.info('%s: Session expired.', FORM_FACTORY);
              self.sessionOpts.sessionId = null;
              self.consecutiveModelTries++;
              return self.getModel();
          } else {
              $log.error(err.message);
              throw err;
          }
				});
		};

		/**
		 * Updates a model, given changes from the view
		 * @param modelFromView
		 * @param refreshSource
		 * @returns {*}
		 */
		FormFactory.prototype.updateModel = function(modelFromView, refreshSource) {
			var self = this;

			$log.info('%s: Updating model...', FORM_FACTORY);
			if(!refreshSource) {
				return $q.reject(new Error('Cannot update without a refresh source'));
			} else {
				$log.info('%s: Refreshing model from server...', FORM_FACTORY);
			}

			var deltasToSend = deltaService.makeRequestDeltas(self.currentElements, modelFromView.elements);
			var deltas = {
				elementKey: refreshSource.key,
				fields: deltasToSend
			};

			return modelService.updateModel(deltas).then(function(data) {
				return self.applyChanges(modelFromView, data);
			}).then(null, function(e) {
				//SESSION TIMEOUT
				//if e is expired session:
				if(e.type === formConsts.SESSION_EXPIRED_ERR) {
					$log.info('%s: Session expired.', FORM_FACTORY);
					self.sessionOpts.sessionId = null;
				}
				throw e;
			});
		};

		FormFactory.prototype.applyChanges = function(modelFromView, data) {
			var self = this;

			return $q.when(data).then(function(data){
				$log.info('%s: Model updated.', FORM_FACTORY);

				if(data.events && data.events.length) {
					var retrievedDeltas = [];
					data.events.forEach(function(event){
							var retrievedDeltas = event.changes.changes;
							$log.debug(retrievedDeltas);
              deltaService.applyResponseDeltas(modelFromView.elements, retrievedDeltas);
					});

          self.currentElements = angular.copy(modelFromView.elements);

					return $q.when(retrievedDeltas);
				} else {
						$log.warn('%s: Model updated, but server responded with no events.', FORM_FACTORY);
				}
				return modelFromView;
			});
		};

		//export
		return new FormFactory();
	});
})();