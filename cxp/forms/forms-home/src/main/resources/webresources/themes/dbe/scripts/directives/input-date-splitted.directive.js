(function () {
    'use strict';
    var app = angular.module('forms-ui');

    app.directive('inputDateSplitted', /*@ngInject*/ function () {

        var template = '<div class="row">\
                            <div class="col-sm-4 ">\
                                <select class="form-control" ng-model="val.date" ng-options="d for d in dates" ng-disabled="{{ element.disabled || element.readonly }}">\
                                    <option value disabled selected>Giorno</option>\
                                </select>\
                            </div>\
                            <div class="col-sm-4 ">\
                                <select class="form-control" ng-model="val.month" ng-options="m.value as m.name for m in months" ng-disabled="{{ element.disabled || element.readonly }}">\
                                    <option value disabled>Mese</option>\
                                </select>\
                            </div>\
                            <div class="col-sm-4 ">\
                                <select class="form-control" ng-model="val.year" ng-options="y for y in years" ng-disabled="{{ element.disabled || element.readonly }}">\
                                    <option value disabled selected>Anno</option>\
                                </select>\
                            </div>\
                        </div>';

        return {
            restrict: 'C',
            replace: true,
            template: template,
            require: ['ngModel', '^bbForm'],
            scope: {
                element: '='
            },

            link: function (scope, elem, attrs, controllers) {
                var italianMonths = ["Gennaio", "Febbraio", "Marzo", 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre']
                var model = controllers[0];
                var formsController = controllers[1];

                // Default values
                var min = moment().subtract(100, 'y');
                var max = moment().add(20, 'y');

                var parseDateParams = function(rule){
                    var date = window.moment();
                    if(rule.compareday){
                        date.add(parseFloat(rule.compareday), 'days');
                    }
                    if(rule.comparemonth){
                        date.add(parseFloat(rule.comparemonth), 'months');
                    }
                    if(rule.compareyear){
                        date.add(parseFloat(rule.compareyear), 'years');
                    }

                    return date;
                };

                var greater = scope.element.validations.filter(function(v){
                    return v.type === 'Date' && v.parameters.comparator === 'greater';
                });

                var less = scope.element.validations.filter(function(v){
                    return v.type === 'Date' && v.parameters.comparator === 'less';
                });

                max = less.length ? parseDateParams(less[0].parameters) : max;
                min = greater.length ? parseDateParams(greater[0].parameters) : min;

                if (scope.element.styles.indexOf('date_of_birth') > -1) {
                    max = moment().set('year', moment().get('year') - 18);
                }

                scope.val = {};
                scope.years = [];

                for (var i = max.year(); i >= min.year(); i--) {
                    scope.years.push(i);
                }

                scope.$watch('val.year', function () {
                    updateMonthOptions();
                });

                scope.$watchCollection('[val.month, val.year]', function () {
                    updateDateOptions();
                });

                scope.$watchCollection('[val.date, val.month, val.year]', function () {
                    if (scope.val.year && scope.val.month && scope.val.date) {
                        var m = moment({
                            year: scope.val.year,
                            month: scope.val.month - 1,
                            date: scope.val.date
                        }).toDate();
                        scope.element.values = [m];
                        formsController.refresh(scope.element);
                    }
                });

                function updateMonthOptions() {
                    // Values begin at 1 to permit easier boolean testing
                    scope.months = [];

                    var minMonth = scope.val.year && min.isSame([scope.val.year], 'year') ? min.month() : 0;
                    var maxMonth = scope.val.year && max.isSame([scope.val.year], 'year') ? max.month() : 11;

                    var monthNames = moment.months();

                    for (var j = minMonth; j <= maxMonth; j++) {
                        scope.months.push({
                            name: italianMonths[j],
                            value: j + 1
                        });
                    }

                    if (scope.val.month - 1 > maxMonth || scope.val.month - 1 < minMonth) delete scope.val.month;
                }

                function updateDateOptions(year, month) {
                    var minDate, maxDate;

                    if (scope.val.year && scope.val.month && min.isSame([scope.val.year, scope.val.month - 1], 'month')) {
                        minDate = min.date();
                    } else {
                        minDate = 1;
                    }

                    if (scope.val.year && scope.val.month && max.isSame([scope.val.year, scope.val.month - 1], 'month')) {
                        maxDate = max.date();
                    } else if (scope.val.year && scope.val.month) {
                        maxDate = moment([scope.val.year, scope.val.month - 1]).daysInMonth();
                    } else {
                        maxDate = 31;
                    }

                    scope.dates = [];

                    for (var i = minDate; i <= maxDate; i++) {
                        scope.dates.push(i);
                    }
                    if (scope.val.date < minDate || scope.val.date > maxDate) delete scope.val.date;
                }


                scope.$watch('element.values[0]', function (ov, nv) {

                    var modelValue = scope.element.values[0];

                    if (!modelValue) return;

                    var m = moment(modelValue);

                    // Always use a dot in ng-model attrs...
                    scope.val = {
                        year: m.year(),
                        month: m.month() + 1,
                        date: m.date()
                    };

                    if (m.isAfter(max)){
                        scope.element.values[0]='';
                    }

                });
            }
        };
    });
})();