(function () {
  'use strict';
  var app = angular.module('forms-ui');

  app.directive('contentItemHeading', /*@ngInject*/ function ($filter) {

    return {
      restrict: 'E',
      require: '^bbForm',
      template:'<div ng-switch="index">' +
                '<h1 class="{{classNames}}" ng-switch-when="1">{{plainText}}</h1>' +
                '<h2 class="{{classNames}}" ng-switch-when="2">{{plainText}}</h2>' +
                '<h3 class="{{classNames}}" ng-switch-when="3">{{plainText}}</h3>' +
                '<h4 class="{{classNames}}" ng-switch-when="4">{{plainText}}</h4>' +
                '<h5 class="{{classNames}}" ng-switch-when="5">{{plainText}}</h5>' +
                '<h6 class="{{classNames}}" ng-switch-when="6">{{plainText}}</h6>' +
              '</div>',
      scope: {
        element: '='
      },
      link: function (scope, element, attrs, formsController) {

        var child = {}, plainText, index, classNames = '';

        index = scope.element.contentStyle.slice(-1);
        child = formsController.lookupElement(scope.element.children[0]);
        plainText = child.plainText;

        if (child.styles.length) {
          classNames = $filter('beautify')(child.styles);
        }

        scope.index = index;
        scope.plainText = plainText;
        scope.classNames = classNames;
      }
    };
  });
})();