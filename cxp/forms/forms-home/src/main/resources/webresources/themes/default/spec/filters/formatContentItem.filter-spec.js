describe('Format Content Item Filter', function() {
	var formatContentItem;

	beforeEach(module('forms-ui'));

	beforeEach(inject(function($injector) {
		formatContentItem = $injector.get('$filter')('formatContentItem');
	}));

	it('formats an element\'s content item nodes', function() {

		var formattedText;

		var contentNodes = [{
			text: "Er zijn (nog) geen kinderen aanwezig.",
			nodeType: "text"
		}];

		formattedText = formatContentItem(contentNodes);
		expect(formattedText).toBe('Er zijn (nog) geen kinderen aanwezig.');
	});

});