(function() {
  'use strict';

  var BB_JSON = 'jsonObject';

  var app = angular.module('forms-ui');

  app.directive(BB_JSON, /*@ngInject*/ function() {
      return {
        restrict: 'EA',
        require: '^bbForm',
        link: function(scope, element, attrs, formsController) {
          var el = scope.element;

          function parseTextItem(ti){
            return formsController.lookupElement(ti.children[0]).plainText;
          }

          function parseContentItem(ci){
            return {
              key: parseTextItem(formsController.lookupElement(ci.children[1])),
              value: parseTextItem(formsController.lookupElement(ci.children[2]))
            };
          }

          function parseJSON(children){
            var res = {};
            angular.forEach(children,function(childKey){
              var child = formsController.lookupElement(childKey);
              if(child.contentStyle === 'BB_JSON_Object'){
                res[child.displayName] = parseJSON(child.children);
              } else if (child.type === 'contentitem' && child.children.length > 0){
                angular.forEach(child.children,function(ciKey){
                  var ci = parseContentItem(formsController.lookupElement(ciKey));
                  res[ci.key] = ci.value;
                });
              }
            });

            return res;
          }

          formsController.setConfig(parseJSON(el.children));
        }
      };
    });
})();