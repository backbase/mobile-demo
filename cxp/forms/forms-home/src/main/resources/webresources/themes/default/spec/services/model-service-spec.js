describe('Model service', function () {

	// Load your module.
	beforeEach(module('forms-ui'));

	var modelService, $httpBackend;

	beforeEach(inject(function($injector) {
		$httpBackend = $injector.get('$httpBackend');
		modelService = $injector.get('modelService');

		jasmine.getJSONFixtures().fixturesPath='base/mocks';
		//To load a fixture:
		//jasmine.getJSONFixtures().load('kinderbijslag/model.json')
	}));

	it('gets a model from a server', function(done) {

		$httpBackend.expectPOST('/run/server/session/123/api/subscribe/123').respond(200,  'dataok');

		modelService.init('/run', '123');
		modelService.getModel().then(function(data) {
			expect(data).toBe('dataok');
			done();
		});

		$httpBackend.flush();
	});

	it('throws if the session timed out when getting a model', function(done) {

		//this is a copy of what the runtime actually sends when a session is not found
		var errData = {
			errorType: "UNKNOWN_SESSION",
			message: "Unknown session for id 'f3033dd6-f8ae-4515-a6b7-b88b8615efd7'",
			stacktrace: null,
			title: "Unknown session"
		};
		var httpSuccess = false;
		var httpFail = false;

		$httpBackend.expectPOST('/run/server/session/123/api/subscribe/123').respond(500, errData);

		modelService.init('/run', '123');
		modelService.getModel().then(function() {
			//this should not happen here
			httpSuccess = true;
		}).then(null, function(err) {
			//catch error. this should happen
			httpFail = true;
			expect(err.type).toBe('SESSION_EXPIRED');
		})['finally'](function() {
			expect(httpSuccess).toBe(false);
			expect(httpFail).toBe(true);
			done();
		});

		$httpBackend.flush();
	});

	it('throws for any other error when getting a model', function(done) {

		var errData = {
			message: 'what happened? dunno'
		};
		var httpSuccess = false;
		var httpFail = false;

		$httpBackend.expectPOST('/run/server/session/123/api/subscribe/123').respond(500, errData);

		modelService.init('/run', '123');
		modelService.getModel().then(function() {
			//this should not happen here
			httpSuccess = true;
		}).then(null, function(err) {
			//catch error. this should happen
			httpFail = true;
			expect(err.message).toBe(errData.message);
		})['finally'](function() {
			expect(httpSuccess).toBe(false);
			expect(httpFail).toBe(true);
			done();
		});

		$httpBackend.flush();
	});

	it('sends a delta update to the server', function(done) {

		var deltasToSend = 'foo';

		$httpBackend.expectPOST('/run/server/session/123/api/subscription/123/handleEvent', deltasToSend).respond(200, 'dataok');

		modelService.init('/run', '123');
		modelService.updateModel(deltasToSend).then(function(data) {
			expect(data).toBe('dataok');
			done();
		});

		$httpBackend.flush();
	});

	it('throws if the session timed out when sending a delta update ', function() {

		var deltasToSend = 'foo';

		//this is a copy of what the runtime actually sends when a session is not found
		var errData = {
			errorType: "EXCEPTION",
			message: "Unknown subscription for id '123'",
			stacktrace: null,
			title: "Exception"
		};
		var httpSuccess = false;
		var httpFail = false;

		$httpBackend.expectPOST('/run/server/session/123/api/subscription/123/handleEvent', deltasToSend).respond(500, errData);

		modelService.init('/run', '123');
		modelService.updateModel(deltasToSend).then(function() {
			//this should not happen here
			httpSuccess = true;
		}).then(null, function(err) {
			//catch error. this should happen
			httpFail = true;
			expect(err.type).toBe('SESSION_EXPIRED');
		})['finally'](function() {
			expect(httpSuccess).toBe(false);
			expect(httpFail).toBe(true);
			done();
		});

		$httpBackend.flush();
	});

	it('throws for any other error when sending a delta update ', function() {
		var deltasToSend = 'foo';

		var errData = {
			message: 'what happened? dunno'
		};
		var httpSuccess = false;
		var httpFail = false;

		$httpBackend.expectPOST('/run/server/session/123/api/subscription/123/handleEvent', deltasToSend).respond(500, errData);

		modelService.init('/run', '123');
		modelService.updateModel(deltasToSend).then(function() {
			//this should not happen here
			httpSuccess = true;
		}).then(null, function(err) {
			//catch error. this should happen
			httpFail = true;
			expect(err.message).toBe(errData.message);
		})['finally'](function() {
			expect(httpSuccess).toBe(false);
			expect(httpFail).toBe(true);
			done();
		});

		$httpBackend.flush();
	});
});