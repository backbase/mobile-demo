(function () {
  'use strict';

  /* global moment */

  var app = angular.module('forms-ui');

  /**
   * Central place for miscallaneous form util functions
   */
  app.factory('formUtil', /*@ngInject*/ function ($log, $filter, formConsts) {

    var throttle = function( delay, no_trailing, callback, debounce_mode ) {
      // After wrapper has stopped being called, this timeout ensures that
      // `callback` is executed at the proper times in `throttle` and `end`
      // debounce modes.
      var timeout_id,

      // Keep track of the last time `callback` was executed.
        last_exec = 0;

      // `no_trailing` defaults to falsy.
      if ( typeof no_trailing !== 'boolean' ) {
        debounce_mode = callback;
        callback = no_trailing;
        no_trailing = undefined;
      }

      // The `wrapper` function encapsulates all of the throttling / debouncing
      // functionality and when executed will limit the rate at which `callback`
      // is executed.
      function wrapper() {
        var that = this,
          elapsed = +new Date() - last_exec,
          args = arguments;

        // Execute `callback` and update the `last_exec` timestamp.
        function exec() {
          last_exec = +new Date();
          callback.apply( that, args );
        };

        // If `debounce_mode` is true (at_begin) this is used to clear the flag
        // to allow future `callback` executions.
        function clear() {
          timeout_id = undefined;
        };

        if ( debounce_mode && !timeout_id ) {
          // Since `wrapper` is being called for the first time and
          // `debounce_mode` is true (at_begin), execute `callback`.
          exec();
        }

        // Clear any existing timeout.
        timeout_id && clearTimeout( timeout_id );

        if ( debounce_mode === undefined && elapsed > delay ) {
          // In throttle mode, if `delay` time has been exceeded, execute
          // `callback`.
          exec();

        } else if ( no_trailing !== true ) {
          // In trailing throttle mode, since `delay` time has not been
          // exceeded, schedule `callback` to execute `delay` ms after most
          // recent execution.
          //
          // If `debounce_mode` is true (at_begin), schedule `clear` to execute
          // after `delay` ms.
          //
          // If `debounce_mode` is false (at end), schedule `callback` to
          // execute after `delay` ms.
          timeout_id = setTimeout( debounce_mode ? clear : exec, debounce_mode === undefined ? delay - elapsed : delay );
        }
      };

      // Set the guid of `wrapper` function to the same of original callback, so
      // it can be removed in jQuery 1.4+ .unbind or .die by using the original
      // callback as a reference.
      if ( $.guid ) {
        wrapper.guid = callback.guid = callback.guid || $.guid++;
      }

      // Return the wrapper function.
      return wrapper;
    };

    var debounce = function( delay, at_begin, callback ) {
      return callback === undefined
        ? throttle( delay, at_begin, false )
        : throttle( delay, callback, at_begin !== false );
    };


    return {
      /**
       * nice string representation of an element. Useful for logging
       * @param type of input field I want to test
       * @returns {boolean}
       */
      checkInputFieldType: function (type) {
        var input = document.createElement("input");
        input.setAttribute("type", type);
        return input.type == type;
      },

      /**
       * nice string representation of an element. Useful for logging
       * @param element
       * @returns {string}
       */
      elementToString: function (element) {
        if (element) {
          return element.key + ' ' + element.name + ' (' + element.type + ')';
        } else {
          return 'null element';
        }
      },

      /**
       * Transforming form patterns to ISO ones
       */
      normalizeFormats: function (formats) {
        formats.patterns.date = formats.patterns.date
          .replace(/(\w*)(\w{2})(.)(\w{2})(.)(\w{4})(\w*)/, function (match, p0, p1, p2, p3, p4, p5, p6) {
            return p1.toUpperCase() + p2 + p3.toUpperCase() + p4 + p5.toUpperCase() + p6;
          })
          .replace(/(\w*)(\w{4})(.)(\w{2})(.)(\w{2})(\w*)/, function (match, p0, p1, p2, p3, p4, p5, p6) {
            return p1.toUpperCase() + p2 + p3.toUpperCase() + p4 + p5.toUpperCase() + p6;
          });

        formats.patterns.datetime = formats.patterns.datetime
          .replace(/(\w*)(\w{2})(.)(\w{2})(.)(\w{4})(\w*)/, function (match, p0, p1, p2, p3, p4, p5, p6) {
            return p1.toUpperCase() + p2 + p3.toUpperCase() + p4 + p5.toUpperCase() + p6;
          })
          .replace(/(\w*)(\w{4})(.)(\w{2})(.)(\w{2})(\w*)/, function (match, p0, p1, p2, p3, p4, p5, p6) {
            return p1.toUpperCase() + p2 + p3.toUpperCase() + p4 + p5.toUpperCase() + p6;
          });

        return formats;
      },
      /**
       *
       * @param elementKey
       * @param elements
       * @returns {*}
       */
      lookupElement: function (elementKey, elements) {
        var element = null;
        if (elements) {
          element = $filter('filter')(elements, {"key": elementKey}, true)[0] || element;
        }
        return element;
      },

      /**
       * Given a list of ui rules, returns the template of the matching rule
       * @param element
       * @param uiRules
       * @returns {*}
       */
      findTemplate: function (element, uiRules) {
        var i, template = null;
        for (i = uiRules.length - 1; i >= 0 && !template; i--) {
          try {
            template = uiRules[i].test(element) ? uiRules[i].template : null;
          } catch (e) {
            $log.warn('Error checking UI rule at index %s', i);
            $log.warn(e);
          }
        }
        return template;
      },

      /**
       * Given a list of available locales, returns the best match for the given locale
       * @param locale
       * @param availableLocales
       * @returns {*}
       */
      getLocalizedMessages: function (locale, messages) {
        var availableLocales = Object.keys(messages);
        locale = locale || 'en';

        var bestMatchLocale = availableLocales.sort().reduce(function (prev, current) {
          return locale.indexOf(current) > -1 ? current : prev;
        }, null);

        return messages[bestMatchLocale];
      },

      /**
       * A key unique to a form instance
       * @param formOpts
       * @returns {string}
       */
      getFormSessionKey: function (formOpts) {
        var keyParts = [ 'bbForms', formOpts.project, formOpts.flow, formOpts.version, formOpts.languageCode ];
        return keyParts.join('.');
      },


      /**
       * Return if the element is in the viewport
       * @param el
       * @returns {boolean}
       */
      isElementInViewport: function (el) {
        var rect = el.getBoundingClientRect();

        return (
          rect.top >= 0 &&
          rect.left >= 0 &&
          rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
          rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
        );
      },


      /**
       * Return if the device is a mobile device
       * @returns {boolean}
       */
      isMobile: function () {
        var check = false;
        (function (a) {
          if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) {
            check = true;
          }
        })(navigator.userAgent || navigator.vendor || window.opera);
        return check;
      },


      /**
       * Getting consistent error messages across Forms versions can be troublesome.
       * You may need to decorate this function
       */
      getNormalizedErrorFromResponse: function (errResponse) {

        var err = new Error();
        err.type = formConsts.UNKNOWN_ERR;

        //exit early if no response data
        if (!errResponse.data) {
          return err;
        }

        //wrap original info
        err.message = errResponse.data.message;
        err.wrapped = errResponse.data;

        //determine if its a problem with the session
        var sessionExpired =
          (errResponse.data.errorType === 'EXCEPTION') ||
          (errResponse.data.errorType === 'UNKNOWN_SESSION') ||
          (errResponse.data.stacktrace && errResponse.data.stacktrace.indexOf(formConsts.AQUIMA_SESSION_EXCEPTION) > -1) ||
          (errResponse.data.type && errResponse.data.type.toLowerCase() === 'unknown session');
        if (sessionExpired) {
          err.type = formConsts.SESSION_EXPIRED_ERR;
          return err;
        }

        //determine if its because the form app is not known
        var unknownApp =
          (errResponse.data.stacktrace && errResponse.data.stacktrace.indexOf(formConsts.AQUIMA_UNKNOWN_APP_EXCEPTION) > -1);
        if (unknownApp) {
          err.type = formConsts.UNKNOWN_APP_ERR;
          return err;
        }

        //determine if its because the language is not valid
        var unknownLang =
          (errResponse.data.stacktrace && errResponse.data.stacktrace.indexOf(formConsts.AQUIMA_UNKNOWN_LANG_EXCEPTION) > -1);
        if (unknownLang) {
          err.type = formConsts.UNKNOWN_LANG_ERR;
          return err;
        }

        return err;
      },
      throttle: throttle,
      debounce: debounce
    };
  });
})();

