// Karma configuration
module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)

    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],

    // list of files / patterns to load in the browser
    files: [

	    'bower_components/es5-shim/es5-shim.min.js',
        'bower_components/mobile-detect/mobile-detect.js',


	    <!-- Deps -->
	    'bower_components/angular/angular.js',
	    'bower_components/angular-animate/angular-animate.js',
	    'bower_components/angular-sanitize/angular-sanitize.js',
        'bower_components/angular-cookies/angular-cookies.js',
	    'bower_components/angular-strap/dist/angular-strap.js',
	    'bower_components/angular-strap/dist/angular-strap.tpl.js',
        'bower_components/angular-strap/dist/angular-strap.tpl.js',

        'bower_components/jquery-bridget/jquery.bridget.js',
        'bower_components/seiyria-bootstrap-slider/js/bootstrap-slider.js',
        'bower_components/angular-bootstrap-slider/slider.js',

	    'bower_components/angular-mocks/angular-mocks.js',
        'bower_components/json-formatter/dist/json-formatter.js',
        'bower_components/moment/moment.js',
        'bower_components/angular-moment/angular-moment.js',

        'bower_components/jquery/dist/jquery.js',
        'bower_components/jasmine-jquery/lib/jasmine-jquery.js',


	    'dist/forms-angular-ui.js',
	    'spec/**/*-spec.js',

        // fixtures
        {pattern: 'mocks/**/*.json', watched: true, served: true, included: false}
    ],

    // list of files to exclude
    exclude: [
    ],

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },

    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],

    // web server port
    port: 9876,

    // enable / disable colors in the output (reporters and logs)
    colors: true,

    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,

    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,

    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: [ 'PhantomJS' ],

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false
  });
};
