(function() {
    'use strict';

    var app = angular.module('forms-ui');

    app.directive('textBackButton', /*@ngInject*/ function($window) {
        return {
            restrict: 'C',
            scope: {
                text: '='
            },
            controller: function ($scope) {
                $window.onbeforeunload = function(e) {
                    e = e || window.event;
                    e.preventDefault = true;
                    e.cancelBubble = true;
                    e.returnValue = $scope.text;
                }

                $scope.$on("$destroy", function handleDestroyEvent() {
                    $window.onbeforeunload = undefined;
                });
            }
        };
    });
})();


