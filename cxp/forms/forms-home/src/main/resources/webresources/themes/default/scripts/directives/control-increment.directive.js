(function() {
  'use strict';

  var app = angular.module('forms-ui');

  /**
   * Element directive (bbControl)
   * This directive is designed to render single controls. It expects and element object as an attribute
   */
  app.directive('controlIncrement', /*@ngInject*/ function(templateBase) {
    return {
      restrict: 'AE',
      templateUrl: templateBase + '/controls/increment.html',
      scope: {
          element: '=',
          step: '='
      },
      link: function(scope) {

        scope.decrease = function () {

          if (scope.element.readonly){
            return false;
          }

          if (isNaN(scope.element.values[0])) {
            scope.element.values[0] = 0;
          } else {

            if (parseInt(scope.element.values[0]) <= 0 ){
              scope.element.values[0] = 0;
            }

            if (parseInt(scope.element.values[0]) > 0 ){
              scope.element.values[0] = parseInt(scope.element.values[0]) - scope.step;
            }
          }

        };

        scope.increase = function () {

          if (scope.element.readonly){
            return false;
          }

          if (isNaN(scope.element.values[0])) {
            scope.element.values[0] = 0;
          } else {

            if (parseInt(scope.element.values[0]) >= scope.element.validations[0].parameters.maxincl ){
              scope.element.values[0] = scope.element.validations[0].parameters.maxincl;
            } else {
              scope.element.values[0] = parseInt(scope.element.values[0]) + scope.step;
            }
          }
        };

      }
    };
  });
})();