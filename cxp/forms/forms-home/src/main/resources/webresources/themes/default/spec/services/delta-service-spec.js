describe('Delta service', function () {

	// Load your module.
	beforeEach(module('forms-ui'));

	var deltaService;

	beforeEach(inject(function($injector) {
		deltaService = $injector.get('deltaService');
	}));

	function clone(obj) {
		return JSON.parse(JSON.stringify(obj));
	}

	var originalElements = [
		{
			'key' : "F1",
			'values' : [ '1', '2', '3']
		},
		{
			'key' : "F2",
			'values' : [ 'one'],
			foo: null
		},
		{
			'key' : "F3",
			'values' : []
		},
		{
			'key' : "F4",
			'values' : [ 'unchanged' ]
		}
	];

	var newElements = [
	{
		'key' : "F1",
		'values' : []
	},
	{
		'key' : "F2",
		'values' : [ 'changed' ]
	},
	{
		'key' : "F3",
		'values' : []
	},
	{
		'key' : "F4",
		'values' : 'unchanged'
	}];

	var deltas = [{
		type: 'add',
		key: 'F5',
		model : {
			key: 'F5',
			values: [ 'five' ]
		}
	},
	{
		type: 'delete',
		key: 'F1',
		model: null
	},
	{
		type: 'delete',
		key: 'F3',
		model: null
	},
	{
		type: 'update',
		key: 'F2',
		model: {
			values: [ 'new' ],
			foo: 'bar'
		}
	}];

	it('makes a delta to send', function() {
		var elements = clone(originalElements);
		var deltas = deltaService.makeRequestDeltas(elements, newElements);
		expect(deltas.length).toBe(2);

		expect(deltas[0]).toEqual(newElements[0]);
		expect(deltas[1]).toEqual(newElements[1]);
	});

	it('applies delta to a model', function() {

		var elements = clone(originalElements);
		var model = deltaService.applyResponseDeltas(elements, deltas);

		expect(model.length).toBe(3);

		expect(model[0].key).toBe('F2');
		expect(model[1].key).toBe('F4');
		expect(model[2].key).toBe('F5');

		expect(model[0].values).toEqual([ 'new' ]);
		expect(model[0].foo).toEqual('bar');
	});
});