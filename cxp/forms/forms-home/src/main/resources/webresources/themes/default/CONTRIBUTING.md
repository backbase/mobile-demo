Forms Angular UI
===========================

An Angular client for Backbase Forms

##Contributing

###How to contribute

* Use this guide to work with the codebase.
* All new features should adhere to the Definition of Done (below)
* Create new features via Pull Request in Stash with the Jira ticket number in your message.

###Setup

Clone the repo and run the following commands to install dependencies

```
npm install
bower install
```

To run a build use gulp:

```
gulp
```

The build will lint code and run the unit tests using Karma + PhantomJS

###Run proxy server

The Forms Angular UI repo includes a small proxy server, so you can run a form outside of a Forms Runtime environment, but
still use real data.

To configure the proxy server, edit `runtime-config.json` and update with the details of the forms runtime you wish
to proxy to. E.g.

```json
{
	"runtimeServer" : "http://my.formsserver",
	"runtimePath" : "/Runtime"
}
```
will proxy to `http://my.formsserver/Runtime`

Then do

`npm start`

This will start the proxy server. You can then visit [http://localhost:5050/examples/index.html](http://localhost:5050/examples/index.html)
to use the example form.

*The example uses the example form "Kinderbijslag" by default. This must be imported in your forms runtime environment for the
example to run straight away.

###Running against mocks

You can also use a mock json file for testing basic rendering.

Start the server in mock mode, using the mock argument, like this:

```
node server --mock /mock-forms/kinderbijslag/model.json
```

###Directory structure

| Directory        | Purpose                                                                                 |
| -----------------|-----------------------------------------------------------------------------------------|
| src              | All source files are found here. This is laid out like a typical angular application    |
| spec             | Jasmine specs mirroring the src directory are found here                                |
| e2e              | Cucumber features are here for end to end (e2e) tests                                   |
| mocks            | When using the mock server configuration for testing forms, mock models are here        |
| examples         | Example usages, for demo and testing                                                    |
| dist             | Templates and minified JS builds are committed to source here                            |
| studio_exports   | Sample Forms studio exports for testing may be stored here                              |
| bower_components | Bower components are used for runtime dependencies. See the `bower.json` file           |
| node_modules     | Node modules are used for development dependencies. See the `package.json` file         |
|

###Coding guidelines

Please adhere to conventions enforced when running JSHint as part of the Gulp build.

Angular coding style should align with this [Angular Style Guide](https://github.com/johnpapa/angularjs-styleguide)

###Running specs / unit tests

Either run

`gulp test`

Or open [the spec runner](http://localhost:5050/spec) in a browser.

###Running functional tests

Functional tests are written using the "Given, When, Then" BDD approach and run using Protractor and Cucumber JS.

The tests are run against the UiTest project export available in the `studio-exports` directory.

To run first (install protractor + webdriver)[http://angular.github.io/protractor/#/]

Start the webdriver server:

`webdriver-manager start`

Then run protractor:

`protractor protractor.conf`

###Defintion of done

All features added to this project should adhere to the Definition of Done:

####For the core

Ensure:
* Code adheres to coding conventions and passes with JSHint
* Unit test(s) are implemented and all other unit tests are passing
* All existing UI control functional tests continue to pass
* Code/tests should run in IE8+ and major browsers

###For new UI controls
First consider if your UI control belongs as a default UI control or whether it should belong in a separate extension
pack.
Ensure:
* A Cucumber feature is written and passing
* All other functional test continue to pass.
