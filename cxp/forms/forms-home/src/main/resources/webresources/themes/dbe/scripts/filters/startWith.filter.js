(function() {
    'use strict';

    var app = angular.module('forms-ui');

    /**
     * Converts the name of a element to a nice id
     * Replaces SomeExample.field in some-example-field
     */
    app.filter('domainStartsWith', function() {
        return function (array, search) {
            var matches = [];
            search = search.toLowerCase();

            for (var i = 0; i < array.length; i++) {
                var domain = array[i].displayValue.toLowerCase();
                if(domain.indexOf(search)===0){
                    matches.push(array[i]);
                }
            }
            return matches;
        };
    });

})();